class APIurl {
  static BaseURL = "http://127.0.0.1:8000/api";
  static login = this.BaseURL + "/login";
  static register = this.BaseURL + "/register";
  static course = this.BaseURL + "/course";
  static cohort = this.BaseURL + "/cohort";
  static trainee = this.BaseURL + "/trainee";
  static company = this.BaseURL + "/company";
  static invoice = this.BaseURL + "/invoice";
  static setting = this.BaseURL + "/setting";
  static state = this.BaseURL + "/state";
  static discount = this.BaseURL + "/discount";
  static billing = this.BaseURL + "/billing";
  static staff = this.BaseURL + "/staff";
  static profile = this.BaseURL + "/profile";
  static bill = this.BaseURL + "/bill";
  static mail = this.BaseURL + "/mail";
  static forgotpassword = this.BaseURL + "/forgotpassword";
  static resetpassword = this.BaseURL + "/resetpassword";
}

export default APIurl;
