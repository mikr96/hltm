import { useParams, useNavigate, useOutlet } from "react-router-dom";
import { useContext, createContext } from "react";

const AppContext = createContext();

export function withRouter(Child) {
  return (props) => {
    const params = useParams();
    const navigate = useNavigate();
    return <Child {...props} params={params} navigate={navigate} />;
  };
}

export function withContext(Child) {
  return (props) => {
    const params = useContext(AppContext);
    return <Child {...props} params={params} />;
  };
}
