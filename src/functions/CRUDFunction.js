import axios from "axios";

class CRUDFunctions {
  static create = async (event, URL, data, header) => {
    event.preventDefault();
    return new Promise((resolve) => {
      axios
        .post(URL, data, header)
        .then((res) => {
          resolve(res.data.data);
        })
        .catch((err) => {
          console.log(err);
        });
    });
  };

  static get = async (URL, header) => {
    return new Promise((resolve) => {
      axios
        .get(URL, header)
        .then((res) => {
          resolve(res.data);
        })
        .catch((err) => {
          console.log(err);
        });
    });
  };

  static update = async (event, URL, data, header) => {
    event.preventDefault();
    return new Promise((resolve) => {
      axios
        .put(URL, data, header)
        .then((res) => {
          resolve(res.data.data);
        })
        .catch((err) => {
          console.log(err);
        });
    });
  };

  static delete = async (URL, header) => {
    return new Promise((resolve) => {
      axios
        .delete(URL, header)
        .then((res) => {
          console.log(res.data.data);
          resolve(res.data.data);
        })
        .catch((err) => {
          console.log(err);
        });
    });
  };

  static createBill = async (URL, data) => {
    return new Promise((resolve) => {
      axios
        .post(URL, data)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          console.log(err);
        });
    });
  };

  static password = async (URL, data) => {
    return new Promise((resolve) => {
      axios
        .post(URL, data)
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          console.log(err);
        });
    });
  };
}

export default CRUDFunctions;
