class APIFunctions {
  static getHeader = () => {
    let token = localStorage.getItem("token");
    return [
      {
        headers: {
          authorization: `Bearer ${token}`,
          ContentType: "application/json",
          Accept: "application/json",
        },
      },
    ];
  };
}

export default APIFunctions;
