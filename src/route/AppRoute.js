import React, { Component, Fragment } from "react";
import { Route, Routes } from "react-router";
import LoginPage from "../pages/LoginPage";
import RegisterPage from "../pages/RegisterPage";
import ResetPasswordPage from "../pages/ResetPasswordPage";
import LayoutDashboard from "../pages/LayoutDashboard";
import ForgetPasswordPage from "../pages/ForgetPasswordPage";
import Dashboard from "../pages/Dashboard/Dashboard";
import CohortPage from "../pages/Dashboard/CohortPage";
import CoursePage from "../pages/Dashboard/CoursePage";
import CompanyPage from "../pages/Dashboard/CompanyPage";
import { InvoicePage, PrintInvoice } from "../pages/Dashboard/InvoicePage";
import SettingPage from "../pages/Dashboard/SettingPage";
import Cohort from "../pages/Dashboard/Cohort";
import CohortDetailsPage from "../pages/Dashboard/CohortDetailsPage";
import NotFoundPage from "../pages/NotFoundPage";
import HomeLayout from "../pages/HomeLayout";

// Public Home
import HomePage from "../pages/Home/HomePage";
import CourseHomePage from "../pages/Home/CoursePage";
import CourseDetailsHomePage from "../pages/Home/CourseDetailsPage";
import TrainerHomePage from "../pages/Home/TrainersPage";
import AlumniHomePage from "../pages/Home/AlumniPage";
import FaqHomePage from "../pages/Home/FaqPage";
import ContactHomePage from "../pages/Home/ContactPage";
import ClientInvoice from "../pages/Client/ClientInvoice";
import ClientAttendance from "../pages/Client/ClientAttendance";
import ClientCourse from "../pages/Client/ClientCourse";
import ClientProfile from "../pages/Client/ClientProfile";
import SuccessfulPage from "../pages/SuccessfulPage";
import InvoicePrintPage from "../pages/InvoicePrintPage";

class AppRoute extends Component {
  constructor() {
    super();
    this.state = {
      user: {},
    };
  }

  setUser = (user) => {
    this.setState({ user: user });
  };

  render() {
    return (
      <Fragment>
        <Routes>
          <Route path="/" element={<HomeLayout />}>
            <Route path="" element={<HomePage />} />
            <Route path="course" element={<CourseHomePage />} />
            <Route path="course/:id" element={<CourseDetailsHomePage />} />
            <Route path="trainers" element={<TrainerHomePage />} />
            <Route path="alumni" element={<AlumniHomePage />} />
            <Route path="faq" element={<FaqHomePage />} />
            <Route path="contact" element={<ContactHomePage />} />
            <Route path="successful" element={<SuccessfulPage />} />
            <Route path="login" element={<LoginPage />} />
            <Route
              path="resetpassword/:token"
              element={<ResetPasswordPage />}
            />
            <Route path="*" element={<NotFoundPage />} />
          </Route>
          <Route path="layoutDashboard/:role" element={<LayoutDashboard />}>
            <Route path="" element={<Dashboard />} />
            <Route path="course" element={<CoursePage />} />
            <Route path="cohort" element={<Cohort />}>
              <Route path="" element={<CohortPage />} />
              <Route path="details/:id" element={<CohortDetailsPage />} />
            </Route>
            <Route path="company" element={<CompanyPage />} />
            <Route path="invoice" element={<InvoicePage />} />
            <Route path="setting" element={<SettingPage />} />
            <Route path="clientinvoice" element={<ClientInvoice />} />
            <Route path="clientattendance" element={<ClientAttendance />} />
            <Route path="clientcourse" element={<ClientCourse />} />

            <Route path="clientprofile" element={<ClientProfile />} />
          </Route>

          <Route path="printinvoice" element={<PrintInvoice />} />
          <Route path="invoiceprint" element={<InvoicePrintPage />} />
          <Route path="/register" element={<RegisterPage />} />
          <Route
            path="/forget"
            element={(props) => (
              <ForgetPasswordPage {...props} key={Date.now()} />
            )}
          />
          <Route component={NotFoundPage} />
        </Routes>
      </Fragment>
    );
  }
}

export default AppRoute;
