import React, { Component, Fragment } from "react";
import { Button, Table, Form, Modal } from "react-bootstrap";
import APIFunction from "../../functions/APIFunction";
import CRUDFunction from "../../functions/CRUDFunction";
import APIurl from "../../api/AppURL";

export class ClientInvoice extends Component {
  constructor() {
    super();
    this.state = {
      invoiceData: [],
      isDataExist: false,
    };
  }

  componentDidMount = async () => {
    const invoiceData = await CRUDFunction.get(
      `${APIurl.invoice}/${this.props.id}`,
      APIFunction.getHeader()[0]
    );
    console.log("Hello");
    if (invoiceData) {
      console.log(invoiceData);
      this.setState({
        invoiceData: [invoiceData.data],
        isDataExist: true,
      });
    }
  };

  render() {
    return (
      <Fragment>
        <br />
        <div class="bg-darkgreen p-15-20">
          <h3>INVOICE LIST</h3>
        </div>
        <Table
          hover
          responsive
          size="lg"
          style={{
            textAlign: "center",
            boxShadow: "0 0 10px 0 rgba(0, 0, 0, 0.2)",
          }}
        >
          <thead style={{ background: "#468C32", color: "white" }}>
            <tr>
              <th>NO</th>
              <th>TRAINING NAME</th>
              <th>STATUS</th>
              <th>ACTION</th>
            </tr>
          </thead>
          <tbody>
            {this.state.invoiceData.map((iteration) => {
              return iteration.map((elem, i) => (
                <tr>
                  <td>{i + 1}</td>
                  <td>{elem.train_name}</td>
                  <td>status</td>
                  <td>action</td>
                </tr>
              ));
            })}
          </tbody>
        </Table>
      </Fragment>
    );
  }
}

export default ClientInvoice;
