import React, { Component, Fragment } from "react";
import { Table, Container } from "react-bootstrap";

class ClientAttendance extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.id,
      show: false,
    };
  }

  render() {
    return (
      <Fragment>
        <Container>
          <div
            style={{
              display: "flex",
              justifyContent: "flex-end",
              flexDirection: "row",
              width: "100%",
            }}
          ></div>
          <br />
          <div class="bg-darkgreen p-15-20">
            <h3>ATTENDANCE LIST</h3>
            <Table
              hover
              responsive
              size="lg"
              style={{
                textAlign: "center",
                boxShadow: "0 0 10px 0 rgba(0, 0, 0, 0.2)",
              }}
            >
              <thead style={{ background: "#468C32", color: "white" }}>
                <tr>
                  <th>NO</th>
                  <th>NAME</th>
                  <th>COHORT</th>
                  <th>CHECK IN</th>
                  <th>CHECK OUT</th>
                </tr>
              </thead>
            </Table>
          </div>
        </Container>
      </Fragment>
    );
  }
}

export default ClientAttendance;
