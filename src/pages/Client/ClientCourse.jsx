import React, { Component, Fragment } from "react";
import { Row, Col } from "react-bootstrap";
import APIFunction from "../../functions/APIFunction";
import CRUDFunction from "../../functions/CRUDFunction";
import APIurl from "../../api/AppURL";
import TableContext from "../../components/Course/TableContext";

export class ClientCourse extends Component {
  constructor() {
    super();
    this.state = {
      header: ["NO", "COURSE NAME", "DESCRIPTION", "ACTION"],
      isDataExist: false,
      courseData: [],
      showModal: false,
      dataModal: [],
    };
  }

  componentDidMount = async () => {
    const courseData = await CRUDFunction.get(
      `${APIurl.course}`,
      APIFunction.getHeader()[0]
    );
    if (courseData) {
      this.setState({
        isDataExist: true,
        courseData: [courseData.data],
      });
    }
  };
  render() {
    return (
      <Fragment>
        {" "}
        <Row>
          <Col md={12}>
            <TableContext
              header={this.state.header}
              courseData={this.state.courseData}
              showModal={this.state.showModal}
              dataModal={this.state.dataModal}
              role="client"
              color="#468C32"
              isDataExist={this.state.isDataExist}
            ></TableContext>
          </Col>
        </Row>
      </Fragment>
    );
  }
}

export default ClientCourse;
