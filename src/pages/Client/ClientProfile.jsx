import React, { Component, Fragment } from "react";
import { Row, Col, Button, Container } from "react-bootstrap";
import axios from "axios";
import Form from "react-bootstrap/Form";
import APIFunction from "../../functions/APIFunction";
import CRUDFunction from "../../functions/CRUDFunction";
import APIurl from "../../api/AppURL";
import Swal from "sweetalert2";

export class ClientProfile extends Component {
  constructor() {
    super();
    this.state = { profileData: [], fullname: "" };

    this.updateProfile = this.updateProfile.bind(this);
  }
  success() {
    Swal.fire({
      position: "top-center",
      icon: "success",
      title: "Successfully updated",
      showConfirmButton: false,
      timer: 2500,
    });
  }
  updateProfile = async (event, data) => {
    event.preventDefault();
    console.log(data);
    const profile = await CRUDFunction.update(
      event,
      `${APIurl.profile}/1`,
      data,
      APIFunction.getHeader()[0]
    );
    if (profile) {
      console.log(profile);
      this.setState({
        fullname: profile[0].fullname,
      });
    }
  };

  componentDidMount = async () => {
    let user = JSON.parse(localStorage.getItem("user"));
    const profileData = await CRUDFunction.get(
      `${APIurl.profile}/${user.id}`,
      APIFunction.getHeader()[0]
    );
    if (profileData) {
      console.log(profileData);
      this.setState({
        isDataExist: true,
        profileData: profileData,
        fullname: profileData.fullname,
        email: profileData.email,
        phone_no: profileData.phone_no,
        company_name: profileData.companies.company_name,
        company_address: profileData.companies.company_address,
        company_branch: profileData.companies.company_branch,
      });
    }
  };
  render() {
    return (
      <Fragment>
        <br />
        <Container>
          <div class="bg-darkGreen p-15-20">
            <h3>PROFILE COMPANY</h3>
          </div>

          <div
            style={{ boxShadow: "0 0 10px 0 rgba(0, 0, 0, 0.2)" }}
            className="bg-white p-15-20"
          >
            <Form
              onSubmit={(e) =>
                this.updateProfile(e, {
                  fullname: e.target.elements.fullname.value,
                  email: e.target.elements.email.value,
                  phone_no: e.target.elements.phone_no.value,
                  company_address: e.target.elements.company_address.value,
                })
              }
            >
              <Row>
                <Form.Group as={Col} controlId="formGridEmail">
                  <Form.Label>Company Name</Form.Label>
                  <Form.Control
                    name="company_name"
                    defaultValue={this.state.company_name}
                    onChange={(e) => {
                      this.setState({
                        newdata: {
                          ...this.state.newdata,
                          [e.target.name]: e.target.value,
                        },
                      });
                    }}
                  />
                </Form.Group>
                <Form.Group
                  className="mb-3"
                  controlId="formGridAddress1"
                  onChange={this.onChangeCompanyType}
                >
                  <Form.Label as={Col} controlId="formGridEmail">
                    Company Type
                  </Form.Label>
                  <Form.Select aria-label="Default select example">
                    <option value="0">Open this select menu</option>
                    <option value="1">Big</option>
                    <option value="2">Medium</option>
                    <option value="3">Small</option>
                    <option value="4">Micro</option>
                    {this.state.company_type}
                  </Form.Select>
                </Form.Group>
              </Row>
              <Row>
                <Form.Group as={Col} controlId="formGridPassword">
                  <Form.Label>Branch</Form.Label>
                  <Form.Control
                    name="biller_address"
                    defaultValue={this.state.biller_address}
                    onChange={(e) => {
                      this.setState({
                        newdata: {
                          ...this.state.newdata,
                          [e.target.name]: e.target.value,
                        },
                      });
                    }}
                  />
                </Form.Group>
                <Form.Group as={Col} controlId="formGridEmail">
                  <Form.Label>Company Address</Form.Label>
                  <Form.Control
                    name="company_address"
                    defaultValue={this.state.company_address}
                    onChange={(e) => {
                      this.setState({
                        newdata: {
                          ...this.state.newdata,
                          [e.target.name]: e.target.value,
                        },
                      });
                    }}
                  />
                </Form.Group>
              </Row>
              <br />
              <Row>
                <Form.Group className="mb-3" controlId="formGridpic">
                  <Form.Label>Person in Charge :</Form.Label>
                </Form.Group>
              </Row>
              <Row className="mb-3">
                <Form.Group className="mb-3" controlId="formGridAddress1">
                  <Form.Label>Full Name</Form.Label>
                  <Form.Control
                    name="fullname"
                    defaultValue={this.state.fullname}
                    onChange={(e) => {
                      this.setState({
                        newdata: {
                          ...this.state.newdata,
                          [e.target.name]: e.target.value,
                        },
                      });
                    }}
                  />
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlTextarea1">
                  <Form.Label>Email</Form.Label>

                  <Form.Control
                    name="email"
                    defaultValue={this.state.email}
                    onChange={(e) => {
                      this.setState({
                        newdata: {
                          ...this.state.newdata,
                          [e.target.name]: e.target.value,
                        },
                      });
                    }}
                  />
                </Form.Group>
              </Row>
              <Row>
                <Form.Group controlId="exampleForm.ControlTextarea1">
                  <Form.Label>No Phone</Form.Label>

                  <Form.Control
                    name="phone_no"
                    defaultValue={this.state.phone_no}
                    onChange={(e) => {
                      this.setState({
                        newdata: {
                          ...this.state.newdata,
                          [e.target.name]: e.target.value,
                        },
                      });
                    }}
                  />
                </Form.Group>
              </Row>
              <br />
              <br />
              <Button
                className="d-flex"
                style={{ float: "right" }}
                variant="primary"
                type="submit"
                onClick={this.success}
              >
                Update
              </Button>
              <br />
              <br />
            </Form>
          </div>
          <br />
        </Container>
      </Fragment>
    );
  }
}

export default ClientProfile;
