import React, { Component, Fragment } from "react";
import { Message } from "semantic-ui-react";
import logo from "../assets/images/colorLogo.png";

import { Link } from "react-router-dom";
import {
  Form,
  Card,
  Row,
  Col,
  Button,
  Image,
  FormControl,
  Dropdown,
} from "react-bootstrap";
import "../assets/css/profile.css";
import axios from "axios";
import APIurl from "../api/AppURL";
import { Navigate } from "react-router";
import toast, { Toaster } from "react-hot-toast";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import InputGroup from "react-bootstrap/InputGroup";
import APIFunction from "../functions/APIFunction";
import CRUDFunction from "../functions/CRUDFunction";
const MySwal = withReactContent(Swal);

class RegisterPage extends Component {
  constructor() {
    super();
    this.state = {
      role_type: "",
      email: "",
      ic: "",
      fullname: "",
      ssm_no: "",
      company_id: "",
      company_name: [],
      company_address: "",
      company_type: "",
      position: "",
      allergies: "",
      referrel_code: "",
      promo_code: "",
      hrdf_claim: false,
      success: false,
      checked: true,
      value: "",
      company_list: "",
      isCompanyNameExist: true,
      isCompanyExist: false,
    };

    this.onFormSubmit = this.onFormSubmit.bind(this);
    this.onChangeBranch = this.onChangeBranch.bind(this);
    this.onChangeCompany = this.onChangeCompany.bind(this);
    this.onChangeCompanyName = this.onChangeCompanyName.bind(this);
  }

  componentDidMount = async () => {
    const companyDetails = await CRUDFunction.get(
      `${APIurl.company}name`,
      APIFunction.getHeader()[0]
    );
    if (companyDetails) {
      let company_name = companyDetails.map((e) => e.company_name);
      this.setState({
        companyDetails: companyDetails,
        company_name: company_name,
        company_list: companyDetails.map((elem) => (
          <option value={elem.id}>{elem.company_name}</option>
        )),
        isCompanyExist: true,
      });
    }
  };

  onFormSubmit = (event) => {
    event.preventDefault();
    let data = {
      email: this.state.email,
      password: this.state.ic,
      fullname: this.state.fullname,
      ic: this.state.ic,
      phone_no: this.state.phone_no,
      position: this.state.position,
      company_id: this.state.company_id,
      company_address: this.state.company_address,
      branch: this.state.branch,
      allergies: this.state.allergies,
      referrer_code: this.state.referrer_code,
      promo_code: this.state.promo_code,
      hrdf_claim: this.state.hrdf_claim,
      role_type: this.state.role,
    };

    if (this.state.role == "company") {
      data = {
        email: this.state.email,
        password: this.state.phone_no,
        fullname: this.state.fullname,
        ic: this.state.ic,
        phone_no: this.state.phone_no,
        position: this.state.position,
        company_id: this.state.company_id,
        company_name:
          typeof this.state.company_name == "object"
            ? this.state.company_name[this.state.company_id]
            : this.state.company_name,
        company_address: this.state.company_address,
        branch: this.state.branch,
        allergies: this.state.allergies,
        referrer_code: this.state.referrer_code,
        promo_code: this.state.promo_code,
        hrdf_claim: this.state.hrdf_claim,
        role_type: this.state.role,
      };
    }

    if (data) {
      console.log(data);
      axios
        .post(APIurl.register, data)
        .then((response) => {
          if (response.status === 200) {
            MySwal.fire({
              title: <p>Successfully Register</p>,
              footer: "Copyright 2022",
              onOpen: () => {
                // MySwal` is a subclass of `Swal`
                //   with all the same instance & static methods
                MySwal.clickConfirm();
              },
            }).then(() => {
              this.setState({ success: true });
            });
          } else {
            toast.error(response.data, {
              duration: 4000,
            });
          }
        })
        .catch((err) => {
          console.log(err.response);
          toast.error(err.response.data, {
            duration: 4000,
          });
        });
    }
  };

  onChangeBranch = (event) => {
    this.setState({ checked: !event.target.checked });
  };
  onChangeCompany = (event) => {
    if (!event.target.checked) {
      this.setState({
        company_name: "",
        isCompanyNameExist: !event.target.checked,
      });
    } else {
      this.setState({
        isCompanyNameExist: !event.target.checked,
        company_address: "",
      });
    }
  };
  onChangeCompanyName = async (event) => {
    if (event.target.value !== "0") {
      const companyAddress = await CRUDFunction.get(
        `${APIurl.company}address/${event.target.value}`,
        APIFunction.getHeader()[0]
      );
      if (companyAddress) {
        console.log(companyAddress);
        this.setState({
          company_address: companyAddress[0].company_address,
          company_id: event.target.value,
        });
      }
    } else {
      this.setState({
        company_address: "",
        company_name: "",
      });
    }
  };

  render() {
    /// After Login Navigate to Profile Page
    if (this.state.success) {
      return <Navigate to={"/login"} />;
    }

    if (this.state.isCompanyExist) {
      console.log("Cpy", this.state.companyDetails);

      return (
        <Fragment>
          <Toaster />
          {/* isLoading?<FlashScreen msg={loadingMsg}/>: */}
          <div className="container">
            <Row style={{ justifyContent: "center" }}>
              <Col>
                <Image
                  style={{ width: "60%", margin: "auto", display: "flex" }}
                  src={logo}
                />
              </Col>
            </Row>
            <Row style={{ justifyContent: "center" }}>
              <Col xs={8} md={5}>
                <Card>
                  <Card.Body>
                    <Row>
                      <Col md={6}>
                        <Button
                          variant="success"
                          style={{
                            width: "100%",
                            color: "white",
                            background: "#468c32",
                          }}
                          onClick={() => this.setState({ role: "personal" })}
                        >
                          Personal
                        </Button>
                      </Col>
                      <Col md={6}>
                        <Button
                          variant="success"
                          style={{
                            width: "100%",
                            color: "white",
                            background: "#468c32",
                          }}
                          onClick={() => this.setState({ role: "company" })}
                        >
                          Company
                        </Button>
                      </Col>
                    </Row>
                  </Card.Body>
                </Card>
              </Col>
            </Row>
            <br />
            <Row style={{ justifyContent: "center" }}>
              <Col xs={6} md={7}>
                {this.state.role === "personal" && (
                  <Card>
                    <Card.Body>
                      <Row>
                        <Col>
                          <Form
                            size="large"
                            onSubmit={this.onFormSubmit.bind(this)}
                          >
                            <Form.Group
                              className="mb-3"
                              controlId="formGridAddress1"
                              onChange={(e) => {
                                this.setState({ fullname: e.target.value });
                              }}
                            >
                              <div class="required" font-color="grey">
                                Required
                              </div>

                              <Form.Label class="required">Fullname</Form.Label>
                              <Form.Control placeholder="" />
                            </Form.Group>
                            <Form.Group
                              className="mb-3"
                              controlId="formGridAddress1"
                              onChange={(e) => {
                                this.setState({ email: e.target.value });
                              }}
                            >
                              <Form.Label class="required">Email</Form.Label>
                              <Form.Control placeholder="" />
                            </Form.Group>
                            <Form.Group
                              className="mb-3"
                              controlId="formGridAddress1"
                              onChange={(e) => {
                                this.setState({ ic: e.target.value });
                              }}
                            >
                              <Form.Label class="required">No. IC</Form.Label>
                              <Form.Control placeholder="" />
                            </Form.Group>
                            <Form.Group
                              className="mb-3"
                              controlId="formGridAddress1"
                              onChange={(e) => {
                                this.setState({ phone_no: e.target.value });
                              }}
                            >
                              <Form.Label class="required">
                                No. Phone
                              </Form.Label>
                              <Form.Control type="number" placeholder="" />
                            </Form.Group>
                            <Form.Group
                              className="mb-3"
                              controlId="formGridAddress1"
                              onChange={(e) => {
                                this.setState({ company_name: e.target.value });
                              }}
                            >
                              <Form.Label class="required">Position</Form.Label>
                              <Form.Control placeholder="" />
                            </Form.Group>
                            <Form.Group
                              className="mb-3"
                              controlId="formGridAddress1"
                              onChange={this.onChangeCompanyName}
                            >
                              <Form.Label class="required">
                                Company Name
                              </Form.Label>
                              <Form.Select aria-label="Default select example">
                                <option value="0">Open this select menu</option>
                                {this.state.company_list}
                              </Form.Select>
                            </Form.Group>
                            <Form.Group
                              className="mb-3"
                              controlId="formGridAddress1"
                            >
                              <Form.Label class="required">
                                Company Address
                              </Form.Label>
                              <Form.Control
                                placeholder=""
                                disabled
                                value={this.state.company_address}
                              />
                            </Form.Group>
                            <Form.Group
                              className="mb-3"
                              controlId="formGridAddress1"
                              onChange={(e) => {
                                this.setState({ allergies: e.target.value });
                              }}
                            >
                              <Form.Label>Allergies/Special Dietary</Form.Label>
                              <Form.Control placeholder="" />
                            </Form.Group>
                            <Form.Group
                              className="mb-3"
                              controlId="formGridAddress1"
                              onChange={(e) => {
                                this.setState({
                                  referrer_code: e.target.value,
                                });
                              }}
                            >
                              <Form.Label>Referrel Code</Form.Label>
                              <Form.Control placeholder="" />
                            </Form.Group>
                            <Form.Group
                              className="mb-3"
                              controlId="formGridAddress1"
                              onChange={(e) => {
                                this.setState({ promo_code: e.target.value });
                              }}
                            >
                              <Form.Label>Promo Code</Form.Label>
                              <Form.Control placeholder="" />
                            </Form.Group>
                            <Form.Group>
                              <Form.Check
                                inline
                                label="Claim with HDRF?"
                                style={{ marginBottom: 10 }}
                                onChange={(e) => {
                                  e.target.value === "on"
                                    ? this.setState({ hrdf_claim: true })
                                    : this.setState({ hrdf_claim: false });
                                }}
                              />
                            </Form.Group>
                            <br />
                            <Button
                              className="d-flex"
                              style={{ float: "right" }}
                              variant="primary"
                              type="submit"
                            >
                              Register
                            </Button>
                          </Form>
                        </Col>
                      </Row>
                    </Card.Body>
                  </Card>
                )}
                {this.state.role === "company" && (
                  <Card>
                    <Card.Body>
                      <Form size="large" onSubmit={this.onFormSubmit}>
                        <Form.Group
                          className="mb-3"
                          controlId="formGridAddress1"
                          onChange={this.onChangeCompanyName}
                        >
                          <div class="required">Required</div>
                          <Form.Label>Company Name</Form.Label>
                          <Form.Select
                            aria-label="Default select example"
                            disabled={!this.state.isCompanyNameExist}
                          >
                            <option value="0">Open this select menu</option>
                            {this.state.company_list}
                          </Form.Select>
                        </Form.Group>
                        <Form.Group
                          className="mb-3"
                          controlId="formGridAddress1"
                          onChange={(e) => {
                            this.setState({
                              company_name: e.target.value,
                              company_id: "0",
                            });
                          }}
                          disabled={this.state.checked}
                        >
                          <Form.Label>Company Name (If new)</Form.Label>
                          <>
                            <InputGroup className="mb-3">
                              <InputGroup.Checkbox
                                aria-label="Checkbox for following text input"
                                onChange={this.onChangeCompany}
                              />
                              <FormControl
                                disabled={this.state.isCompanyNameExist}
                                aria-label="Text input with checkbox"
                              />
                            </InputGroup>
                          </>
                        </Form.Group>

                        <Form.Group
                          className="mb-3"
                          controlId="formGridAddress1"
                          onChange={(e) => {
                            this.setState({ branch: e.target.value });
                          }}
                          disabled={this.state.checked}
                        >
                          <Form.Label>
                            Branch (If have, enter your branch address)
                          </Form.Label>
                          <>
                            <InputGroup className="mb-3">
                              <InputGroup.Checkbox
                                aria-label="Checkbox for following text input"
                                onChange={this.onChangeBranch}
                              />
                              <FormControl
                                disabled={this.state.checked}
                                aria-label="Text input with checkbox"
                              />
                            </InputGroup>
                          </>
                        </Form.Group>

                        <Form.Group
                          className="mb-3"
                          controlId="formGridAddress1"
                          onChange={(e) => {
                            this.setState({ company_address: e.target.value });
                          }}
                        >
                          <Form.Label class="required">
                            Company Address
                          </Form.Label>
                          <Form.Control
                            placeholder=""
                            disabled={this.state.isCompanyNameExist}
                            value={this.state.company_address}
                          />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formGridpic">
                          <Form.Label>Person in Charge</Form.Label>
                        </Form.Group>

                        <Form.Group
                          as={Col}
                          controlId="formGridname"
                          onChange={(e) => {
                            this.setState({ fullname: e.target.value });
                          }}
                        >
                          <Form.Label class="required">Full name</Form.Label>
                          <Form.Control />
                        </Form.Group>

                        <Form.Group
                          as={Col}
                          controlId="formGridic"
                          onChange={(e) => {
                            this.setState({ email: e.target.value });
                          }}
                        >
                          <Form.Label class="required">Email</Form.Label>
                          <Form.Control />
                        </Form.Group>

                        <Form.Group
                          as={Col}
                          controlId="formGridemail"
                          onChange={(e) => {
                            this.setState({ phone_no: e.target.value });
                          }}
                        >
                          <Form.Label class="required">No Phone</Form.Label>
                          <Form.Control type="number" />
                        </Form.Group>
                        <br />

                        <Button
                          className="d-flex"
                          style={{ float: "right" }}
                          variant="primary"
                          type="submit"
                        >
                          Register
                        </Button>
                      </Form>
                    </Card.Body>
                  </Card>
                )}
              </Col>
            </Row>
            <br />
            <Row style={{ justifyContent: "center" }}>
              <Col xs={6} md={7}>
                <Message>
                  Already have an account? <Link to="/login">Login here</Link>
                </Message>
              </Col>
            </Row>
            <br />
          </div>
        </Fragment>
      );
    } else {
      return "";
    }
  }
}

export default RegisterPage;
