import React, { Component, Fragment } from "react";
import Sidebar from "../components/common/Sidebar";
import HeaderDesktop from "../components/common/HeaderDesktop";
import { Row, Col, Container } from "react-bootstrap";
import { Outlet } from "react-router-dom";
import { withRouter } from "./../functions/withRouter";
class LayoutDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sidebar: false,
    };
  }
  onChangeSidebarStatus = () => {
    this.setState((prev) => ({
      sidebar: !prev.sidebar,
    }));
  };
  componentDidMount = () => {
    this.setState({ role: JSON.parse(localStorage.getItem("user")).role });
  };
  render = () => {
    return (
      <Fragment>
        <Row>
          <HeaderDesktop
            role={this.props.params.role}
            onChangeStatus={this.onChangeSidebarStatus}
          />
        </Row>
        <br />
        <br />
        <br />
        <Row>
          <Col
            sm={2}
            md={2}
            lg={2}
            className={this.state.sidebar ? "zeroContent" : ""}
            style={{ paddingLeft: "0px" }}
          >
            <Sidebar
              role={this.props.params.role}
              status={this.state.sidebar}
            />
          </Col>
          <Col
            sm={10}
            md={10}
            lg={10}
            className={this.state.sidebar ? "forceContent" : ""}
          >
            <Container>
              <br />
              <Outlet />
            </Container>
          </Col>
        </Row>
      </Fragment>
    );
  };
}

export default withRouter(LayoutDashboard);
