import React, { Component, Fragment } from "react";
import { Container, Row, Col } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "./../assets/css/successful.css";

export class SuccessfulPage extends Component {
  render() {
    return (
      <Fragment>
        <Container>
          <Row>
            <Col md={6} className="mx-auto mt-5">
              <div
                className="payment"
                style={{ boxShadow: "0 0 10px 0 rgba(0, 0, 0, 0.2)" }}
              >
                <div className="payment_header">
                  <div className="check">
                    <FontAwesomeIcon
                      icon={["fas", "check"]}
                      color="#468c32"
                      size="lg"
                      style={{
                        marginTop: "10px",
                        verticalAlign: "middle",
                        lineHeight: "50px",
                        fontSize: "30px",
                      }}
                    />
                  </div>
                </div>
                <div className="content">
                  <h1>Payment Success !</h1>
                  <p style={{ paddingLeft: "40px", paddingRight: "40px" }}>
                    Lorem ipsum, or lipsum as it is sometimes known, is dummy
                    text used in laying out print, graphic or web designs.{" "}
                  </p>
                  <a href="/">Go to Home</a>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
        <br />
        <br />
      </Fragment>
    );
  }
}

export default SuccessfulPage;
