import React, { Component } from "react";

export class NotFoundPage extends Component {
  render() {
    return <h1>Not Found Error 404</h1>;
  }
}

export default NotFoundPage;
