import React, { Component, Fragment } from "react";
import TableContext from "../../components/Company/TableContext";
import { Row, Col, Button, Modal, Form } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import APIurl from "../../api/AppURL";
import APIFunction from "../../functions/APIFunction";
import axios from "axios";
import CRUDFunction from "../../functions/CRUDFunction";
import Swal from "sweetalert2";

class CompanyPage extends Component {
  constructor() {
    super();
    this.state = {
      show: false,
      stateList: false,
      data: [],
      newdata: [],
      dataModal: [],
      showModal: false,
      id: "",
      header: [
        "NO",
        "COMPANY NAME",
        "COMPANY ADDRESS",
        "COMPANY BRANCH",
        "COMPANY REGISTERED NUMBER",
        "COMPANY TYPE",
        "COMPANY DETAILS",
        "ACTION",
      ],
    };

    this.deleteCompany = this.deleteCompany.bind(this);
    this.updateCompany = this.updateCompany.bind(this);
    this.getCompanybyID = this.getCompanybyID.bind(this);
    this.addCompany = this.addCompany.bind(this);
  }

  componentDidMount = () => {
    axios
      .get(APIurl.company, APIFunction.getHeader()[0])
      .then((com) => {
        if (Object.keys(com.data.data).length == 0) {
          this.setState({
            data: [
              {
                id: 0,
                company_name: "none",
                company_address: "none",
                company_branch: "none",
                company_register_no: "none",
                company_type: "none",
                company_details: "none",
              },
            ],
          });
        } else {
          this.setState({
            data: {
              ...com.data.data,
            },
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  addCompany = async (event, formData) => {
    const biller = await CRUDFunction.create(
      event,
      `${APIurl.company}`,
      formData,
      APIFunction.getHeader()[0]
    );
    if (biller) {
      Swal.fire({
        position: "top-center",
        icon: "success",
        title: "Successfully updated",
        showConfirmButton: false,
        timer: 2500,
      }).then(() => {
        this.setState({
          show: false,
          stateList: false,
          data: { ...biller.data },
        });
      });
    }
    // axios
    //   .post(APIurl.company, formData, APIFunction.getHeader()[0])
    //   .then((com) => {
    //     console.log(com);
    //     this.setState({
    //       show: false,
    //       stateList: false,
    //       data: { ...com.data.data },
    //     });
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //   });
  };

  updateCompany = (event, data) => {
    event.preventDefault();
    console.log("data: ", data);
    axios
      .put(
        `${APIurl.company}/${this.state.id}`,
        data,
        APIFunction.getHeader()[0]
      )
      .then((com) => {
        if (com) {
          this.setState({
            showModal: false,
            data: { ...com.data.data },
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  deleteCompany = (event, id) => {
    event.preventDefault();
    axios
      .delete(`${APIurl.company}/${id}`, APIFunction.getHeader()[0])
      .then((com) => {
        console.log(com);
        if (com) {
          Swal.fire({
            title: "Are you sure?",
            text: "You want delete your item?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes, delete it!",
          }).then((result) => {
            if (result.isConfirmed) {
              Swal.fire("Deleted!", "Your file has been deleted.", "success");
              this.setState({
                data: {
                  ...com.data.data,
                },
              });
            }
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  getCompanybyID = (e, id) => {
    axios
      .get(`${APIurl.company}/${id}`, APIFunction.getHeader()[0])
      .then((com) => {
        localStorage.setItem("id", id);
        this.setState({
          dataModal: {
            company_name: com.data.company_name,
            company_address: com.data.company_address,
            company_branch: com.data.company_branch,
            company_register_no: com.data.company_register_no,
            company_type: com.data.company_type,
            company_details: com.data.company_details,
          },
          showModal: true,
          id: id,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  closeModal = () => {
    this.setState({
      showModal: false,
    });
  };

  render = () => {
    return (
      <Fragment>
        <br />
        <Row>
          <div
            style={{
              display: "flex",
              justifyContent: "flex-end",
            }}
          >
            <Button
              onClick={() => {
                this.setState({ show: true, stateList: true });
              }}
              className="primary"
            >
              <FontAwesomeIcon icon={["fas", "plus"]} color="white" size="sm" />
              &nbsp; Add Company
            </Button>
          </div>
        </Row>
        {this.state.stateList && (
          <Modal
            show={this.state.show}
            onHide={() => {
              this.setState({
                show: false,
              });
            }}
          >
            <Form
              onSubmit={(e) => {
                this.addCompany(e, {
                  company_name: e.target.elements.company_name.value,
                  company_address: JSON.stringify([
                    e.target.elements.line_1.value,
                    e.target.elements.line_2.value,
                    e.target.elements.line_3.value,
                    e.target.elements.line_4.value,
                  ]),
                  company_branch: e.target.elements.company_branch.value,
                  company_register_no:
                    e.target.elements.company_register_no.value,
                  company_type: e.target.elements.company_type.value,
                  company_details: e.target.elements.company_details.value,
                });
              }}
            >
              <Modal.Header closeButton>
                <Modal.Title>ADD COMPANY</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <Form.Group
                  className="mb-3"
                  onChange={(e) => {
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        [e.target.name]: e.target.value,
                      },
                    });
                  }}
                >
                  <Form.Label>COMPANY NAME</Form.Label>
                  <Form.Control
                    type="string"
                    name="company_name"
                    placeholder="Enter company name"
                  />
                </Form.Group>
                <Form.Group
                  className="mb-3"
                  onChange={(e) => {
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        [e.target.name]: e.target.value,
                      },
                    });
                  }}
                >
                  <Form.Label>COMPANY ADDRESS</Form.Label>
                  <Form.Control
                    type="string"
                    name="line_1"
                    placeholder="Address 1"
                    style={{ marginBottom: "0.5rem" }}
                  />
                  <Form.Control
                    type="string"
                    name="line_2"
                    placeholder="Address 2"
                    style={{ marginBottom: "0.5rem" }}
                  />
                  <Form.Control
                    type="string"
                    name="line_3"
                    placeholder="City"
                    style={{ marginBottom: "0.5rem" }}
                  />
                  <Form.Control
                    type="string"
                    name="line_4"
                    placeholder="Country"
                    style={{ marginBottom: "0.5rem" }}
                  />
                </Form.Group>
                <Form.Group
                  className="mb-3"
                  onChange={(e) => {
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        [e.target.name]: e.target.value,
                      },
                    });
                  }}
                >
                  <Form.Label>COMPANY BRANCH</Form.Label>
                  <Form.Control
                    type="string"
                    name="company_branch"
                    placeholder="Enter company branch (optional)"
                  />
                </Form.Group>
                <Form.Group
                  className="mb-3"
                  onChange={(e) => {
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        [e.target.name]: e.target.value,
                      },
                    });
                  }}
                >
                  <Form.Label>COMPANY REGISTERED NUMBER</Form.Label>
                  <Form.Control
                    type="string"
                    name="company_register_no"
                    placeholder="Enter register number (optional)"
                  />
                </Form.Group>
                <Form.Group
                  className="mb-3"
                  onChange={(e) => {
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        [e.target.name]: e.target.value,
                      },
                    });
                  }}
                >
                  <Form.Label>COMPANY TYPE</Form.Label>
                  <Form.Control
                    as="textarea"
                    rows={3}
                    placeholder="Enter company type"
                    name="company_type"
                  />
                </Form.Group>
                <Form.Group
                  className="mb-3"
                  onChange={(e) => {
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        [e.target.name]: e.target.value,
                      },
                    });
                  }}
                >
                  <Form.Label>COMPANY DETAILS</Form.Label>
                  <Form.Control
                    type="string"
                    name="company_details"
                    placeholder="Enter company details"
                  />
                </Form.Group>
              </Modal.Body>

              <Modal.Footer>
                <Button
                  variant="primary"
                  onClick={() =>
                    this.setState({
                      show: false,
                      stateList: false,
                    })
                  }
                >
                  Close
                </Button>
                <Button variant="primary" type="submit">
                  Add
                </Button>
              </Modal.Footer>
            </Form>
          </Modal>
        )}
        <Row>
          <Col md={12}>
            <TableContext
              header={this.state.header}
              data={this.state.data}
              showModal={this.state.showModal}
              dataModal={this.state.dataModal}
              updateCompany={this.updateCompany}
              getCompanybyID={this.getCompanybyID}
              closeModal={this.closeModal}
              deleteCompany={this.deleteCompany}
            ></TableContext>
          </Col>
        </Row>
      </Fragment>
    );
  };
}

export default CompanyPage;
