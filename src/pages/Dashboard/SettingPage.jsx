import React, { Component, Fragment } from "react";
import {
  Row,
  Col,
  Button,
  Modal,
  Dropdown,
  Form,
  Container,
  Nav,
  Navbar,
} from "react-bootstrap";
import Discount from "../../components/Setting/Discount";
import State from "../../components/Setting/State";
import APIFunction from "../../functions/APIFunction";
import CRUDFunction from "../../functions/CRUDFunction";
import APIurl from "../../api/AppURL";

class SettingPage extends Component {
  constructor() {
    super();
    this.state = {
      result: "",
      component: "state",
      activeState: true,
      activeDiscount: false,
      courseDetail: [],
    };

    this.handleSelect = this.handleSelect.bind(this);
  }

  componentDidMount = async () => {
    const courseDetail = await CRUDFunction.get(
      `${APIurl.course}name`,
      APIFunction.getHeader()[0]
    );
    if (courseDetail) {
      this.setState({
        courseDetail: courseDetail,
      });
    }
  };

  handleSelect = (event) => {
    if (event === "state") {
      this.setState({
        component: event,
        activeState: true,
        activeDiscount: false,
      });
    } else if (event === "discount") {
      this.setState({
        component: event,
        activeState: false,
        activeDiscount: true,
      });
    }
  };

  render() {
    return (
      <Fragment>
        <Container>
          <br />
          <Row style={{ width: "100%", margin: "0px" }}>
            <Navbar
              onSelect={(event) => {
                this.handleSelect(event);
              }}
              style={{
                background: "#0f1a26",
                display: "flex",
                textAlign: "center",
                justifyContent: "center",
                padding: "0px",
              }}
            >
              <Nav style={{ width: "100%" }}>
                <Col md={6}>
                  <Nav.Item>
                    <Nav.Link eventKey="state" active={this.state.activeState}>
                      <strong>STATE LIST</strong>
                    </Nav.Link>
                  </Nav.Item>
                </Col>
                <Col md={6}>
                  <Nav.Item>
                    <Nav.Link
                      eventKey="discount"
                      active={this.state.activeDiscount}
                    >
                      <strong>DISCOUNT</strong>
                    </Nav.Link>
                  </Nav.Item>
                </Col>
              </Nav>
            </Navbar>
          </Row>

          <br />
          {this.state.component === "state" ? <State /> : ""}
          {this.state.component === "discount" ? (
            <Discount courseDetail={this.state.courseDetail} />
          ) : (
            ""
          )}
        </Container>
      </Fragment>
    );
  }
}

export default SettingPage;
