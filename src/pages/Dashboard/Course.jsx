import React, { Component } from "react";
import { Outlet } from "react-router-dom";

export class Course extends Component {
  render() {
    return (
      <div>
        <Outlet />
      </div>
    );
  }
}

export default Course;
