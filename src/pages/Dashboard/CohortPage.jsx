import React, { Component, Fragment } from "react";
import TableContext from "../../components/Cohort/TableContext";
import { Row, Col, Button, Modal, Dropdown, Form } from "react-bootstrap";
import APIFunction from "../../functions/APIFunction";
import CRUDFunction from "../../functions/CRUDFunction";
import APIurl from "../../api/AppURL";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Swal from "sweetalert2";
import DateRangePicker from "react-bootstrap-daterangepicker";
import "bootstrap-daterangepicker/daterangepicker.css";

class CohortPage extends Component {
  constructor() {
    super();
    this.state = {
      result: "",
      cohort: false,
      newdata: [],
      show: false,
      courseDetail: [],
      cohortData: [],
      header: [
        "NO",
        "COHORT NAME",
        "TOTAL TRAINEE",
        "PLACE",
        "START DATE",
        "END DATE",
        "ACTION",
      ],
      dataModal: [],
      dateRangeInput: 0,
      dateAdded: false,
      id: "",
      isDataExist: false,
      state_data: [],
      physicalChecked: false,
      onlineChecked: false,
    };
    this.addCohort = this.addCohort.bind(this);
    this.updateCohort = this.updateCohort.bind(this);
    this.deleteCohort = this.deleteCohort.bind(this);
    this.getCohortbyID = this.getCohortbyID.bind(this);
  }

  getCohortbyID = async (e, id) => {
    const dataModal = await CRUDFunction.get(
      `${APIurl.cohort}/${id}`,
      APIFunction.getHeader()[0]
    );
    if (dataModal) {
      this.setState({
        dataModal: dataModal,
        showModal: true,
        id: id,
      });
    }
  };

  addCohort = async (event) => {
    event.preventDefault();
    console.log(this.state.newdata);
    // const newdata = await CRUDFunction.create(
    //   event,
    //   APIurl.cohort,
    //   this.state.newdata,
    //   APIFunction.getHeader()[0]
    // );
    // if (newdata) {
    //   console.log(newdata);
    //   this.setState({
    //     cohortData: [newdata],
    //     show: false,
    //     cohort: false,
    //   });
    // }
  };

  updateCohort = async (event, formdata) => {
    const data = await CRUDFunction.update(
      event,
      `${APIurl.cohort}/${this.state.id}`,
      formdata,
      APIFunction.getHeader()[0]
    );
    if (data) {
      console.log("data after update: ", data);
      this.setState({
        showModal: false,
        cohortData: [data],
      });
    }
  };

  deleteCohort = async (event, id) => {
    event.preventDefault();
    const data = await CRUDFunction.delete(
      `${APIurl.cohort}/${id}`,
      APIFunction.getHeader()[0]
    );
    if (data) {
      Swal.fire({
        title: "Are you sure?",
        text: "You want delete your item?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!",
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire("Deleted!", "Your file has been deleted.", "success");
          this.setState({
            cohortData: [data],
          });
        }
      });
    }
  };

  componentDidMount = async () => {
    const courseDetail = await CRUDFunction.get(
      `${APIurl.course}name`,
      APIFunction.getHeader()[0]
    );
    if (courseDetail) {
      console.log(courseDetail);
      this.setState({
        courseDetail: courseDetail,
      });
    }
    const cohortData = await CRUDFunction.get(
      `${APIurl.cohort}`,
      APIFunction.getHeader()[0]
    );
    if (cohortData) {
      console.log(cohortData);
      this.setState({
        isDataExist: true,
        cohortData: [cohortData.data],
      });
    }

    const stateData = await CRUDFunction.get(
      `${APIurl.state}`,
      APIFunction.getHeader()[0]
    );
    if (stateData) {
      this.setState({
        state_data: stateData.data.map((elem) => (
          <option value={elem.state_name}>
            {elem.state_code} - {elem.state_name}
          </option>
        )),
      });
    }
  };

  closeModal = () => {
    this.setState({
      showModal: false,
    });
  };

  handleApply = (event) => {
    console.log(event.target.value);
  };

  render() {
    let dateObject = [];
    for (let a = 0; a < this.state.dateRangeInput; a++) {
      dateObject.push(
        <div style={{ marginTop: "10px" }}>
          <DateRangePicker onApply={this.handleApply}>
            <Form.Control type="text" name="cohort_estimate_date" />
          </DateRangePicker>
        </div>
      );
    }

    return (
      <Fragment>
        <br />
        <Row>
          <Col md={6} lg={6}>
            <Form.Label>SELECT COURSE</Form.Label>
            <Form.Select aria-label="Default select example">
              <option>Open this select menu</option>
              {this.state.courseDetail.map((elem) => (
                <option value={elem.id}>{elem.course_name}</option>
              ))}
            </Form.Select>
          </Col>
          <Col md={6} lg={6}>
            <Form.Label>YEAR</Form.Label>
            <Form.Select aria-label="Default select example">
              <option>Open this select menu</option>
              <option value="1">2021</option>
              <option value="2">2020</option>
              <option value="3">2019</option>
            </Form.Select>
          </Col>
        </Row>
        <br />
        <Row>
          <div
            style={{
              display: "flex",
              justifyContent: "flex-end",
            }}
          >
            <Button
              onClick={() => {
                this.setState({ show: true, cohort: true });
              }}
              className="primary"
            >
              <FontAwesomeIcon icon={["fas", "plus"]} color="white" size="sm" />
              &nbsp; Add Cohort
            </Button>
          </div>
        </Row>
        <br />
        {this.state.cohort && (
          <Modal
            show={this.state.show}
            onHide={() => {
              this.setState({
                show: false,
              });
            }}
          >
            <Form onSubmit={this.addCohort}>
              <Modal.Header closeButton>
                <Modal.Title>ADD COHORT</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <Form.Group
                  className="mb-3"
                  onChange={(e) => {
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        [e.target.name]: parseInt(e.target.value),
                      },
                    });
                  }}
                >
                  <Form.Label>COURSE</Form.Label>
                  <Form.Select
                    aria-label="Default select course"
                    name="course_id"
                  >
                    <option>Select course</option>
                    {this.state.courseDetail.map((elem) => (
                      <option value={elem.id}>{elem.course_name}</option>
                    ))}
                  </Form.Select>
                </Form.Group>
                <Form.Group
                  className="mb-3"
                  onChange={(e) => {
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        [e.target.name]: e.target.value,
                      },
                    });
                  }}
                >
                  <Form.Label>COHORT NAME</Form.Label>
                  <Form.Control
                    type="text"
                    name="cohort_name"
                    placeholder="Enter cohort name"
                  />
                </Form.Group>
                <Form.Group
                  className="mb-3"
                  onChange={(e) => {
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        [e.target.name]: e.target.value,
                      },
                    });
                  }}
                >
                  <Form.Label>STATE</Form.Label>
                  <Form.Select
                    aria-label="Default select state"
                    name="cohort_state"
                  >
                    <option value="0">Open this select menu</option>
                    {this.state.state_data}
                  </Form.Select>
                </Form.Group>
                <Form.Group
                  className="mb-3"
                  onChange={(e) => {
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        [e.target.name]: e.target.value,
                      },
                    });
                  }}
                >
                  <Form.Label>COHORT PLACE</Form.Label>
                  <Form.Control
                    placeholder="Enter cohort place"
                    name="cohort_place"
                  />
                </Form.Group>
                <Form.Group
                  className="mb-3"
                  onChange={(e) => {
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        [e.target.name]: e.target.value,
                      },
                    });
                  }}
                >
                  <Form.Label>COHORT ADDRESS</Form.Label>
                  <Form.Control
                    as="textarea"
                    rows={2}
                    name="cohort_address"
                    placeholder="Enter cohort address"
                  />
                </Form.Group>
                <Row>
                  <Col>
                    <Form.Group
                      inline
                      className="mb-3"
                      onChange={(e) => {
                        this.setState({
                          newdata: {
                            ...this.state.newdata,
                            [e.target.name]: e.target.value,
                          },
                        });
                      }}
                    >
                      <Form.Label>COHORT DATE</Form.Label>
                      <DateRangePicker
                        initialSettings={{
                          startDate: new Date(),
                        }}
                        onApply={this.handleApply}
                      >
                        <Form.Control type="text" name="cohort_estimate_date" />
                      </DateRangePicker>
                      {dateObject}
                      <div style={{ margin: "10px 0px" }}>
                        <Button
                          style={{ float: "left" }}
                          onClick={() => {
                            this.setState({
                              dateRangeInput: this.state.dateRangeInput + 1,
                              dateAdded: true,
                            });
                          }}
                        >
                          Add Date
                        </Button>
                        <Button
                          style={{ float: "right" }}
                          variant="danger"
                          onClick={() => {
                            this.setState({
                              dateRangeInput: this.state.dateRangeInput - 1,
                              dateAdded: true,
                            });
                          }}
                        >
                          Remove Date
                        </Button>
                      </div>
                    </Form.Group>
                  </Col>
                </Row>
                <br />
                <Form.Group
                  className="mb-3"
                  onChange={(e) => {
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        [e.target.name]: e.target.value,
                      },
                    });
                  }}
                >
                  <Form.Label>COHORT INCLUDE</Form.Label>
                  <Form.Control
                    as="textarea"
                    rows={2}
                    name="cohort_include"
                    placeholder="Enter cohort include"
                  />
                </Form.Group>
                <Form.Group
                  className="mb-3"
                  onChange={(e) => {
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        [e.target.name]: e.target.value,
                      },
                    });
                  }}
                >
                  <Form.Label>COHORT DESCRIPTION</Form.Label>
                  <Form.Control
                    as="textarea"
                    rows={3}
                    name="cohort_desc"
                    placeholder="Enter cohort details"
                  />
                </Form.Group>
                <Form.Group
                  className="mb-3"
                  onChange={(e) => {
                    if (e.target.checked == true && e.target.name == "online") {
                      this.setState({
                        physicalChecked: false,
                        onlineChecked: true,
                      });
                    } else if (
                      e.target.checked == true &&
                      e.target.name == "physical"
                    ) {
                      this.setState({
                        physicalChecked: true,
                        onlineChecked: false,
                      });
                    }
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        cohort_mode: e.target.name,
                      },
                    });
                  }}
                >
                  <Form.Label>COHORT MODE &nbsp;</Form.Label>
                  <Form.Check
                    inline
                    label="ONLINE"
                    name="online"
                    type="radio"
                    checked={this.state.onlineChecked}
                  />
                  <Form.Check
                    inline
                    label="PHYSICAL"
                    name="physical"
                    type="radio"
                    checked={this.state.physicalChecked}
                  />
                </Form.Group>
              </Modal.Body>

              <Modal.Footer>
                <Button
                  variant="primary"
                  onClick={() =>
                    this.setState({
                      show: false,
                      cohort: false,
                    })
                  }
                >
                  Close
                </Button>
                <Button variant="primary" type="submit">
                  Add
                </Button>
              </Modal.Footer>
            </Form>
          </Modal>
        )}
        <Row>
          <Col md={12}>
            <TableContext
              header={this.state.header}
              cohortData={this.state.cohortData}
              showModal={this.state.showModal}
              dataModal={this.state.dataModal}
              isDataExist={this.state.isDataExist}
              stateData={this.state.state_data}
              updateCohort={this.updateCohort}
              getCohortbyID={this.getCohortbyID}
              closeModal={this.closeModal}
              deleteCohort={this.deleteCohort}
            ></TableContext>
          </Col>
        </Row>
      </Fragment>
    );
  }
}

export default CohortPage;
