import React, { Component, Fragment } from "react";
import {
  Row,
  Col,
  Button,
  Modal,
  Container,
  Nav,
  Navbar,
} from "react-bootstrap";
import { Form } from "semantic-ui-react";
import "../../assets/css/customTab.css";
import { withRouter } from "../../functions/withRouter";
import CohortInfo from "../../components/Cohort/CohortInfo";
import TraineeList from "../../components/Cohort/TraineeList";
import TraineeAttendance from "../../components/Cohort/TraineeAttendance";

class CohortDetailsPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      result: "",
      component: "info",
      activeInfo: true,
      activeList: false,
      activeAttendance: false,
    };
    this.handleSelect = this.handleSelect.bind(this);
  }

  handleSelect = (event) => {
    if (event === "list") {
      this.setState({
        component: event,
        activeList: true,
        activeInfo: false,
        activeAttendance: false,
      });
    } else if (event === "info") {
      this.setState({
        component: event,
        activeInfo: true,
        activeList: false,
        activeAttendance: false,
      });
    } else {
      this.setState({
        component: event,
        activeAttendance: true,
        activeInfo: false,
        activeList: false,
      });
    }
  };

  render() {
    return (
      <Fragment>
        <Container>
          <br />
          <Row style={{ width: "100%", margin: "0px" }}>
            <Navbar
              onSelect={(event) => {
                this.handleSelect(event);
              }}
              style={{
                background: "#0f1a26",
                display: "flex",
                textAlign: "center",
                justifyContent: "center",
                padding: "0px",
              }}
            >
              <Nav style={{ width: "100%" }}>
                <Col md={4}>
                  <Nav.Item>
                    <Nav.Link eventKey="info" active={this.state.activeInfo}>
                      <strong>INFO</strong>
                    </Nav.Link>
                  </Nav.Item>
                </Col>
                <Col md={4}>
                  <Nav.Item>
                    <Nav.Link eventKey="list" active={this.state.activeList}>
                      <strong>TRAINEE LIST</strong>
                    </Nav.Link>
                  </Nav.Item>
                </Col>
                <Col md={4}>
                  <Nav.Item>
                    <Nav.Link
                      eventKey="attendance"
                      active={this.state.activeAttendance}
                    >
                      <strong>TRAINEE ATTENDANCE</strong>
                    </Nav.Link>
                  </Nav.Item>
                </Col>
              </Nav>
            </Navbar>
          </Row>

          <br />
          {this.state.component === "info" ? (
            <CohortInfo id={this.props.params.id} />
          ) : (
            ""
          )}
          {this.state.component === "list" ? (
            <TraineeList id={this.props.params.id} />
          ) : (
            ""
          )}
          {this.state.component === "attendance" ? (
            <TraineeAttendance id={this.props.params.id} />
          ) : (
            ""
          )}

          <br />
          {this.state.trainee && (
            <Modal
              show={this.state.show}
              onHide={() =>
                this.setState({
                  show: false,
                  trainee: false,
                  attendance: false,
                  print: false,
                })
              }
            >
              <Modal.Header closeButton>
                <Modal.Title>Trainee Form</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <Form.Input fluid id="name" placeholder="Name" />
              </Modal.Body>
              <Col>
                {" "}
                <Modal.Body>
                  <Form.Input fluid id="position" placeholder="Position" />
                </Modal.Body>
              </Col>

              <Modal.Body>
                <Form.Input fluid id="icno" placeholder="IC No" />
              </Modal.Body>
              <Modal.Body>
                <Form.Input fluid id="phoneno" placeholder="Phone No" />
              </Modal.Body>
              <Modal.Body>
                <Form.Input fluid id="email" placeholder="Email" />
              </Modal.Body>
              <Modal.Body>
                <Form.Input fluid id="company" placeholder="Company" />
              </Modal.Body>
              <Modal.Body>
                <Form.Input fluid id="phoneno" placeholder="Address" />
              </Modal.Body>
              <Modal.Body>
                <Form.Input fluid id="promocode" placeholder="Promo Code" />
              </Modal.Body>
              <Modal.Body>
                <Form.Input
                  fluid
                  id="special"
                  placeholder="Special Dietary Needs/ Allergies"
                />
              </Modal.Body>
              <Modal.Body>
                <Form.Input fluid id="referrer" placeholder="Referrer" />
              </Modal.Body>

              <Modal.Footer>
                <Button
                  variant="primary"
                  onClick={() =>
                    this.setState({
                      show: false,
                      trainee: false,
                      attendance: false,
                      print: false,
                    })
                  }
                >
                  Close
                </Button>
                <Button
                  variant="primary"
                  onClick={() =>
                    this.setState({
                      show: false,
                      trainee: false,
                      attendance: false,
                      print: false,
                    })
                  }
                >
                  Add
                </Button>
              </Modal.Footer>
            </Modal>
          )}
          {this.state.indicator && (
            <Modal
              show={this.state.show}
              onHide={() =>
                this.setState({
                  show: false,
                  indicator: false,
                  trainee: false,
                  print: false,
                })
              }
            >
              <Modal.Header closeButton>
                <Modal.Title>Attendance Indicator</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <Form.Input
                  fluid
                  icon="clock"
                  id="code"
                  iconPosition="left"
                  placeholder="Code"
                />
              </Modal.Body>{" "}
              <Modal.Body>
                <Form.Input
                  fluid
                  icon="clock"
                  id="notes"
                  iconPosition="left"
                  placeholder="Notes"
                />
              </Modal.Body>{" "}
              <Modal.Footer>
                <Button
                  variant="primary"
                  onClick={() =>
                    this.setState({
                      show: false,
                      discount: false,
                      stateList: false,
                    })
                  }
                >
                  Close
                </Button>
                <Button
                  variant="primary"
                  onClick={() =>
                    this.setState({
                      show: false,
                      indicator: false,
                      trainee: false,
                      print: false,
                    })
                  }
                  onClick={this.alert}
                >
                  Add
                </Button>
              </Modal.Footer>
            </Modal>
          )}

          {this.state.print && (
            <Modal
              show={this.state.show}
              onHide={() =>
                this.setState({
                  show: false,
                  print: false,
                  indicator: false,
                  trainee: false,
                })
              }
            >
              <Modal.Header closeButton>
                <Modal.Title>Template Print for Attendance</Modal.Title>
              </Modal.Header>
              <Modal.Footer>
                <Button
                  variant="primary"
                  onClick={() =>
                    this.setState({
                      show: false,
                      print: false,
                      indicator: false,
                      trainee: false,
                    })
                  }
                >
                  Close
                </Button>
                <Button
                  variant="primary"
                  onClick={() =>
                    this.setState({
                      show: false,
                      print: false,
                      indicator: false,
                      trainee: false,
                    })
                  }
                  onClick={this.alert}
                >
                  Add
                </Button>
              </Modal.Footer>
            </Modal>
          )}
        </Container>
      </Fragment>
    );
  }
}

// export default CohortDetailsPage;

export default withRouter(CohortDetailsPage);
