import React, { Component, Fragment } from "react";
import { Button, Table, Form, Modal } from "react-bootstrap";
import APIFunction from "../../functions/APIFunction";
import CRUDFunction from "../../functions/CRUDFunction";
import APIurl from "../../api/AppURL";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export class CourseDetailsPage extends Component {
  constructor() {
    super();
    this.state = {
      courseData: [],
      attachment: [],
    };
    this.add = this.add.bind(this);
  }

  componentDidMount = async () => {
    const courseData = await CRUDFunction.get(
      `${APIurl.course}/${this.props.id}`,
      APIFunction.getHeader()[0]
    );
    if (courseData) {
      console.log(courseData);
      this.setState({
        courseData: [courseData],
      });
    }
  };

  add = (event) => {
    event.preventDefault();
    console.log("hai");
    console.log(event);
    console.log(this.state.attachment);
  };

  render() {
    return (
      <Fragment>
        <h3 style={{ textAlign: "center" }}>
          {" "}
          Professional Certificate for Halal Executive (PCHE){" "}
        </h3>
        <div
          style={{
            display: "flex",
            justifyContent: "flex-end",
            flexDirection: "row",
            width: "100%",
          }}
        >
          <Button
            className="primary"
            onClick={() => this.setState({ show: true, traineeForm: true })}
          >
            <FontAwesomeIcon icon={["fas", "plus"]} color="white" size="sm" />{" "}
            &nbsp; Add Attachment
          </Button>
        </div>

        <br />

        <div class="bg-darkgreen p-15-20">
          <h3>ATTACHMENT LIST</h3>
        </div>
        <Table
          hover
          responsive
          size="lg"
          style={{
            textAlign: "center",
            boxShadow: "0 0 10px 0 rgba(0, 0, 0, 0.2)",
          }}
        >
          <thead style={{ background: "#468C32", color: "white" }}>
            <tr>
              <th>NO</th>
              <th>ATTACHMENT DESCRIPTION</th>
              <th>ATTACHMENT TYPE</th>
              <th>ATTACHMENT FILE</th>
              <th>ACTION</th>
            </tr>
          </thead>
        </Table>

        <Modal
          show={this.state.show}
          onHide={() =>
            this.setState({
              show: false,
            })
          }
        >
          <Form onSubmit={this.add}>
            <Modal.Header closeButton>
              <Modal.Title>Add Attachment</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form.Group
                className="mb-3"
                onChange={(e) => {
                  this.setState({
                    attachment: {
                      ...this.state.attachment,
                      [e.target.name]: e.target.value,
                    },
                  });
                }}
              >
                <Form.Label>Attachment Description</Form.Label>
                <Form.Control
                  as="textarea"
                  type="text"
                  name="attach_desc"
                  placeholder="Enter attachment description"
                />
              </Form.Group>
              <Form.Group
                className="mb-3"
                onChange={(e) => {
                  this.setState({
                    attachment: {
                      ...this.state.attachment,
                      [e.target.name]: e.target.value,
                    },
                  });
                }}
              >
                <Form.Label class="required">Attachment Type</Form.Label>
                <Form.Select
                  name="attach_type"
                  aria-label="Default select example"
                >
                  <option value="0">Open this select menu</option>
                  <option value="1">HRDC Document</option>
                  <option value="2">Email</option>
                  {this.state.company_list}
                </Form.Select>
              </Form.Group>
              <Form.Group
                className="mb-3"
                onChange={(e) => {
                  this.setState({
                    attachment: {
                      ...this.state.attachment,
                      [e.target.name]: e.target.value,
                    },
                  });
                }}
              >
                <Form.Label>Attachment File</Form.Label>
                <Form.Control
                  type="file"
                  name="attach_file"
                  placeholder="Enter attachment file"
                />
              </Form.Group>
            </Modal.Body>

            <Modal.Footer>
              <Button
                variant="primary"
                onClick={() =>
                  this.setState({
                    show: false,
                  })
                }
              >
                Close
              </Button>
              <Button variant="primary" type="submit">
                Add
              </Button>
            </Modal.Footer>
          </Form>
        </Modal>
      </Fragment>
    );
  }
}

export default CourseDetailsPage;
