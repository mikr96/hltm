import React, { Component, Fragment } from "react";
import { Row, Col, Button, Form, Modal } from "react-bootstrap";
import { Navigate } from "react-router";
import CardContext from "../../components/Dashboard/CardContext";
import TableContext from "../../components/Dashboard/TableContext";
import TableStaff from "../../components/Staff/TableStaff";
import APIurl from "../../api/AppURL";
import APIFunction from "../../functions/APIFunction";
import Calendar from "react-calendar";
import "../../assets/css/calendar.css";
import CRUDFunction from "../../functions/CRUDFunction";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Swal from "sweetalert2";

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      role: "",
      showModal: false,
      show: false,
      dataModal: [],
      dashboard: true,
      cohortDetail: [],
      data: {
        no: "",
        cohort_name: "",
        total_cohortee: "",
        status: "",
        date: "",
        action: "",
      },
      header: ["NO", "COHORT NAME", "DATE", "STATUS"],
      headerStaff: ["NO", "NAME", "IC", "EMAIL", "PHONE NO", "ACTION"],
      date: [],
      cohort: [],
      newStaffData: [],
      staffData: [],
      staff: false,
      company_id: "",
      isDataExist: false,
      isStaffExist: false,
    };
    this.addStaff = this.addStaff.bind(this);
    this.deleteStaff = this.deleteStaff.bind(this);
    this.updateStaff = this.updateStaff.bind(this);
    this.getStaffbyID = this.getStaffbyID.bind(this);
  }

  content = ({ date, view }) => {
    let a = this.state.date.findIndex((e) => this.formatDate(date) == e);
    if (a > 0) {
      return <p>{this.state.cohort[a]}</p>;
    }
  };

  formatDate = (date) => {
    let d = new Date(date),
      month = "" + (d.getMonth() + 1),
      day = "" + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;

    return [year, month, day].join("-");
  };

  componentDidMount = async () => {
    this.setState({
      role: JSON.parse(localStorage.getItem("user")).role[0].role_type,
      company_id: JSON.parse(localStorage.getItem("user")).company_id,
    });
    const courseData = await CRUDFunction.get(
      `${APIurl.course}`,
      APIFunction.getHeader()[0]
    );
    if (courseData) {
      this.setState({
        data: courseData.data.data,
      });
    }

    const cohortDetail = await CRUDFunction.get(
      `${APIurl.cohort}`,
      APIFunction.getHeader()[0]
    );

    if (cohortDetail) {
      console.log(cohortDetail.data.length);
      if (cohortDetail.data.length > 0) {
        this.setState({
          isDataExist: true,
          cohortDetail: [cohortDetail.data],
        });
      } else {
        this.setState({
          isDataExist: false,
        });
      }
    }

    const cohortAllDate = await CRUDFunction.get(
      `${APIurl.cohort}alldate`,
      APIFunction.getHeader()[0]
    );

    if (cohortAllDate) {
      let allDate = [],
        allName = [];
      cohortAllDate.map((e) => {
        let dateInBetween = this.getDateInBetween(
          e.cohort_date_start,
          e.cohort_date_end
        );
        for (let n of dateInBetween) {
          allDate.push(n);
          allName.push(e.cohort_name);
        }
      });
      this.setState({
        date: allDate,
        cohort: allName,
      });

      const staffDetail = await CRUDFunction.get(
        `${APIurl.staff}`,
        APIFunction.getHeader()[0]
      );

      if (staffDetail) {
        console.log(staffDetail.data.length);
        if (staffDetail.data.length > 0) {
          this.setState({
            isStaffExist: true,
            staffData: [staffDetail.data],
          });
        } else {
          this.setState({
            isStaffExist: false,
          });
        }
      }
    }
  };

  getDateInBetween = (start_date, end_date) => {
    let daylist = this.getDaysArray(new Date(start_date), new Date(end_date));
    return daylist.map((v) => this.formatDate(v));
  };

  getDaysArray = (start, end) => {
    for (
      var arr = [], dt = new Date(start);
      dt <= end;
      dt.setDate(dt.getDate() + 1)
    ) {
      arr.push(new Date(dt));
    }
    return arr;
  };

  addStaff = async (event, formData) => {
    event.preventDefault();
    console.log("newStaffData:", formData);

    const newStaffData = await CRUDFunction.create(
      event,
      APIurl.staff,
      { ...formData, company_id: this.state.company_id },
      APIFunction.getHeader()[0]
    );
    if (newStaffData) {
      console.log(newStaffData);
      this.setState({
        staffData: [newStaffData],
        show: false,
        staff: false,
      });
    }
  };

  updateStaff = async (event, newStaffdata) => {
    const response = await CRUDFunction.update(
      event,
      `${APIurl.staff}/${this.state.id}`,
      newStaffdata,
      APIFunction.getHeader()[0]
    );
    if (response) {
      console.log(response);
      this.setState({
        showModal: false,
        staffData: [response],
      });
    }
  };

  getStaffbyID = async (e, id) => {
    localStorage.setItem("id", id);
    const dataModal = await CRUDFunction.get(
      `${APIurl.staff}/${id}`,
      APIFunction.getHeader()[0]
    );
    if (dataModal) {
      this.setState({
        dataModal: {
          staff_name: dataModal.staff_name,
          staff_ic: dataModal.staff_ic,
          staff_phoneno: dataModal.staff_phoneno,
          staff_email: dataModal.staff_email,
        },
        showModal: true,
        id: id,
      });
    }
  };

  deleteStaff = async (event, id) => {
    event.preventDefault();
    const data = await CRUDFunction.delete(
      `${APIurl.staff}/${id}`,
      APIFunction.getHeader()[0]
    );
    if (data) {
      Swal.fire({
        title: "Are you sure?",
        text: "You want delete your item?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!",
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire("Deleted!", "Your file has been deleted.", "success");
          this.setState({
            staffData: [data],
          });
        }
      });
    }
  };
  closeModal = () => {
    this.setState({
      showModal: false,
    });
  };

  render() {
    if (JSON.parse(localStorage.getItem("user")).role == "") {
      return <Navigate to={"/"} />;
    } else {
      return (
        <Fragment>
          <br />
          {this.state.role == "admin" && (
            <Row>
              <Col md={3}>
                <CardContext
                  title="Total Number of Participant Registered"
                  content="10"
                ></CardContext>
              </Col>
              <Col md={3}>
                <CardContext
                  title="Total Number of Participant Registered"
                  content="10"
                ></CardContext>
              </Col>
              <Col md={3}>
                <CardContext
                  title="Total Number of Participant Registered"
                  content="10"
                ></CardContext>
              </Col>
              <Col md={3}>
                <CardContext
                  title="Total Number of Participant Registered"
                  content="10"
                ></CardContext>
              </Col>
            </Row>
          )}

          {this.state.role == "company" && (
            <div>
              <Row style={{ marginLeft: "0px", marginRight: "0px" }}>
                <Col md={6}>
                  <h3 style={{ lineHeight: "2" }}>STAFF LIST</h3>
                </Col>
                <Col md={6} style={{ paddingRight: "0px" }}>
                  <div
                    style={{
                      width: "100%",
                      float: "right",
                      display: "flex",
                      justifyContent: "flex-end",
                      paddingRight: "0px",
                    }}
                  >
                    <Button
                      onClick={() => {
                        this.setState({ show: true });
                      }}
                      className="primary"
                    >
                      <FontAwesomeIcon
                        icon={["fas", "plus"]}
                        color="white"
                        size="sm"
                      />
                      &nbsp; Add Staff
                    </Button>
                  </div>
                </Col>
              </Row>
              <TableStaff
                headerStaff={this.state.headerStaff}
                staffData={this.state.staffData}
                deleteStaff={this.deleteStaff}
                updateStaff={this.updateStaff}
                showModal={this.state.showModal}
                dataModal={this.state.dataModal}
                getStaffbyID={this.getStaffbyID}
                closeModal={this.closeModal}
                role="company"
                isStaffExist={this.state.isStaffExist}
              />
            </div>
          )}
          <br />
          <Row>
            <h3>COHORT LIST</h3>
            <TableContext
              header={this.state.header}
              cohortDetail={this.state.cohortDetail}
              isDataExist={this.state.isDataExist}
              role="admin"
            />
          </Row>
          <br />
          <Row>
            <Calendar
              onChange={this.onChange}
              calendarType="Hebrew"
              tileContent={this.content}
            />
          </Row>
          <br />
          <Modal
            show={this.state.show}
            onHide={() =>
              this.setState({
                show: false,
              })
            }
          >
            <Modal.Header closeButton>
              <Modal.Title>Staff Form</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form.Group
                className="mb-3"
                onChange={(e) => {
                  this.setState({
                    newStaffData: {
                      ...this.state.newStaffData,
                      [e.target.name]: e.target.value,
                    },
                  });
                }}
              >
                <Form.Label>Name</Form.Label>
                <Form.Control
                  type="text"
                  name="staff_name"
                  placeholder="Enter Your Name"
                />
              </Form.Group>
              <Form.Group
                className="mb-3"
                onChange={(e) => {
                  this.setState({
                    newStaffData: {
                      ...this.state.newStaffData,
                      [e.target.name]: e.target.value,
                    },
                  });
                }}
              >
                <Form.Label>IC No</Form.Label>
                <Form.Control
                  type="text"
                  name="staff_ic"
                  placeholder="Enter Your IC No"
                />
              </Form.Group>
              <Form.Group
                className="mb-3"
                onChange={(e) => {
                  this.setState({
                    newStaffData: {
                      ...this.state.newStaffData,
                      [e.target.name]: e.target.value,
                    },
                  });
                }}
              >
                <Form.Label>Phone No</Form.Label>
                <Form.Control
                  type="text"
                  name="staff_phoneno"
                  placeholder="Enter Your Phone No"
                />
              </Form.Group>
              <Form.Group
                className="mb-3"
                onChange={(e) => {
                  this.setState({
                    newStaffData: {
                      ...this.state.newStaffData,
                      [e.target.name]: e.target.value,
                    },
                  });
                }}
              >
                <Form.Label>Email</Form.Label>
                <Form.Control
                  type="text"
                  name="staff_email"
                  placeholder="Enter Your Email"
                />
              </Form.Group>
            </Modal.Body>
            <Modal.Footer>
              <Button
                variant="primary"
                onClick={() =>
                  this.setState({
                    show: false,
                  })
                }
              >
                Close
              </Button>
              <Button
                variant="primary"
                onClick={(event) =>
                  this.addStaff(event, this.state.newStaffData)
                }
              >
                Add
              </Button>
            </Modal.Footer>
          </Modal>
        </Fragment>
      );
    }
  }
}

export default Dashboard;
