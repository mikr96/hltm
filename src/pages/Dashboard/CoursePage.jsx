import React, { Component, Fragment } from "react";
import CardContext from "../../components/Dashboard/CardContext";
import TableContext from "../../components/Course/TableContext";
import { Row, Col, Button, Modal, Form } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import APIurl from "../../api/AppURL";
import APIFunction from "../../functions/APIFunction";
import CRUDFunction from "../../functions/CRUDFunction";
import Swal from "sweetalert2";

class CoursePage extends Component {
  constructor() {
    super();
    this.state = {
      show: false,
      stateList: false,
      data: [],
      newdata: [],
      dataModal: [],
      showModal: false,
      id: "",
      header: ["NO", "COURSE NAME", "DESCRIPTION", "FEE", "IMAGE", "ACTION"],
      isDataExist: false,
    };

    this.deleteCourse = this.deleteCourse.bind(this);
    this.updateCourse = this.updateCourse.bind(this);
    this.getCoursebyID = this.getCoursebyID.bind(this);
    this.addCourse = this.addCourse.bind(this);
  }

  componentDidMount = async () => {
    const courseDetail = await CRUDFunction.get(
      `${APIurl.course}`,
      APIFunction.getHeader()[0]
    );
    if (courseDetail) {
      this.setState({
        isDataExist: true,
        data: [courseDetail.data],
      });
    }
  };

  addCourse = async (event) => {
    const newdata = await CRUDFunction.create(
      event,
      APIurl.course,
      this.state.newdata,
      APIFunction.getHeader()[0]
    );
    if (newdata) {
      console.log(newdata);
      this.setState({
        data: [newdata],
        show: false,
        cohort: false,
      });
    }
  };

  updateCourse = async (event, formdata) => {
    const data = await CRUDFunction.update(
      event,
      `${APIurl.course}/${this.state.id}`,
      formdata,
      APIFunction.getHeader()[0]
    );
    if (data) {
      this.setState({
        showModal: false,
        data: [data],
      });
    }
  };

  deleteCourse = async (event, id) => {
    event.preventDefault();
    const data = await CRUDFunction.delete(
      `${APIurl.course}/${id}`,
      APIFunction.getHeader()[0]
    );
    if (data) {
      Swal.fire({
        title: "Are you sure?",
        text: "You want delete your item?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!",
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire("Deleted!", "Your file has been deleted.", "success");
          this.setState({
            data: [data],
          });
        }
      });
    }
  };

  getCoursebyID = async (e, id) => {
    localStorage.setItem("id", id);
    const dataModal = await CRUDFunction.get(
      `${APIurl.course}/${id}`,
      APIFunction.getHeader()[0]
    );
    if (dataModal) {
      this.setState({
        dataModal: {
          course_name: dataModal.data.course_name,
          course_desc: dataModal.data.course_desc,
          course_fee: dataModal.data.course_fee,
          max_student: dataModal.data.max_student,
          course_image: dataModal.data.course_image,
        },
        showModal: true,
        id: id,
      });
    }
  };

  closeModal = () => {
    this.setState({
      showModal: false,
    });
  };

  render = () => {
    return (
      <Fragment>
        <Row>
          <Col md={3}>
            <CardContext
              title="Total Number of Participant Registered"
              content="10"
            ></CardContext>
          </Col>
          <Col md={3}>
            <CardContext
              title="Total Number of Participant Registered"
              content="10"
            ></CardContext>
          </Col>
          <Col md={3}>
            <CardContext
              title="Total Number of Participant Registered"
              content="10"
            ></CardContext>
          </Col>
          <Col md={3}>
            <CardContext
              title="Total Number of Participant Registered"
              content="10"
            ></CardContext>
          </Col>
        </Row>
        <Row>
          <div
            style={{
              display: "flex",
              justifyContent: "flex-end",
            }}
          >
            <Button
              onClick={() => {
                this.setState({ show: true, stateList: true });
              }}
              className="primary"
            >
              <FontAwesomeIcon icon={["fas", "plus"]} color="white" size="sm" />
              &nbsp; Add Course
            </Button>
          </div>
        </Row>
        {this.state.stateList && (
          <Modal
            show={this.state.show}
            onHide={() => {
              this.setState({
                show: false,
              });
            }}
          >
            <Modal.Header closeButton>
              <Modal.Title>ADD COURSE</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form>
                <Form.Group
                  className="mb-3"
                  onChange={(e) => {
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        [e.target.name]: e.target.value,
                      },
                    });
                  }}
                >
                  <Form.Label>COURSE NAME</Form.Label>
                  <Form.Control
                    type="string"
                    name="course_name"
                    placeholder="Enter course name"
                  />
                </Form.Group>
                <Form.Group
                  className="mb-3"
                  onChange={(e) => {
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        [e.target.name]: parseInt(e.target.value),
                      },
                    });
                  }}
                >
                  <Form.Label>COURSE FEE</Form.Label>
                  <Form.Control
                    type="number"
                    name="course_fee"
                    placeholder="Enter course fee"
                  />
                </Form.Group>

                <Form.Group
                  className="mb-3"
                  onChange={(e) => {
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        [e.target.name]: e.target.value,
                      },
                    });
                  }}
                >
                  <Form.Label>COURSE DESCRIPTION</Form.Label>
                  <Form.Control
                    as="textarea"
                    rows={3}
                    placeholder="Enter course description"
                    name="course_desc"
                  />
                </Form.Group>
                <Form.Group
                  className="mb-3"
                  onChange={(e) => {
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        [e.target.name]: e.target.value,
                      },
                    });
                  }}
                >
                  <Form.Label>MAX STUDENT</Form.Label>
                  <Form.Control
                    type="number"
                    name="max_student"
                    placeholder="Enter max student"
                  />
                </Form.Group>
                <Form.Group
                  className="mb-3"
                  onChange={(e) => {
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        [e.target.name]: e.target.value,
                      },
                    });
                  }}
                >
                  <Form.Label>COURSE IMAGE</Form.Label>
                  <Form.Control
                    type="string"
                    name="course_image"
                    placeholder="Enter course image URL"
                  />
                </Form.Group>
              </Form>
            </Modal.Body>

            <Modal.Footer>
              <Button
                variant="primary"
                onClick={() =>
                  this.setState({
                    show: false,
                    stateList: false,
                  })
                }
              >
                Close
              </Button>
              <Button variant="primary" onClick={this.addCourse.bind(this)}>
                Add
              </Button>
            </Modal.Footer>
          </Modal>
        )}
        <Row>
          <Col md={12}>
            <TableContext
              header={this.state.header}
              courseData={this.state.data}
              showModal={this.state.showModal}
              dataModal={this.state.dataModal}
              role="admin"
              color="#468C32"
              updateCourse={this.updateCourse}
              getCoursebyID={this.getCoursebyID}
              closeModal={this.closeModal}
              deleteCourse={this.deleteCourse}
              isDataExist={this.state.isDataExist}
            ></TableContext>
          </Col>
        </Row>
      </Fragment>
    );
  };
}

export default CoursePage;
