import React, { Component, Fragment } from "react";
import Biller from "../../components/Invoice/Biller";
import Invoice from "../../components/Invoice/Invoice";
import {
  Table,
  Col,
  Row,
  Container,
  Nav,
  Navbar,
  Image,
} from "react-bootstrap";
import APIFunction from "../../functions/APIFunction";
import CRUDFunction from "../../functions/CRUDFunction";
import APIurl from "../../api/AppURL";
import logo from "../../assets/images/holisticslab.png";

class InvoicePage extends Component {
  constructor() {
    super();
    this.state = {
      result: "",
      component: "biller",
      activeBiller: true,
      activeInvoice: false,
      courseDetail: [],
      companyDetail: [],
    };

    this.handleSelect = this.handleSelect.bind(this);
  }

  handleSelect = (event) => {
    if (event === "biller") {
      this.setState({
        component: event,
        activeBiller: true,
        activeInvoice: false,
      });
    } else if (event === "invoice") {
      this.setState({
        component: event,
        activeBiller: false,
        activeInvoice: true,
      });
    }
  };

  componentDidMount = async () => {
    const cohortDetail = await CRUDFunction.get(
      `${APIurl.cohort}name`,
      APIFunction.getHeader()[0]
    );
    if (cohortDetail) {
      this.setState({
        cohortDetail: cohortDetail,
      });
    }

    const companyDetail = await CRUDFunction.get(
      `${APIurl.company}name`,
      APIFunction.getHeader()[0]
    );
    if (companyDetail) {
      this.setState({
        companyDetail: companyDetail,
      });
    }
  };

  render() {
    return (
      <Fragment>
        <Container>
          <br />
          <Row style={{ width: "100%", margin: "0px" }} id="tab">
            <Navbar
              onSelect={(event) => {
                this.handleSelect(event);
              }}
              style={{
                background: "#0f1a26",
                display: "flex",
                textAlign: "center",
                justifyContent: "center",
                padding: "0px",
              }}
              id="tab"
            >
              <Nav style={{ width: "100%" }}>
                <Col md={6}>
                  <Nav.Item>
                    <Nav.Link
                      eventKey="biller"
                      active={this.state.activeBiller}
                    >
                      <strong>BILLER INFORMATION</strong>
                    </Nav.Link>
                  </Nav.Item>
                </Col>
                <Col md={6}>
                  <Nav.Item>
                    <Nav.Link
                      eventKey="invoice"
                      active={this.state.activeInvoice}
                    >
                      <strong>INVOICE SETTING</strong>
                    </Nav.Link>
                  </Nav.Item>
                </Col>
              </Nav>
            </Navbar>
          </Row>

          <br />
          {this.state.component == "biller" ? <Biller /> : ""}
          {this.state.component == "invoice" ? (
            <Invoice
              companyDetail={this.state.companyDetail}
              cohortDetail={this.state.cohortDetail}
            />
          ) : (
            ""
          )}
        </Container>
      </Fragment>
    );
  }
}

const styles = () => ({
  invoiceBox: {
    maxWidth: "800px",
    margin: "auto",
    padding: "10px 30px",
    border: "1px solid #eee",
    boxShadow: "0 0 10px rgba(0, 0, 0, .15)",
    fontSize: "14px",
    fontFamily: "'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif",
    color: "#555",
  },

  invoiceHeader: {
    width: "860px",
    margin: "0",
    fontFamily: "Arial, sans-serif, 'Helvetica Neue', 'Helvetica', Helvetica",
    borderSpacing: "0",
    border: "1px solid #eee",
    boxShadow: "0 0 10px rgba(0, 0, 0, .15)",
    borderBottom: "0",
    marginBottom: "0px",
    width: "100%",
  },

  invoiceFooter: {
    width: "860px",
    margin: "auto",
    fontFamily: "'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif",
    borderSpacing: "0",
    border: "1px solid #eee",
    boxShadow: "0 0 10px rgba(0, 0, 0, .15)",
    backgroundColor: "#699b12",
  },

  invoiceBox: {
    width: "calc(100% - 60px)",
  },
  invoiceFooter: {
    width: "100%",
    margin: "0",
  },
});
class PrintInvoice extends Component {
  constructor(props) {
    super(props);
  }

  dateDifference = (issued, due) => {
    const date1 = new Date(issued);
    const date2 = new Date(due);
    const diffTime = Math.abs(date2 - date1);
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
    return diffDays + " days";
  };

  render = () => {
    console.log(this.props.cohortDetails);
    if (this.props.isBillerExist) {
      return (
        <div id="printable" style={{ display: "none" }}>
          <br />
          <br />
          <br />
          <Table className={styles.invoiceHeader}>
            <tr>
              <td className="title" width="50%">
                <Image
                  src={logo}
                  style={{
                    width: "100%",
                    maxWidth: "400px",
                    marginTop: "-100px",
                  }}
                />
              </td>

              <td style={{ textAlign: "right" }}>
                <p style={{ padding: "0", margin: "0", fontSize: "30px" }}>
                  <strong>INVOICE</strong>
                </p>
                <p style={{ padding: "0", margin: "0", fontSize: "14px" }}>
                  <strong>
                    {this.props.biller.biller_name}&nbsp;(
                    {this.props.biller.biller_ssm})
                  </strong>
                </p>
                <br />
                <p style={{ padding: "0", margin: "0", fontSize: "14px" }}>
                  {JSON.parse(this.props.biller.biller_address)[0]}
                  <br /> {JSON.parse(this.props.biller.biller_address)[1]}
                  <br />
                  {JSON.parse(this.props.biller.biller_address)[2]}
                  <br />
                  {JSON.parse(this.props.biller.biller_address)[3]}
                </p>
                <br />
                <p>
                  Office : {this.props.biller.biller_phone_no} <br />
                  {this.props.biller.biller_website}
                </p>
              </td>
            </tr>
          </Table>
          <div
            style={{
              height: "15px",
              width: "100%",
              display: "block",
              background: "#699b12",
            }}
            width="100%"
          ></div>
          <br />
          <Table className={styles.invoiceBox}>
            <tr>
              <td width="50%">
                <p>
                  To: <br />
                  <strong>{this.props.companyDetails.company_name}</strong>
                  <br />
                  {this.props.companyDetails.company_address.length > 2020
                    ? JSON.parse(this.props.companyDetails.company_address).map(
                        (e) => (
                          <Fragment>
                            {e}
                            <br />
                          </Fragment>
                        )
                      )
                    : this.props.companyDetails.company_address}
                </p>
              </td>
              <td style={{ textAlign: "left" }} width="15%">
                <p>
                  <strong>Invoice Number</strong>
                  <br />
                  <strong>Invoice Date</strong>
                  <br />
                  <strong>Invoice Payment Due</strong>
                  <br />
                  <strong>Invoice Amount Due</strong>
                </p>
              </td>{" "}
              <td style={{ textAlign: "left" }} width="1%">
                <p>
                  <strong>:</strong>
                  <br />
                  <strong>:</strong>
                  <br />
                  <strong>:</strong>
                  <br />
                  <strong>:</strong>
                  <br />
                </p>
              </td>
              <td style={{ textAlign: "left" }} width="10%">
                <p>
                  {this.props.invoiceDetail[0][this.props.id].invoice_num}
                  <br />
                  {this.props.invoiceDetail[0][this.props.id].invoice_date}
                  <br />
                  {this.dateDifference(
                    this.props.invoiceDetail[0][this.props.id].invoice_date,
                    this.props.invoiceDetail[0][this.props.id].invoice_due
                  )}
                  <br />
                  RM&nbsp;
                  {this.props.invoiceDetail[0][this.props.id].invoice_amount}
                </p>
              </td>
            </tr>
          </Table>
          <Table>
            <thead style={{ background: "#b7efa7", color: "#468c32" }}>
              <th
                width="85%"
                style={{ padding: "10px 30px", fontSize: "15px" }}
              >
                Item
              </th>
              <th
                width="15%"
                style={{
                  padding: "10px",
                  fontSize: "15px",
                  textAlign: "center",
                  border: "1px solid #b7efa7",
                }}
              >
                Price
              </th>
            </thead>
            <tbody
              style={{
                border: "0px",
                color: "#468c32",
                fontSize: "15px",
                border: "1px solid #b7efa7",
              }}
            >
              <tr>
                <td style={{ padding: "10px 30px" }}>
                  <p>
                    <strong>{this.props.cohortDetails.cohort_name}</strong>
                  </p>
                  <p>
                    <strong>Estimate Date:</strong>
                  </p>
                  <ul>
                    <li>11-14 Jan 2021 (Mon - Thu)</li>
                    <li>18-21 Jan 2021 (Mon - Thu)</li>
                  </ul>
                  <p>
                    <strong>Training includes:</strong>
                  </p>
                  <ul>
                    <li>8 days</li>
                    <li>4 hours (Web Conference)</li>
                    <li>4 hours (E-learning)</li>
                    <li>QuikHalal License 3 months</li>
                    <li>E-learning Access (hacademy.my)</li>
                  </ul>
                </td>
                <td style={{ textAlign: "center" }}>
                  RM {this.props.invoiceDetail[0][this.props.id].invoice_amount}
                </td>
              </tr>
              <tr style={{ border: "1px solid #b7efa7" }}>
                <td style={{ padding: "10px 30px" }}></td>
                <td style={{ textAlign: "center" }}>
                  <strong>
                    Total: RM{" "}
                    {this.props.invoiceDetail[0][this.props.id].invoice_amount}
                  </strong>
                </td>
              </tr>
            </tbody>
          </Table>
          <Table>
            <tr>
              <td>
                <p style={{ fontSize: "12px", color: "#468c32" }}>
                  Payment can be made to company account:
                  <br />
                  <br />
                  Account No: 5513-0652-5410 (MAYBANK)
                  <br />
                  Name: HOLISTICS LAB SDN BHD
                  <br />
                  <br />
                  *All cheques must be crossed and made payable to{" "}
                  <strong>"HOLISTICS LAB SDN BHD"</strong>
                  <br />
                  *Courses date are subject to change / extend <br />
                  <br />
                  *Payment must be make before 11 Jan 2021
                </p>
              </td>
            </tr>
          </Table>
          <br />
          <Table class={styles.invoiceFooter}>
            <tr style={{ background: "#699b12" }}>
              <td
                style={{
                  background: "#062730",
                  backgroundImage: "#699b12",
                  color: "white",
                  textAlign: "right",
                  padding: "10px",
                  borderRadius: "0px 20px 20px 0px",
                  paddingRight: "20px",
                  position: "absolute",
                  zIndex: "1",
                  width: "38%",
                }}
              >
                www.holisticslab.my
              </td>

              <td
                style={{
                  background: "#699b12",
                  color: "white",
                  textAlign: "left",
                  padding: "10px",
                  width: "50%",
                }}
              >
                Aspiring Halal Excellence
              </td>
            </tr>
          </Table>
        </div>
      );
    } else {
      return null;
    }
  };
}

class PrintProforma extends Component {
  render = () => {
    return (
      <div id="printable" style={{ display: "none" }}>
        <br />
        <br />
        <br />
        <Table className={styles.invoiceHeader} style={{ marginBottom: "0px" }}>
          <tr>
            <td className="title" width="40%">
              <Image
                src="https://www.holisticslab.my/wp-content/uploads/2022/01/HLSB-1536x1112.png"
                style={{
                  width: "100%",
                  maxWidth: "400px",
                  marginTop: "-70px",
                  marginLeft: "20px",
                }}
              />
            </td>

            <td
              style={{
                textAlign: "right",
                borderRadius: "200px 0px 0px 0px",
                background: "#062730",
                color: "white",
                padding: "0px 30px",
              }}
              width="50%"
            >
              <p style={{ padding: "0", margin: "0", fontSize: "30px" }}>
                <strong>PROFORMA INVOICE</strong>
              </p>
              <p style={{ padding: "0", margin: "0", fontSize: "14px" }}>
                <strong>HOLISTICS LAB Sdn. Bhd. (1151997-T)</strong>
              </p>
              <br />
              <p style={{ padding: "0", margin: "0", fontSize: "14px" }}>
                Invoice #: HL 1/2022
                <br />
                Created: 03/01/2022
              </p>
            </td>
          </tr>
        </Table>
        <div
          style={{
            height: "15px",
            width: "100%",
            display: "block",
            background: "#699b12",
          }}
          width="100%"
        ></div>
        <br />
        <Table className={styles.invoiceBox} style={{ fontSize: "15px" }}>
          <tr>
            <td width="50%">
              <p>
                To: <br />
                <strong>Unison Nutraceuticals Sdn Bhd</strong>
                <br />
                No. 13, Jln TU 52, Taman Tasik Utama, <br />
                75450 Ayer Keroh, Melaka
                <br />
                0175008648
                <br />
                Azira Mohd Rawi
                <br />
                aziramohdrawi@gmail.com
              </p>
            </td>
            <td style={{ textAlign: "left" }} width="30%">
              <p>
                To: <br />
                <strong>Holistics Lab Sdn Bhd</strong>
                <br />
                BG-F-05, Medini 6, <br /> Jalan Medini Sentral 5, Medini, <br />
                79250 Nusajaya, Johor
                <br />
                Office : 019-7765075 <br />
                www.holisticslab.my
              </p>
            </td>{" "}
            <br />
          </tr>
        </Table>
        <br />
        <div
          style={{
            width: "100%",
            minHeight: "70px",
          }}
        >
          <p style={{ fontSize: "15px", padding: "10px" }}>
            Thank you very much for your intrest in registering Program
            Kesedaran Halal / Halal Awareness Program organized by HOLISTICS Lab
            Sdn. Bhd. (1151997- T).
          </p>
        </div>
        <br />
        <Table>
          <thead style={{ background: "#eeeeee", color: "#555555" }}>
            <th width="85%" style={{ padding: "10px 30px", fontSize: "15px" }}>
              Item
            </th>
            <th
              width="15%"
              style={{
                padding: "10px",
                fontSize: "15px",
                textAlign: "center",
                border: "1px solid #eeeeee",
              }}
            >
              Price
            </th>
          </thead>
          <tbody
            style={{
              border: "0px",
              color: "#555555",
              fontSize: "15px",
              border: "1px solid #eeeeee",
            }}
          >
            <tr>
              <td style={{ padding: "10px 30px" }}>
                <p>
                  <strong>
                    Professional Certificate for Halal Executive (PCHE)
                  </strong>
                </p>
                <p>
                  <strong>Estimate Date:</strong>
                </p>
                <ul>
                  <li>11-14 Jan 2021 (Mon - Thu)</li>
                  <li>18-21 Jan 2021 (Mon - Thu)</li>
                </ul>
                <p>
                  <strong>Training includes:</strong>
                </p>
                <ul>
                  <li>8 days</li>
                  <li>4 hours (Web Conference)</li>
                  <li>4 hours (E-learning)</li>
                  <li>QuikHalal License 3 months</li>
                  <li>E-learning Access (hacademy.my)</li>
                </ul>
              </td>
              <td style={{ textAlign: "center" }}>RM 2500</td>
            </tr>
            <tr style={{ border: "1px solid #eeeeee" }}>
              <td style={{ padding: "10px 30px" }}></td>
              <td style={{ textAlign: "center" }}>
                <strong>Total: RM 2500</strong>
              </td>
            </tr>
          </tbody>
        </Table>
        <Table>
          <tr>
            <td>
              <p style={{ fontSize: "12px", color: "#555555" }}>
                Payment can be made to company account:
                <br />
                <br />
                Account No: 5513-0652-5410 (MAYBANK)
                <br />
                Name: HOLISTICS LAB SDN BHD
                <br />
                <br />
                *All cheques must be crossed and made payable to{" "}
                <strong>"HOLISTICS LAB SDN BHD"</strong>
                <br />
                *Courses date are subject to change / extend <br />
              </p>
            </td>
          </tr>
        </Table>
        <br />
        <Table class={styles.invoiceFooter}>
          <tr style={{ background: "#699b12" }}>
            <td
              style={{
                background: "#062730",
                backgroundImage: "#699b12",
                color: "white",
                textAlign: "right",
                padding: "10px",
                borderRadius: "0px 20px 20px 0px",
                paddingRight: "20px",
                position: "absolute",
                zIndex: "1",
                width: "50%",
              }}
            >
              www.holisticslab.my
            </td>

            <td
              style={{
                background: "#699b12",
                color: "white",
                textAlign: "left",
                padding: "10px",
                width: "50%",
              }}
            >
              Aspiring Halal Excellence
            </td>
          </tr>
        </Table>
      </div>
    );
  };
}

class PrintReceipt extends Component {
  render = () => {
    return (
      <div id="printable" style={{ display: "none" }}>
        <br />
        <br />
        <br />
        <Table className={styles.invoiceHeader} style={{ marginBottom: "0px" }}>
          <tr>
            <td className="title" width="40%">
              <Image
                src="https://www.holisticslab.my/wp-content/uploads/2022/01/HLSB-1536x1112.png"
                style={{
                  width: "100%",
                  maxWidth: "400px",
                  marginTop: "-70px",
                  marginLeft: "20px",
                }}
              />
            </td>

            <td
              style={{
                textAlign: "right",
                borderRadius: "200px 0px 0px 0px",
                background: "#062730",
                color: "white",
                padding: "0px 30px",
              }}
              width="50%"
            >
              <p style={{ padding: "0", margin: "0", fontSize: "30px" }}>
                <strong>OFFICIAL RECEIPT</strong>
              </p>
              <p style={{ padding: "0", margin: "0", fontSize: "14px" }}>
                <strong>HOLISTICS LAB Sdn. Bhd. (1151997-T)</strong>
              </p>
              <br />
              <p style={{ padding: "0", margin: "0", fontSize: "14px" }}>
                BG-F-05, Medini 6, <br /> Jalan Medini Sentral 5, Medini, <br />
                79250 Nusajaya, Johor
                <br />
                Office : 019-7765075 <br />
                www.holisticslab.my
              </p>
            </td>
          </tr>
        </Table>
        <div
          style={{
            height: "15px",
            width: "100%",
            display: "block",
            background: "#699b12",
          }}
          width="100%"
        ></div>
        <center>
          <p>
            Office Mobile No: +6019 776 5075 &nbsp; &nbsp; Fax: - &nbsp; &nbsp;
            Email: admin@holisticslab.my
          </p>
        </center>
        <Table className={styles.invoiceBox} style={{ fontSize: "15px" }}>
          <tr>
            <td width="20%">
              <p style={{ marginTop: "-70px" }}>RECEIVED FROM:</p>
            </td>
            <td width="50%">
              <p>
                <strong>
                  Unison Nutraceuticals Sdn Bhd
                  <br />
                  No. 13, Jln TU 52, Taman Tasik Utama, <br />
                  75450 Ayer Keroh, Melaka
                  <br />
                  0175008648
                  <br />
                  Azira Mohd Rawi
                  <br />
                  aziramohdrawi@gmail.com
                </strong>
              </p>
            </td>
            <td style={{ textAlign: "left" }} width="30%">
              <p style={{ marginTop: "-70px", textAlign: "right" }}>
                OR No.: OR-202103-0035
                <br />
                Date: 18/03/2021
              </p>
            </td>{" "}
            <br />
          </tr>
          <tr style={{ borderTop: "0px" }}>
            <td>RECEIVED THE SUM OF:</td>
            <td>
              <p style={{ textDecoration: "underline" }}>
                RINGGIT MALAYSIA SEVENTY-FIVE ONLY
              </p>
            </td>
            <td>
              <Table style={{ border: "1px solid #eeeeee" }}>
                <tr>
                  <td>
                    <p>
                      Payment By
                      <br />
                      BANK TRANSFER
                    </p>
                  </td>
                  <td>
                    <p>
                      Payment Amount
                      <br />
                      RM 75.00
                    </p>
                  </td>
                </tr>
              </Table>
            </td>
          </tr>
        </Table>
        <br />
        <Table>
          <thead style={{ background: "#eeeeee", color: "#555555" }}>
            <th
              width="85%"
              style={{
                padding: "10px 30px",
                fontSize: "15px",
                verticalAlign: "middle",
              }}
            >
              <p style={{ fontSize: "15px" }}>
                <strong>Payment Received</strong>
              </p>
            </th>
            <th
              width="15%"
              style={{
                padding: "10px",
                fontSize: "15px",
                textAlign: "center",
                border: "1px solid #eeeeee",
              }}
            >
              <p style={{ fontSize: "15px" }}>
                <strong>Payment Amount</strong>
              </p>
            </th>
          </thead>
          <tbody
            style={{
              border: "0px",
              color: "#555555",
              fontSize: "15px",
              border: "1px solid #eeeeee",
            }}
          >
            <tr>
              <td style={{ padding: "10px 30px" }}>
                <p>
                  <strong>
                    Professional Certificate for Halal Executive (PCHE)
                  </strong>
                </p>
                <p>
                  <strong>Estimate Date:</strong>
                </p>
                <ul>
                  <li>11-14 Jan 2021 (Mon - Thu)</li>
                  <li>18-21 Jan 2021 (Mon - Thu)</li>
                </ul>
                <p>
                  <strong>Training includes:</strong>
                </p>
                <ul>
                  <li>8 days</li>
                  <li>4 hours (Web Conference)</li>
                  <li>4 hours (E-learning)</li>
                  <li>QuikHalal License 3 months</li>
                  <li>E-learning Access (hacademy.my)</li>
                </ul>
              </td>
              <td style={{ textAlign: "center" }}>RM 2500</td>
            </tr>
            <tr style={{ border: "1px solid #eeeeee" }}>
              <td style={{ padding: "10px 30px" }}></td>
              <td style={{ textAlign: "center" }}>
                <strong>Total: RM 2500</strong>
              </td>
            </tr>
          </tbody>
        </Table>
        <Table>
          <tr>
            <td>
              <p style={{ fontSize: "12px", color: "#555555", padding: "0px" }}>
                Note:
                <br />
                1) This is computer generated receipt. No signature required
                <hr />
                <Table>
                  <thead>
                    <th>Inv. No</th>
                    <th>Inv. Date</th>
                    <th>Invoice Amount</th>
                    <th>Paid Amount</th>
                    <th>Outstanding Balance</th>
                  </thead>
                  <tbody style={{ borderTop: "0px" }}>
                    <td>HLINV202103- 00043</td>
                    <td>16/03/2021</td>
                    <td>RM 2500.00</td>
                    <td>RM 2500.00</td>
                  </tbody>
                </Table>
              </p>
            </td>
          </tr>
        </Table>
        <Table>
          <tr style={{ background: "#699b12" }}>
            <td
              style={{
                background: "#062730",
                backgroundImage: "#699b12",
                color: "white",
                textAlign: "right",
                padding: "10px",
                borderRadius: "0px 20px 20px 0px",
                paddingRight: "20px",
                position: "absolute",
                zIndex: "1",
                width: "38%",
              }}
            >
              www.holisticslab.my
            </td>

            <td
              style={{
                background: "#699b12",
                color: "white",
                textAlign: "left",
                padding: "10px",
                width: "50%",
              }}
            >
              Aspiring Halal Excellence
            </td>
          </tr>
        </Table>
      </div>
    );
  };
}

export { InvoicePage, PrintInvoice, PrintProforma, PrintReceipt };
