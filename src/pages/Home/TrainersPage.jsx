import React, { Component } from "react";
import { Container, Row, Col } from "react-bootstrap";
import ProfileCardContext from "../../components/Home/ProfileCardContext";

export class TrainerHomePage extends Component {
  render() {
    return (
      <div>
        <Container>
          <br />
          <br />
          <Row>
            <Col>
              <div style={{ textAlign: "center" }}>
                <h2>HALAL INSTRUCTORS</h2>
                <span
                  style={{ fontSize: "16px", color: "gray", fontWeight: "100" }}
                >
                  JAKIM Certified Halal Trainers with PhDs, Masters and Degrees
                  from various specialization (Islamic Studies, Information
                  Technology, Logistics, Food Technology, etc). <br />
                  <br />
                  The trainers also have vast R&D and practical in halal
                  industry.
                </span>
              </div>
            </Col>
          </Row>
          <hr />
          <br />
          <Row>
            <Col md={4} sm={12}>
              <ProfileCardContext
                image="https://training.holisticslab.my/wp-content/uploads/2018/12/TRAINER-01-768x768.png"
                name="PROF. DR. FARAHWAHIDA BINTI MOHD YUSOF"
                background="Center of Research for Fiqh Science and Technology (C-First)"
              ></ProfileCardContext>
            </Col>
            <Col md={4} sm={12}>
              <ProfileCardContext
                image="https://training.holisticslab.my/wp-content/uploads/2018/12/DR-ARIEF-Diff-Size-1024x1024.png"
                name="ASSOCIATE PROFESSOR DR. ARIEFF SALLEH BIN ROSMAN"
                background="Faculty of Social Sciences and Humanities"
              ></ProfileCardContext>
            </Col>
            <Col md={4} sm={12}>
              <ProfileCardContext
                image="https://training.holisticslab.my/wp-content/uploads/2018/12/TRAINER-04-1024x1024.png"
                name="TS. DR. MOHD ISKANDAR BIN ILLYAS TAN"
                background="Azman Hashim International Business School"
              ></ProfileCardContext>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default TrainerHomePage;
