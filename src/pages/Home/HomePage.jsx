import React, { Component } from "react";
import { Container, Row, Col, Image } from "react-bootstrap";
import ItemSlider from "../../components/Home/ItemSlider";
import AccordianPanel from "../../components/Home/AccordianPanel";
import TimeCountdown from "../../components/Home/TimeCountdown";

export class HomePage extends Component {
  render() {
    let data = [
      {
        id: 1,
        question:
          "It will be a compulsory requirements for company applying/ renewing Malaysia Halal Certificate)",
        answer:
          "“Obtaining Halal Executive Certificate from Halal Training Provider registered under HPB JAKIM” (Clause 3.5,pg.3, Malaysian Halal Certification Procedure Manual (Domestic) 2020).",
      },
      {
        id: 2,
        question:
          "It will be a compulsory requirements for company applying/ renewing Malaysia Halal Certificate)",
        answer:
          "“Obtaining Halal Executive Certificate from Halal Training Provider registered under HPB JAKIM” (Clause 3.5,pg.3, Malaysian Halal Certification Procedure Manual (Domestic) 2020).",
      },
      {
        id: 2,
        question:
          "It will be a compulsory requirements for company applying/ renewing Malaysia Halal Certificate)",
        answer:
          "“Obtaining Halal Executive Certificate from Halal Training Provider registered under HPB JAKIM” (Clause 3.5,pg.3, Malaysian Halal Certification Procedure Manual (Domestic) 2020).",
      },
      {
        id: 2,
        question:
          "It will be a compulsory requirements for company applying/ renewing Malaysia Halal Certificate)",
        answer:
          "“Obtaining Halal Executive Certificate from Halal Training Provider registered under HPB JAKIM” (Clause 3.5,pg.3, Malaysian Halal Certification Procedure Manual (Domestic) 2020).",
      },
      {
        id: 2,
        question:
          "It will be a compulsory requirements for company applying/ renewing Malaysia Halal Certificate)",
        answer:
          "“Obtaining Halal Executive Certificate from Halal Training Provider registered under HPB JAKIM” (Clause 3.5,pg.3, Malaysian Halal Certification Procedure Manual (Domestic) 2020).",
      },
      {
        id: 2,
        question:
          "It will be a compulsory requirements for company applying/ renewing Malaysia Halal Certificate)",
        answer:
          "“Obtaining Halal Executive Certificate from Halal Training Provider registered under HPB JAKIM” (Clause 3.5,pg.3, Malaysian Halal Certification Procedure Manual (Domestic) 2020).",
      },
      {
        id: 2,
        question:
          "It will be a compulsory requirements for company applying/ renewing Malaysia Halal Certificate)",
        answer:
          "“Obtaining Halal Executive Certificate from Halal Training Provider registered under HPB JAKIM” (Clause 3.5,pg.3, Malaysian Halal Certification Procedure Manual (Domestic) 2020).",
      },
    ];
    let images = [
      "https://training.holisticslab.my/wp-content/uploads/2018/11/footer1-300x81.png",
      "https://training.holisticslab.my/wp-content/uploads/2018/11/Logo-Color-UTM-300x98.png",
      "https://training.holisticslab.my/wp-content/uploads/2018/12/jakim-logo-01-1024x322.png",
      "https://training.holisticslab.my/wp-content/uploads/2018/11/Logo-Color-JAIJ.png",
      "https://training.holisticslab.my/wp-content/uploads/2018/11/footer44.png",
      "https://training.holisticslab.my/wp-content/uploads/2018/11/footer5.png",
      "https://training.holisticslab.my/wp-content/uploads/2018/11/footer6-1.png",
      "https://training.holisticslab.my/wp-content/uploads/2018/11/NCIA-Logo-1-1.png",
      "https://training.holisticslab.my/wp-content/uploads/2018/11/Hc-logo-2a-01-1024x724.png",
      "https://training.holisticslab.my/wp-content/uploads/2018/11/C1-01-1024x724.png",
    ];
    return (
      <div>
        <ItemSlider></ItemSlider>
        <div style={{ height: "50vh" }}>
          <Container>
            <br />
            <TimeCountdown></TimeCountdown>
            <br />
          </Container>
        </div>
        <br />
        <div>
          <Container>
            <h2 style={{ fontSize: "40px", textAlign: "center" }}>
              WHY JOIN OUR PROGRAM?
            </h2>
            <br />
            {data.map((e) => (
              <AccordianPanel
                id={e.id}
                question={e.question}
                answer={e.answer}
              ></AccordianPanel>
            ))}
          </Container>
        </div>
        <div
          style={{
            height: "auto",
            margin: "auto",
          }}
        >
          <Container>
            <br />
            <Row className="justify-content-center align-items-center text-center">
              <Col md={6}>
                <h3>ORGANIZED BY</h3>
              </Col>
              <Col md={6}>
                <h3>ENDORSED AND CERTIFIED BY</h3>
              </Col>
            </Row>
            <br />
            <Row className="justify-content-center align-items-center text-center">
              <Col md={3}>
                <Image className="d-block w-50 m-auto" src={images[0]} />
              </Col>
              <Col md={3}>
                <Image className="d-block w-50 m-auto" src={images[1]} />
              </Col>
              <Col md={6}>
                <Image className="d-block w-50 m-auto" src={images[2]} />
              </Col>
            </Row>
            <br />
            <Row className="justify-content-center align-items-center text-center">
              <Col md={12}>
                <h3>SUPPORTED BY</h3>
              </Col>
            </Row>
            <br />
            <Row
              style={{ width: "100%" }}
              className="justify-content-center align-items-center text-center"
            >
              <Col md={12}>
                <Image
                  style={{ width: "10%", margin: "10px" }}
                  src={images[3]}
                />
                <Image
                  style={{ width: "10%", margin: "10px" }}
                  src={images[4]}
                />
                <Image
                  style={{ width: "10%", margin: "10px" }}
                  src={images[5]}
                />
                <Image
                  style={{ width: "10%", margin: "10px" }}
                  src={images[6]}
                />
                <Image
                  style={{ width: "10%", margin: "10px" }}
                  src={images[7]}
                />
                <Image
                  style={{ width: "10%", margin: "10px" }}
                  src={images[8]}
                />
                <Image
                  style={{ width: "10%", margin: "10px" }}
                  src={images[9]}
                />
              </Col>
            </Row>
            <br />
          </Container>
        </div>
      </div>
    );
  }
}

export default HomePage;
