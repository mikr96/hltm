import React, { Component } from "react";
import { Container, Row, Col, Accordion } from "react-bootstrap";

export class FaqHomePage extends Component {
  render() {
    return (
      <div>
        <Container>
          <br />
          <br />
          <Row>
            <Col md={12}>
              <div style={{ textAlign: "center" }}>
                <h2>FREQUENTLY ASKED QUESTIONS</h2>
                <h4>Most frequent questions and answers</h4>
              </div>
            </Col>
          </Row>
          <hr />
          <br />
          <Row>
            <Accordion defaultActiveKey={["0"]} alwaysOpen>
              <Accordion.Item eventKey="0">
                <Accordion.Header>
                  What is the training offered by Holistics Lab Sdn Bhd ?
                </Accordion.Header>
                <Accordion.Body>
                  Holistics Lab Sdn Bhd offers two(2) programs under Halal
                  Professional Board JAKIM which are{" "}
                  <strong>
                    Professional Certificate for Halal Executive (PCHE){" "}
                  </strong>
                  and{" "}
                  <strong>
                    Professional Certificate for Halal Internal Audit (PCHIA)
                  </strong>{" "}
                  . Besides that Holistics Lab also offers{" "}
                  <strong>Halal Awareness Program</strong> and{" "}
                  <strong>Halal Competency Program</strong>.
                </Accordion.Body>
              </Accordion.Item>
              <Accordion.Item eventKey="1">
                <Accordion.Header>Is the training compulsory?</Accordion.Header>
                <Accordion.Body>
                  “Halal Executives shall be appointed at each branch of the
                  premises” <br /> <br />
                  <strong>
                    - (Clause 18.1(f)(i)(b).,pg.31, Malaysian Halal
                    Certification Procedure Manual (Domestic) 2020).
                  </strong>
                  <br /> <br /> “Halal Executive refers to an individual that
                  responsible for ensuring the Halal compliance in a company or
                  premises. Halal Executives must meet the following conditions:
                  <br />
                  <br />
                  1. Muslim
                  <br />
                  2. Citizen of Malaysia
                  <br />
                  3. Permanent position
                  <br />
                  4. Minimum Diploma in Halal Management or any equivalent
                  qualification or experience in halal management for at least
                  five (5) years
                  <br />
                  5. Obtaining Halal Executive Certificate from Halal Training
                  Provider registered under HPB JAKIM” (Clause 3.5,pg.3,
                  Malaysian Halal Certification Procedure Manual (Domestic)
                  2020).”
                  <br />
                  <br />
                  <strong>
                    - (Clause 3.5,pg.3, Malaysian Halal Certification Procedure
                    Manual (Domestic) 2020).
                  </strong>
                </Accordion.Body>
              </Accordion.Item>
              <Accordion.Item eventKey="2">
                <Accordion.Header>
                  How long does this training program take ?
                </Accordion.Header>
                <Accordion.Body>
                  HOLISTICS Lab conduct the training in three(3) mode ;{" "}
                  <strong>PHYSICAL, ONLINE & IN-HOUSE.</strong> <br />
                  <br />
                  Whereby PHYSICAL & ONLINE training (Halal Professional Board)
                  are conducting in <strong>8 days.</strong> For IN-HOUSE
                  training (Halal Professional Board) are conducting in{" "}
                  <strong>5 days.</strong>
                </Accordion.Body>
              </Accordion.Item>
              <Accordion.Item eventKey="3">
                <Accordion.Header>
                  How about the fees of the training offers ?
                </Accordion.Header>
                <Accordion.Body>
                  Face-to-face training cost is{" "}
                  <strong>
                    MYR 4000 (Malaysians) & USD 3000 (Non-Malaysians){" "}
                  </strong>{" "}
                  per person for each training. Online training costs{" "}
                  <strong>
                    MYR 2500 (Malaysians) & USD 1000 (Non-Malaysians).
                  </strong>
                </Accordion.Body>
              </Accordion.Item>
              <Accordion.Item eventKey="4">
                <Accordion.Header>
                  Are both face-to-face and online training conducted at the
                  same time ?
                </Accordion.Header>
                <Accordion.Body>
                  No. It’s conduct in separate time because it is a different
                  training. Holistics Lab offers in several schedule. You may
                  view our calendar here.
                </Accordion.Body>
              </Accordion.Item>
              <Accordion.Item eventKey="5">
                <Accordion.Header>
                  What is included in the fees ?
                </Accordion.Header>
                <Accordion.Body>
                  For those who are participating in our training, you will
                  receive <br />
                  <br />
                  <strong>
                    - 4 Different Certificates (Certificate of Attendance,
                    Professional Certificate for QuikHalal, Professional
                    Certificate for Halal Executive by Holistics Lab & Halal
                    Professional Board JAKIM){" "}
                  </strong>{" "}
                  <br /> <br />
                  Face-to-face participants will receive additional <br />{" "}
                  <br />
                  <strong>- Halal Assurance System (HAS) File </strong>
                  <br />
                  <strong>- Bag Pack </strong>
                  <br />
                  <strong>- Meals (Breakfast, Lunch, Tea Break) </strong>
                </Accordion.Body>
              </Accordion.Item>
              <Accordion.Item eventKey="6">
                <Accordion.Header>What is the language used ?</Accordion.Header>
                <Accordion.Body>
                  Both training used <strong>English language</strong> since the
                  syllabus/content received from JAKIM is English. Notes and
                  slides used also in English Language.
                </Accordion.Body>
              </Accordion.Item>
              <Accordion.Item eventKey="7">
                <Accordion.Header>
                  What makes Holistics Lab Sdn Bhd different from other trainers
                  provider ?
                </Accordion.Header>
                <Accordion.Body>
                  As a special bonus for attending our training program, we’ll
                  give you a special access to{" "}
                  <strong>Hacademy (online Halal learning platform) </strong>and{" "}
                  <strong>FREE 3 month of QuikHalal </strong> (First mobile
                  cloud based Halal auditing apps).
                </Accordion.Body>
              </Accordion.Item>
              <Accordion.Item eventKey="8">
                <Accordion.Header>
                  Can I attend other companies which offer the same training?
                </Accordion.Header>
                <Accordion.Body>
                  Yes. But you must attend the training provider that is
                  recognized by JAKIM only.
                </Accordion.Body>
              </Accordion.Item>
              <Accordion.Item eventKey="9">
                <Accordion.Header>
                  I have attended the training before this but how can I know
                  that my precious trainers are recognized by JAKIM?
                </Accordion.Header>
                <Accordion.Body>
                  You may visit JAKIM Facebook or refer to this attachment :
                  Lists of Registered Halal Training Provider under JAKIM.
                </Accordion.Body>
              </Accordion.Item>
            </Accordion>
          </Row>
        </Container>
        <br />
        <br />
        <Row>
          <Col md={12}>
            <div style={{ textAlign: "center" }}>
              <h2>
                For any inquiries/ questions, please do not hesitate to contact
                us.
              </h2>
            </div>
          </Col>
        </Row>
        <br />
        <br />
      </div>
    );
  }
}

export default FaqHomePage;
