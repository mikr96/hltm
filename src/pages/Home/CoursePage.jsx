import React, { Component } from "react";
import { Container, Row, Col } from "react-bootstrap";
import CardContext from "../../components/Home/CardContext";
import CourseDetailsHomePage from "./CourseDetailsPage";

export class CourseHomePage extends Component {
  image1 =
    "https://training.holisticslab.my/wp-content/uploads/2021/05/PCHE-1024x724.png";
  image2 =
    "https://training.holisticslab.my/wp-content/uploads/2021/05/PCIHA-1024x724.png";
  image3 =
    "https://training.holisticslab.my/wp-content/uploads/2021/05/HAP-1024x724.png";
  image4 =
    "https://training.holisticslab.my/wp-content/uploads/2021/05/HCPP-1024x724.png";
  render() {
    return (
      <div>
        <Container>
          <br />
          <br />
          <Row>
            <Col md={12}>
              <div style={{ textAlign: "center" }}>
                <h2>COURSE OFFERS</h2>
              </div>
            </Col>
          </Row>
          <hr />
          <br />
          <Row>
            <Col md={6} sm={12}>
              <CardContext image={this.image1} url="PCHE"></CardContext>
            </Col>
            <Col md={6} sm={12}>
              <CardContext image={this.image2} url="PCIHA"></CardContext>
            </Col>
          </Row>
          <Row>
            <Col md={6} sm={12}>
              <CardContext image={this.image3} url="PCHA"></CardContext>
            </Col>
            <Col md={6} sm={12}>
              <CardContext image={this.image4} url="PCHC"></CardContext>
            </Col>
          </Row>
          <br />
        </Container>
      </div>
    );
  }
}

export default CourseHomePage;
