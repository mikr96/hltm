import React, { Component, Fragment } from "react";
import { withRouter } from "../../functions/withRouter";
import {
  Container,
  Row,
  Col,
  Button,
  Card,
  ListGroup,
  Form,
} from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import APIFunction from "../../functions/APIFunction";
import CRUDFunction from "../../functions/CRUDFunction";
import APIurl from "../../api/AppURL";

export class CourseDetailsHomePage extends Component {
  constructor() {
    super();
    this.state = {
      checkout: false,
      email: "",
      mobile: "",
      name: "",
      amount: 20000,
      collection_id: "btplwgiz",
      redirect_url: "http://localhost:3000/successful",
      callback_url: "http://localhost:3000/course",
      description: "Testing je",
    };
    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit = async (event) => {
    event.preventDefault();
    const data = {
      email: this.state.email,
      mobile: this.state.mobile,
      name: this.state.name,
      amount: parseInt(this.state.amount),
      collection_id: this.state.collection_id,
      callback_url: this.state.callback_url,
      redirect_url: this.state.redirect_url,
      description: this.state.description,
    };
    // console.log(data);
    const response = await CRUDFunction.createBill(APIurl.bill, data);
    if (response) {
      console.log(response);
      window.location.href = response.data.url;
    }
  };

  viewCourse = () => {
    let pageHeight = window.innerHeight;
    window.scrollBy(0, pageHeight);
  };

  render() {
    if (this.state.checkout) {
      return (
        <Fragment>
          <div
            className="bg-image"
            style={{
              height: "100vh",
              color: "white",
            }}
          >
            <Container>
              <br />
              <br />
              <h2 style={{ fontSize: "40px" }}>Professional Certificate for</h2>
              <h2 style={{ fontSize: "40px" }}>Halal Executive</h2>
              <br />
              <br />
              <p style={{ fontSize: "20px" }}>
                This course is designed for Halal Executives or Halal
                Consultants that will guide organisations to receive their{" "}
                <strong>Halal Certification</strong> to certify that their
                products or services meet with the{" "}
                <strong>Malaysia Halal Standard.</strong>
              </p>
              <br />
              <p style={{ fontSize: "20px" }}>
                Trainees will be equipped with theoretical knowledge and
                practical skills to become
                <strong> Halal Executives</strong> hat can assist organisations
                to meet the Halal Toyyiban Standards and ensure that practical
                methods, techniques, procedures, technology and tools are
                employed.
              </p>
              <br />
              <Button
                className="primary"
                onClick={() => {
                  this.setState({ checkout: true });
                }}
              >
                REGISTER NOW!
              </Button>
              &nbsp; &nbsp; &nbsp; &nbsp;
              <Button variant="primary" onClick={this.viewCourse}>
                COURSE DETAILS
              </Button>
            </Container>
          </div>
          <br />
          <br />
          <Row style={{ height: "10vh" }}>
            <Col md={12}>
              <br />
              <div style={{ textAlign: "center" }}>
                <h2>COURSE OUTLINE</h2>
              </div>
            </Col>
          </Row>{" "}
          <Row style={{ height: "70vh" }}>
            <Col md={6}>
              <Card style={{ minHeight: "70vh", margin: "30px" }}>
                <Card.Body>
                  <Card.Title style={{ fontSize: "20px" }}>
                    Objective
                  </Card.Title>
                  <Card.Text style={{ fontSize: "20px" }}>
                    <ul>
                      <li
                        style={{
                          textAlign: "justify",
                          textJustify: "inter-word",
                          padding: "10px",
                        }}
                      >
                        To establish a competent{" "}
                        <strong>Halal Executive</strong> that able to setup and
                        manage the internal halal committee, halal manuals and
                        record.
                      </li>
                      <li
                        style={{
                          textAlign: "justify",
                          textJustify: "inter-word",
                          padding: "10px",
                        }}
                      >
                        To provide participants with knowledge and skills to
                        administer the internal halal certification processes
                        and quality assurance.
                      </li>
                      <li
                        style={{
                          textAlign: "justify",
                          textJustify: "inter-word",
                          padding: "10px",
                        }}
                      >
                        To provide hands-on knowledge on the technology
                        application of halal certification and audit.
                      </li>
                    </ul>
                  </Card.Text>
                </Card.Body>
              </Card>
            </Col>
            <Col md={6}>
              <div
                style={{
                  margin: "30px",
                  background: "#e9e9e9",
                  minHeight: "70vh",
                  padding: "20px",
                }}
              >
                <ul
                  style={{
                    listStyleType: "none",
                    fontSize: "20px",
                    paddingLeft: "0px",
                  }}
                >
                  <li
                    style={{
                      padding: "10px",
                    }}
                  >
                    <strong>Duration</strong> : 8 days (2 weeks)
                  </li>
                  <li
                    style={{
                      padding: "10px",
                    }}
                  >
                    <strong>Day</strong> : Monday – Thursday
                  </li>
                  <li
                    style={{
                      padding: "10px",
                    }}
                  >
                    <strong>Time</strong> : 9.00am – 5.00pm
                  </li>
                </ul>
                <span
                  style={{
                    padding: "10px",
                    fontSize: "15px",
                    display: "block",
                  }}
                >
                  For In-house training, please do contact Admin +019-776 5075/
                  email to training@holisticslab.my
                </span>
                <ListGroup style={{ padding: "10px" }}>
                  <ListGroup.Item>
                    {" "}
                    <FontAwesomeIcon
                      icon={["fas", "check"]}
                      color="#50656E"
                      size="lg"
                    />{" "}
                    &nbsp; HRDF Claimable
                  </ListGroup.Item>
                  <ListGroup.Item>
                    {" "}
                    <FontAwesomeIcon
                      icon={["fas", "check"]}
                      color="#50656E"
                      size="lg"
                    />{" "}
                    &nbsp;In-house Training available
                  </ListGroup.Item>
                </ListGroup>
                <div style={{ padding: "10px" }}>
                  <Button
                    className="primary"
                    onClick={() => this.setState({ checkout: true })}
                  >
                    Register Now!
                  </Button>
                  &nbsp;&nbsp;&nbsp;
                  <Button className="secondary">Download Brochure</Button>
                </div>
              </div>
            </Col>
          </Row>
          <Row style={{ height: "10vh" }}></Row>
        </Fragment>
      );
    } else {
      return (
        <Fragment>
          <br />
          <br />
          <Container>
            <Row>
              <h2 style={{ textAlign: "center" }}>REGISTRATION</h2>
              <hr />
              <br />
            </Row>
            <Row>
              <Col md={5}>
                <Button
                  style={{
                    display: "inline-block",
                    width: "100%",
                    float: "right",
                  }}
                  className="primary"
                >
                  Personal
                </Button>{" "}
              </Col>
              <Col md={2} style={{ textAlign: "center", alignItems: "middle" }}>
                <span>OR</span>
              </Col>
              <Col md={5}>
                <Button
                  style={{
                    display: "inline-block",
                    width: "100%",
                    float: "left",
                  }}
                  className="primary"
                >
                  Company
                </Button>
              </Col>
            </Row>
            <Form size="large" onSubmit={this.onSubmit}>
              <br />
              <Row>
                <Col md={6}>
                  <Form.Group
                    className="mb-3"
                    controlId="formGridAddress1"
                    onChange={(e) => {
                      this.setState({ fullname: e.target.value });
                    }}
                  >
                    <Form.Label class="required">Fullname</Form.Label>
                    <Form.Control placeholder="" />
                  </Form.Group>
                </Col>
                <Col md={6}>
                  <Form.Group
                    className="mb-3"
                    controlId="formGridAddress1"
                    onChange={(e) => {
                      this.setState({ email: e.target.value });
                    }}
                  >
                    <Form.Label class="required">Email</Form.Label>
                    <Form.Control placeholder="" />
                  </Form.Group>
                </Col>
              </Row>
              <Row>
                <Col md={6}>
                  <Form.Group
                    className="mb-3"
                    controlId="formGridAddress1"
                    onChange={(e) => {
                      this.setState({ ic: e.target.value });
                    }}
                  >
                    <Form.Label class="required">No. IC</Form.Label>
                    <Form.Control placeholder="" />
                  </Form.Group>
                </Col>
                <Col md={6}>
                  <Form.Group
                    className="mb-3"
                    controlId="formGridAddress1"
                    onChange={(e) => {
                      this.setState({ phone_no: e.target.value });
                    }}
                  >
                    <Form.Label class="required">No. Phone</Form.Label>
                    <Form.Control type="number" placeholder="" />
                  </Form.Group>
                </Col>
              </Row>
              <Row>
                <Col md={6}>
                  <Form.Group
                    className="mb-3"
                    controlId="formGridAddress1"
                    onChange={(e) => {
                      this.setState({ company_name: e.target.value });
                    }}
                  >
                    <Form.Label class="required">Position</Form.Label>
                    <Form.Control placeholder="" />
                  </Form.Group>
                </Col>
                <Col md={6}>
                  <Form.Group
                    className="mb-3"
                    controlId="formGridAddress1"
                    onChange={this.onChangeCompanyName}
                  >
                    <Form.Label class="required">Company Name</Form.Label>
                    <Form.Select aria-label="Default select example">
                      <option value="0">Open this select menu</option>
                      {this.state.company_list}
                    </Form.Select>
                  </Form.Group>
                </Col>
              </Row>
              <Row>
                <Col md={6}>
                  <Form.Group className="mb-3" controlId="formGridAddress1">
                    <Form.Label class="required">Company Address</Form.Label>
                    <Form.Control
                      placeholder=""
                      value={this.state.company_address}
                    />
                  </Form.Group>
                </Col>
                <Col md={6}>
                  <Form.Group className="mb-3" controlId="formGridAddress1">
                    <Form.Label class="required">Company Address</Form.Label>
                    <Form.Control
                      placeholder=""
                      value={this.state.company_address}
                    />
                  </Form.Group>
                </Col>
              </Row>
              <br />
              <br />
              <Row>
                <h3 style={{ textAlign: "center" }}>PAYMENT DETAILS</h3>
                <hr />
                <br />
              </Row>
              <Row>
                <Col md={6}>
                  <Form.Group
                    className="mb-3"
                    controlId="formGridAddress1"
                    onChange={(e) => {
                      this.setState({ email: e.target.value });
                    }}
                  >
                    <Form.Label class="required">Email</Form.Label>
                    <Form.Control placeholder="" />
                  </Form.Group>
                </Col>
                <Col md={6}>
                  <Form.Group
                    className="mb-3"
                    controlId="formGridAddress1"
                    onChange={(e) => {
                      this.setState({ mobile: e.target.value });
                    }}
                  >
                    <Form.Label class="required">Phone No.</Form.Label>
                    <Form.Control placeholder="" />
                  </Form.Group>
                </Col>
              </Row>
              <Row>
                <Col md={6}>
                  <Form.Group
                    className="mb-3"
                    controlId="formGridAddress1"
                    onChange={(e) => {
                      this.setState({ name: e.target.value });
                    }}
                  >
                    <Form.Label class="required">Name</Form.Label>
                    <Form.Control placeholder="" />
                  </Form.Group>
                </Col>
                <Col md={6}>
                  <Form.Group
                    className="mb-3"
                    controlId="formGridAddress1"
                    onChange={(e) => {
                      this.setState({ amount: e.target.value });
                    }}
                  >
                    <Form.Label class="required">Fees (RM)</Form.Label>
                    <Form.Control type="number" defaultValue="200" disabled />
                  </Form.Group>
                </Col>
              </Row>
              <br />
              <Row className="justify-content-center">
                <Button variant="primary" type="submit">
                  Checkout
                </Button>
              </Row>
              <br />
              <br />
            </Form>
          </Container>
        </Fragment>
      );
    }
  }
}

export default withRouter(CourseDetailsHomePage);
