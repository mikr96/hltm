import React, { Component, Fragment } from "react";
import { withRouter } from "../functions/withRouter";
import APIurl from "../api/AppURL";
import CRUDFunction from "../functions/CRUDFunction";
import { Container, Row, Col, Card, Form, Button } from "react-bootstrap";
import withReactContent from "sweetalert2-react-content";
import Swal from "sweetalert2";
import toast, { Toaster } from "react-hot-toast";

const MySwal = withReactContent(Swal);

export class ResetPasswordPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        email: "",
        password: "",
        confirmed_password: "",
        token: this.props.params.token,
      },
    };
    this.resetPassword = this.resetPassword.bind(this);
  }

  resetPassword = async (event) => {
    event.preventDefault();
    const response = await CRUDFunction.password(
      APIurl.resetpassword,
      this.state.data
    );
    if (response) {
      if (response.data.message == "Your password has been reset!") {
        Swal.fire({
          position: "center",
          icon: "success",
          title: "Password Successfully Updated",
          showConfirmButton: true,
          timer: 2500,
        }).then(() => {
          this.props.navigate("/login");
        });
      } else {
        toast.error(response.data.message, {
          duration: 4000,
        });
      }
    }
  };

  render() {
    console.log();
    return (
      <Fragment>
        <br />
        <Container>
          <Row>
            <Col md={{ span: 6, offset: 3 }}>
              <Card style={{ padding: "20px" }}>
                <Form
                  style={{ fontSize: "15px" }}
                  onSubmit={this.resetPassword}
                >
                  <Form.Group
                    className="mb-3"
                    controlId="formBasicEmail"
                    onChange={(e) => {
                      this.setState({
                        data: {
                          ...this.state.data,
                          [e.target.name]: e.target.value,
                        },
                      });
                    }}
                  >
                    <Form.Label>Email address</Form.Label>
                    <Form.Control
                      name="email"
                      type="email"
                      placeholder="Enter email"
                    />
                  </Form.Group>

                  <Form.Group
                    className="mb-3"
                    controlId="formBasicPassword"
                    onChange={(e) => {
                      this.setState({
                        data: {
                          ...this.state.data,
                          [e.target.name]: e.target.value,
                        },
                      });
                    }}
                  >
                    <Form.Label>Enter Password</Form.Label>
                    <Form.Control
                      name="password"
                      type="password"
                      placeholder="Password"
                    />
                  </Form.Group>

                  <Form.Group
                    className="mb-3"
                    controlId="formBasicPassword"
                    onChange={(e) => {
                      this.setState({
                        data: {
                          ...this.state.data,
                          [e.target.name]: e.target.value,
                        },
                      });
                    }}
                  >
                    <Form.Label>Reconfirm Password</Form.Label>
                    <Form.Control
                      name="confirmed_password"
                      type="password"
                      placeholder="Confirm Password"
                    />
                  </Form.Group>

                  <Button
                    style={{ float: "right" }}
                    className="primary"
                    type="submit"
                  >
                    Submit
                  </Button>
                </Form>
              </Card>
            </Col>
          </Row>
        </Container>
        <br />
      </Fragment>
    );
  }
}

export default withRouter(ResetPasswordPage);
