import React, { Component, Fragment } from "react";
import HeaderDesktop from "../components/common/HeaderDesktop";
import FooterDesktop from "../components/common/FooterDesktop";
import HeaderMobile from "../components/common/HeaderMobile";
import FooterMobile from "../components/common/FooterMobile";
import { Container, Row } from "react-bootstrap";
import { Outlet } from "react-router-dom";

class HomeLayout extends Component {
  componentDidMount() {
    window.scroll(0, 0);
  }

  render() {
    return (
      <Fragment>
        <Row>
          <div className="Desktop">
            <HeaderDesktop role="home"></HeaderDesktop>
          </div>
          <div className="Mobile">
            <HeaderMobile></HeaderMobile>
          </div>
        </Row>
        <Outlet />
        <Row>
          <div className="Desktop">
            <FooterDesktop role="home"></FooterDesktop>
          </div>
          <div className="Mobile">
            <FooterMobile></FooterMobile>
          </div>
        </Row>
      </Fragment>
    );
  }
}

export default HomeLayout;
