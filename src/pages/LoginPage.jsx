import React, { Component, Fragment } from "react";
import {
  Button,
  Form,
  Grid,
  Header,
  Image,
  Message,
  Segment,
  Checkbox,
} from "semantic-ui-react";
import logo from "../assets/images/colorLogo.png";
import { Link } from "react-router-dom";
import axios from "axios";
import { Navigate } from "react-router";
import { Modal } from "react-bootstrap";
import toast, { Toaster } from "react-hot-toast";
import withReactContent from "sweetalert2-react-content";
import APIurl from "../api/AppURL";
import CRUDFunction from "../functions/CRUDFunction";

import Swal from "sweetalert2";
const MySwal = withReactContent(Swal);

class LoginPage extends Component {
  constructor() {
    super();
    this.state = {
      role: "",
      email: "",
      password: "",
      reset_email: "",
      loggedIn: false,
      show: false,
    };
    this.onFormSubmit = this.onFormSubmit.bind(this);
    this.forgotPassword = this.forgotPassword.bind(this);
  }

  forgotPassword = async (event) => {
    event.preventDefault();
    const response = await CRUDFunction.password(APIurl.forgotpassword, {
      email: this.state.reset_email,
    });
    if (response) {
      Swal.fire({
        position: "center",
        icon: "success",
        title: "Successfull send to your email",
        showConfirmButton: false,
        timer: 2500,
      });
      this.setState({ show: false });
    }
  };

  componentDidMount() {
    caches.keys().then((names) => {
      names.forEach((name) => {
        caches.delete(name);
      });
    });
  }

  handleInputChange = (e) => {
    if (e.target.id === "email") {
      this.setState({ email: e.target.value });
    } else {
      this.setState({ password: e.target.value });
    }
  };

  onFormSubmit = (event) => {
    event.preventDefault();
    const data = {
      email: this.state.email,
      password: this.state.password,
    };

    if (data) {
      axios
        .post(APIurl.login, data)
        .then((response) => {
          if (response.status == 200) {
            localStorage.setItem("token", response.data.token);
            localStorage.setItem(
              "user",
              JSON.stringify({
                id: response.data.id,
                role: response.data.role,
                company_id: response.data.company_id,
              })
            );

            const Toast = MySwal.mixin({
              toast: true,
              position: "top-end",
              showConfirmButton: false,
              timer: 2000,
              timerProgressBar: true,
              didOpen: (toast) => {
                toast.addEventListener("mouseenter", MySwal.stopTimer);
                toast.addEventListener("mouseleave", MySwal.resumeTimer);
              },
              didClose: (res) => {
                this.setState({
                  loggedIn: true,
                  role: response.data.role[0].role_type,
                });
              },
            });

            Toast.fire({
              icon: "success",
              title: "Signed in successfully",
            });
          } else {
            toast.error(response.data, {
              duration: 4000,
            });
          }
        })
        .catch((err) => {
          toast.error(err.response.data, {
            duration: 4000,
          });
        });
    }
  };

  render() {
    /// After Login Navigate to Profile Page
    if (this.state.loggedIn) {
      return <Navigate to={`/layoutDashboard/${this.state.role}`} />;
    }

    return (
      <Fragment>
        <Toaster />
        <Grid
          textAlign="center"
          style={{ height: "100vh" }}
          verticalAlign="middle"
        >
          <Grid.Column style={{ maxWidth: 500 }}>
            <Image src={logo} />
            <hr />
            <Form size="large" onSubmit={this.onFormSubmit.bind(this)}>
              <Segment stacked>
                <Form.Input
                  fluid
                  icon="user"
                  id="email"
                  iconPosition="left"
                  placeholder="Email"
                  onChange={this.handleInputChange.bind(this)}
                  // required
                />
                <Form.Input
                  fluid
                  icon="lock"
                  iconPosition="left"
                  placeholder="Password"
                  type="password"
                  onChange={this.handleInputChange.bind(this)}
                  // required
                />
                <Form.Field>
                  <Checkbox label="Remember Me" />
                </Form.Field>

                <Message>
                  Forgot Password? &nbsp;
                  <span
                    style={{
                      color: "#4183c4",
                      textDecoration: "none",
                      cursor: "pointer",
                    }}
                    onClick={() => this.setState({ show: true })}
                  >
                    Reset Password
                  </span>
                </Message>
                <Button
                  type="submit"
                  className="ui fluid button"
                  style={{ color: "white", background: "#468c32" }}
                >
                  Login
                </Button>

                <Message>
                  Don't have an account? <Link to="/register">Sign Up Now</Link>
                </Message>
              </Segment>
            </Form>
          </Grid.Column>
        </Grid>
        <Modal
          show={this.state.show}
          onHide={() => this.setState({ show: false })}
        >
          <Form onSubmit={this.forgotPassword}>
            <Modal.Header closeButton>
              <Modal.Title>Reset Your Password</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form.Input
                fluid
                icon="user"
                id="email"
                iconPosition="left"
                placeholder="Email"
                onChange={(e) =>
                  this.setState({
                    reset_email: e.target.value,
                  })
                }
                // required
              />
            </Modal.Body>
            <Modal.Footer>
              <Button
                variant="secondary"
                onClick={() => this.setState({ show: false })}
              >
                Close
              </Button>
              <Button variant="primary" type="submit">
                Reset Password
              </Button>
            </Modal.Footer>
          </Form>
        </Modal>
      </Fragment>
    );
  }
}

export default LoginPage;
