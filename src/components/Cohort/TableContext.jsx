import React, { Component, Fragment } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Table, Modal, Form, Button, Row, Col } from "react-bootstrap";
import APIFunction from "../../functions/APIFunction";
import CRUDFunction from "../../functions/CRUDFunction";
import APIurl from "../../api/AppURL";
import { useNavigate } from "react-router-dom";

class TableContext extends Component {
  constructor(props) {
    super(props);
    console.log(this.props.dataModal);
    this.state = {
      data: {
        id: this.props.dataModal.id,
        course_id: this.props.dataModal.course_id,
        cohort_name: this.props.dataModal.cohort_name,
        cohort_place: this.props.dataModal.cohort_place,
        cohort_state: this.props.dataModal.cohort_state,
        cohort_address: this.props.dataModal.cohort_address,
        cohort_mode: this.props.dataModal.cohort_mode,
        cohort_include: this.props.dataModal.cohort_include,
        cohort_desc: this.props.dataModal.cohort_desc,
        cohort_date_start: this.props.dataModal.cohort_date_start,
        cohort_date_end: this.props.dataModal.cohort_date_end,
      },
    };
    this.handleInput = this.handleInput.bind(this);
  }

  handleInput = (e) => {
    this.setState({
      data: {
        ...this.state.data,
        [e.target.name]: e.target.value,
      },
    });
  };

  render() {
    const isDataExist = this.props.isDataExist;
    if (isDataExist) {
      return (
        <Fragment>
          <div class="bg-darkgreen p-15-20">
            <h3>LIST OF COHORT</h3>
          </div>

          <Table
            hover
            responsive
            size="lg"
            style={{
              textAlign: "center",
              boxShadow: "0 0 10px 0 rgba(0, 0, 0, 0.2)",
            }}
          >
            <thead style={{ background: "#468C32", color: "white" }}>
              <tr>
                {this.props.header.map((elem) => (
                  <th>{elem}</th>
                ))}
              </tr>
            </thead>
            <tbody style={{ border: "0px white", background: "white" }}>
              {this.props.cohortData.map((iteration) => {
                console.log("iteration:", iteration);
                return iteration.map((elem, i) => (
                  <tr>
                    <td>{i + 1}</td>
                    <td>{elem.cohort_name}</td>
                    <td>
                      {Object.keys(elem.cohorts).length > 0
                        ? Object.keys(elem.cohorts).length
                        : "0"}
                    </td>
                    <td>{elem.cohort_place}</td>
                    <td>{elem.cohort_date_start}</td>
                    <td>{elem.cohort_date_end}</td>
                    <td>
                      {" "}
                      <FontAwesomeIcon
                        icon={["fas", "folder-open"]}
                        color="black"
                        size="lg"
                        onClick={() => {
                          this.props.navigate(`details/${elem.id}`);
                        }}
                      />
                      <FontAwesomeIcon
                        icon={["fas", "pen"]}
                        color="blue"
                        size="lg"
                        onClick={(e) => {
                          this.props.getCohortbyID(e, elem.id);
                        }}
                      />
                      <FontAwesomeIcon
                        icon={["fas", "trash"]}
                        color="red"
                        size="lg"
                        onClick={(e) => {
                          this.props.deleteCohort(e, elem.id);
                        }}
                      />
                    </td>
                  </tr>
                ));
              })}
            </tbody>
          </Table>
          <Modal show={this.props.showModal} onHide={!this.props.showModal}>
            <Form
              method="PUT"
              onSubmit={(event) => {
                console.log("e", event.target.elements.cohort_address.value);
                this.props.updateCohort(event, {
                  cohort_name: event.target.elements.cohort_name.value,
                  cohort_place: event.target.elements.cohort_place.value,
                  cohort_state: event.target.elements.cohort_state.value,
                  cohort_address: event.target.elements.cohort_address.value,
                  cohort_mode: event.target.elements.cohort_mode.value,
                  cohort_include: event.target.elements.cohort_include.value,
                  cohort_desc: event.target.elements.cohort_desc.value,
                  cohort_date_start:
                    event.target.elements.cohort_date_start.value,
                  cohort_date_end: event.target.elements.cohort_date_end.value,
                });
              }}
            >
              <Modal.Header closeButton>
                <Modal.Title>EDIT CHORT</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <Form.Group className="mb-3" onChange={this.handleInput}>
                  <Form.Label>COURSE NAME</Form.Label>
                  <Form.Control
                    type="string"
                    name="cohort_name"
                    defaultValue={this.props.dataModal.cohort_name}
                  />
                </Form.Group>
                <Form.Group className="mb-3" onChange={this.handleInput}>
                  <Form.Label>COHORT PLACE</Form.Label>
                  <Form.Control
                    type="string"
                    name="cohort_place"
                    defaultValue={this.props.dataModal.cohort_place}
                  />
                </Form.Group>
                <Form.Group className="mb-3" onChange={this.handleInput}>
                  <Form.Label>STATE</Form.Label>
                  <Form.Select
                    aria-label="Default select state"
                    name="cohort_state"
                  >
                    <option value={this.props.dataModal.cohort_state}>
                      {this.props.dataModal.cohort_state}
                    </option>
                    {this.props.stateData}
                  </Form.Select>
                </Form.Group>
                <Form.Group className="mb-3" onChange={this.handleInput}>
                  <Form.Label>ADDRESS</Form.Label>
                  <Form.Control
                    type="string"
                    name="cohort_address"
                    defaultValue={this.props.dataModal.cohort_address}
                  />
                </Form.Group>
                <Form.Group className="mb-3" onChange={this.handleInput}>
                  <Form.Label>MODE</Form.Label>
                  <Form.Control
                    type="string"
                    name="cohort_mode"
                    defaultValue={this.props.dataModal.cohort_mode}
                  />
                </Form.Group>
                <Form.Group className="mb-3" onChange={this.handleInput}>
                  <Form.Label>COHORT DESCRIPTION</Form.Label>
                  <Form.Control
                    as="textarea"
                    rows={3}
                    name="cohort_desc"
                    defaultValue={this.props.dataModal.cohort_desc}
                  />
                </Form.Group>
                <Form.Group className="mb-3" onChange={this.handleInput}>
                  <Form.Label>COHORT INCLUDE</Form.Label>
                  <Form.Control
                    as="textarea"
                    rows={3}
                    name="cohort_include"
                    defaultValue={this.props.dataModal.cohort_include}
                  />
                </Form.Group>
                <Row>
                  <Col>
                    <Form.Group className="mb-3" onChange={this.handleInput}>
                      <Form.Label>START DATE</Form.Label>
                      <Form.Control
                        type="date"
                        name="cohort_date_start"
                        defaultValue={this.props.dataModal.cohort_date_start}
                      />
                    </Form.Group>
                  </Col>
                  <Col>
                    <Form.Group className="mb-3" onChange={this.handleInput}>
                      <Form.Label>END DATE</Form.Label>
                      <Form.Control
                        type="date"
                        name="cohort_date_end"
                        defaultValue={this.props.dataModal.cohort_date_end}
                      />
                    </Form.Group>
                  </Col>
                </Row>
              </Modal.Body>
              <Modal.Footer>
                <Button variant="primary" onClick={this.props.closeModal}>
                  Close
                </Button>
                <Button variant="primary" type="submit">
                  Update
                </Button>
              </Modal.Footer>
            </Form>
          </Modal>
        </Fragment>
      );
    } else {
      return (
        <Fragment>
          <div class="bg-darkgreen p-15-20">
            <h3>LIST OF COHORT</h3>
          </div>
          <Table
            hover
            responsive
            size="lg"
            style={{
              textAlign: "center",
              boxShadow: "0 0 10px 0 rgba(0, 0, 0, 0.2)",
            }}
          >
            <thead style={{ background: "#468C32", color: "white" }}>
              <tr>
                {this.props.header.map((elem) => (
                  <th>{elem}</th>
                ))}
              </tr>
            </thead>
            <tbody style={{ border: "0px white", background: "white" }}>
              <tr>
                <td>none</td>
                <td>none</td>
                <td>none</td>
                <td>none</td>
                <td>none</td>
                <td>none</td>
                <td>none</td>
              </tr>
            </tbody>
          </Table>
        </Fragment>
      );
    }
  }
}

export default navigateHook(TableContext);

function navigateHook(TableContext) {
  return function WrappedComponent(props) {
    const navigate = useNavigate();
    return <TableContext {...props} navigate={navigate} />;
  };
}
