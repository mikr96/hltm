import React, { Component, Fragment } from "react";
import { Row, Col } from "react-bootstrap";
import APIFunction from "../../functions/APIFunction";
import CRUDFunction from "../../functions/CRUDFunction";
import APIurl from "../../api/AppURL";

export class CohortInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cohortInfo: [],
    };
  }
  componentDidMount = async () => {
    const cohortInfo = await CRUDFunction.get(
      `${APIurl.cohort}/${this.props.id}`,
      APIFunction.getHeader()[0]
    );
    if (cohortInfo) {
      this.setState({
        cohortInfo: cohortInfo,
      });
    }
  };

  render() {
    return (
      <Fragment>
        <div className="bg-darkGreen p-15-20">
          <Row>
            <h3>Cohort Info</h3>
          </Row>
        </div>
        <div
          className="bg-white p-15-20"
          style={{ boxShadow: "0 0 10px 0 rgba(0, 0, 0, 0.2)" }}
        >
          <Row>
            <Col md={8}>
              {" "}
              <strong>Cohort Name</strong>
              <br /> {this.state.cohortInfo.cohort_name}
            </Col>
            <Col md={4}>
              {" "}
              <strong>Cohort Place</strong>
              <br /> {this.state.cohortInfo.cohort_place}
            </Col>
          </Row>
          <Row>
            <br />
          </Row>
          <Row>
            <Col md={4}>
              {" "}
              <strong>Date Start</strong>
              <br /> {this.state.cohortInfo.cohort_date_start}
            </Col>
            <Col md={4}>
              {" "}
              <strong>Date End</strong>
              <br /> {this.state.cohortInfo.cohort_date_end}
            </Col>
            <Col md={4}>
              {" "}
              <strong>Cohort Address</strong>
              <br /> {this.state.cohortInfo.cohort_address}
            </Col>
          </Row>
        </div>
      </Fragment>
    );
  }
}

export default CohortInfo;
