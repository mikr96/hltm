import React, { Component, Fragment } from "react";
import { Table, Dropdown, Button, Form, Modal } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import APIFunction from "../../functions/APIFunction";
import CRUDFunction from "../../functions/CRUDFunction";
import APIurl from "../../api/AppURL";
import Swal from "sweetalert2";

class TraineeList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.id,
      show: false,
      traineeForm: false,
      newTraineeData: [],
      traineeData: [],
      isTraineeExist: false,
      company_list: [],
    };
  }

  componentDidMount = async () => {
    const traineeData = await CRUDFunction.get(
      `${APIurl.trainee}/${this.props.id}`,
      APIFunction.getHeader()[0]
    );
    if (traineeData) {
      if (traineeData.length > 0) {
        this.setState({
          isTraineeExist: true,
          traineeData: [traineeData],
        });
      } else {
        this.setState({
          isTraineeExist: false,
        });
      }
    }
    const companyData = await CRUDFunction.get(
      `${APIurl.company}name`,
      APIFunction.getHeader()[0]
    );
    if (companyData) {
      let company_name = companyData.map((e) => e.company_name);
      this.setState({
        company_list: companyData.map((elem) => (
          <option value={elem.id}>{elem.company_name}</option>
        )),
      });
    }
  };

  addTrainee = async (event) => {
    let formData = {
      cohort_id: parseInt(this.props.id),
      ...this.state.newTraineeData,
    };
    console.log(formData);
    const newdata = await CRUDFunction.create(
      event,
      APIurl.trainee,
      formData,
      APIFunction.getHeader()[0]
    );
    if (newdata) {
      console.log(newdata);
      this.setState({
        traineeData: [newdata],
        show: false,
        isTraineeExist: true,
      });
    }
  };

  deleteTrainee = async (event, id) => {
    event.preventDefault();
    const data = await CRUDFunction.delete(
      `${APIurl.trainee}/${id}`,
      APIFunction.getHeader()[0]
    );
    if (data) {
      Swal.fire({
        title: "Are you sure?",
        text: "You want delete your item?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!",
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire("Deleted!", "Your file has been deleted.", "success");
          this.setState({
            traineeData: [data],
          });
        }
      });
    }
  };

  render() {
    return (
      <Fragment>
        <div
          style={{
            display: "flex",
            justifyContent: "flex-end",
            flexDirection: "row",
            width: "100%",
          }}
        >
          <Button
            className="primary"
            onClick={() => this.setState({ show: true, traineeForm: true })}
          >
            <FontAwesomeIcon icon={["fas", "plus"]} color="white" size="sm" />{" "}
            &nbsp; Add Trainee
          </Button>
        </div>
        <br />
        <div class="bg-darkgreen p-15-20">
          <h3>TRAINEE LIST</h3>
        </div>

        <Table
          hover
          responsive
          size="lg"
          style={{
            textAlign: "center",
            boxShadow: "0 0 10px 0 rgba(0, 0, 0, 0.2)",
          }}
        >
          <thead style={{ background: "#468C32", color: "white" }}>
            <tr>
              <th>NO</th>
              <th>TRAINEE</th>
              <th>COMPANY</th>
              <th>CONTACT</th>
              <th>COHORT INFO</th>
              <th>JOIN STATUS</th>
              <th>PAYMENT STATUS</th>
              <th>HRDC STATUS</th>
              <th>ACTION</th>
            </tr>
          </thead>
          <tbody style={{ border: "0px white", background: "white" }}>
            {this.state.isTraineeExist &&
              this.state.traineeData.map((iteration) => {
                return iteration.map((elem, i) => (
                  <tr>
                    <td>{i + 1}</td>
                    <td>
                      {" "}
                      <strong>{elem.trainee_name}</strong>
                      <br />
                      <small>{elem.trainee_ic}</small>
                    </td>
                    <td>{elem.company_id}</td>
                    <td>
                      <span>
                        Email : <strong>{elem.trainee_email}</strong>{" "}
                      </span>
                      <br />
                      <span>
                        Contact No : <strong> {elem.trainee_phoneno}</strong>{" "}
                      </span>
                    </td>
                    <td>
                      <span>
                        Promo Code : <strong>{elem.promo_code}</strong>{" "}
                      </span>
                      <br />
                      <span>
                        Special Diet : <strong>{elem.allergies}</strong>{" "}
                      </span>
                      <br />
                      <span>
                        Referrer : <strong>{elem.referrer_code}</strong>{" "}
                      </span>
                      <br />
                    </td>
                    <td>{elem.trainee_status}</td>
                    <td>
                      {elem.trainee_payment}

                      {/* <FontAwesomeIcon
                        icon={["fas", "pen"]}
                        color="blue"
                        size="lg"
                        onClick={(e) => {
                          this.props.getTrainingbyID(e, elem.id);
                        }}
                      /> */}
                    </td>
                    <td>Yes</td>
                    <td>
                      <Dropdown>
                        <Dropdown.Toggle
                          variant="success"
                          id="dropdown-basic"
                        ></Dropdown.Toggle>

                        <Dropdown.Menu>
                          <Dropdown.Item
                            onClick={() => {
                              this.props.navigate(
                                "/layoutDashboard/attendance"
                              );
                            }}
                          >
                            Attendance
                          </Dropdown.Item>
                          <Dropdown.Item href="#/action-2">
                            Status Change
                          </Dropdown.Item>
                          <Dropdown.Item href="#/action-3">
                            Profoma Invoice
                          </Dropdown.Item>
                          <Dropdown.Item href="#/action-3">
                            Resend Profoma
                          </Dropdown.Item>
                          <Dropdown.Item href="#/action-3">
                            Generate Invoice
                          </Dropdown.Item>
                          <Dropdown.Item href="#/action-3">
                            Send HRDC
                          </Dropdown.Item>
                          <Dropdown.Item href="#/action-3">
                            Proof Payment
                          </Dropdown.Item>
                          <Dropdown.Item>Update</Dropdown.Item>
                          <Dropdown.Item
                            onClick={() => {
                              console.log("hello");
                            }}
                          >
                            Delete
                          </Dropdown.Item>
                        </Dropdown.Menu>
                      </Dropdown>
                    </td>
                  </tr>
                ));
              })}
            {!this.state.isTraineeExist && (
              <tr>
                <td>0</td>
                <td>
                  {" "}
                  <strong>None</strong>
                  <br />
                  <small>None</small>
                </td>
                <td>None</td>
                <td>
                  <span>
                    Email : <strong>None</strong>{" "}
                  </span>
                  <br />
                  <span>
                    Contact No : <strong>None</strong>{" "}
                  </span>
                </td>
                <td>
                  <span>
                    Promo Code : <strong>None</strong>{" "}
                  </span>
                  <br />
                  <span>
                    Special Diet : <strong>None</strong>{" "}
                  </span>
                  <br />
                  <span>
                    Referrer : <strong>None</strong>{" "}
                  </span>
                  <br />
                </td>
                <td>None</td>
                <td>None</td>
                <td>Ongoing</td>
                <td>
                  <Dropdown>
                    <Dropdown.Toggle
                      variant="success"
                      id="dropdown-basic"
                    ></Dropdown.Toggle>

                    <Dropdown.Menu>
                      <Dropdown.Item
                        onClick={() => {
                          this.props.navigate("/layoutDashboard/attendance");
                        }}
                      >
                        Attendance
                      </Dropdown.Item>
                      <Dropdown.Item href="#/action-2">
                        Status Change
                      </Dropdown.Item>
                      <Dropdown.Item href="#/action-3">
                        Profoma Invoice
                      </Dropdown.Item>
                      <Dropdown.Item href="#/action-3">
                        Resend Profoma
                      </Dropdown.Item>
                      <Dropdown.Item href="#/action-3">
                        Generate Invoice
                      </Dropdown.Item>
                      <Dropdown.Item href="#/action-3">Send HRDC</Dropdown.Item>
                      <Dropdown.Item href="#/action-3">
                        Proof Payment
                      </Dropdown.Item>
                      <Dropdown.Item>Update</Dropdown.Item>
                      <Dropdown.Item
                        onClick={() => {
                          console.log("hello");
                        }}
                      >
                        Delete
                      </Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                </td>
              </tr>
            )}
          </tbody>
        </Table>
        <Modal
          show={this.state.show}
          onHide={() =>
            this.setState({
              show: false,
            })
          }
        >
          <Modal.Header closeButton>
            <Modal.Title>Trainee Form</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group
              className="mb-3"
              onChange={(e) => {
                this.setState({
                  newTraineeData: {
                    ...this.state.newTraineeData,
                    [e.target.name]: e.target.value,
                  },
                });
              }}
            >
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="text"
                name="trainee_name"
                placeholder="Enter Your Name"
              />
            </Form.Group>
            <Form.Group
              className="mb-3"
              onChange={(e) => {
                this.setState({
                  newTraineeData: {
                    ...this.state.newTraineeData,
                    [e.target.name]: e.target.value,
                  },
                });
              }}
            >
              <Form.Label>IC No</Form.Label>
              <Form.Control
                type="text"
                name="trainee_ic"
                placeholder="Enter Your IC No"
              />
            </Form.Group>
            <Form.Group
              className="mb-3"
              onChange={(e) => {
                this.setState({
                  newTraineeData: {
                    ...this.state.newTraineeData,
                    [e.target.name]: e.target.value,
                  },
                });
              }}
            >
              <Form.Label>Phone No</Form.Label>
              <Form.Control
                type="text"
                name="trainee_phoneno"
                placeholder="Enter Your Phone No"
              />
            </Form.Group>
            <Form.Group
              className="mb-3"
              onChange={(e) => {
                this.setState({
                  newTraineeData: {
                    ...this.state.newTraineeData,
                    [e.target.name]: e.target.value,
                  },
                });
              }}
            >
              <Form.Label>Email</Form.Label>
              <Form.Control
                type="text"
                name="trainee_email"
                placeholder="Enter Your Email"
              />
            </Form.Group>
            <Form.Group
              className="mb-3"
              onChange={(e) => {
                this.setState({
                  newTraineeData: {
                    ...this.state.newTraineeData,
                    [e.target.name]: e.target.value,
                  },
                });
              }}
            >
              <Form.Label class="required">Company Name</Form.Label>
              <Form.Select
                name="company_id"
                aria-label="Default select example"
              >
                <option value="0">Open this select menu</option>
                {this.state.company_list}
              </Form.Select>
            </Form.Group>
            <Form.Group
              className="mb-3"
              onChange={(e) => {
                this.setState({
                  newTraineeData: {
                    ...this.state.newTraineeData,
                    [e.target.name]: e.target.value,
                  },
                });
              }}
            >
              <Form.Label>Promo Code</Form.Label>
              <Form.Control
                type="text"
                name="promo_code"
                placeholder="Enter Your Promo Code"
              />
            </Form.Group>
            <Form.Group
              className="mb-3"
              onChange={(e) => {
                this.setState({
                  newTraineeData: {
                    ...this.state.newTraineeData,
                    [e.target.name]: e.target.value,
                  },
                });
              }}
            >
              <Form.Label>
                Special Dietary Needs / Allergies (if any)
              </Form.Label>
              <Form.Control
                type="text"
                name="allergies"
                placeholder="Special Dietary Needs / Allergies"
              />
            </Form.Group>
            <Form.Group
              className="mb-3"
              onChange={(e) => {
                this.setState({
                  newTraineeData: {
                    ...this.state.newTraineeData,
                    [e.target.name]: e.target.value,
                  },
                });
              }}
            >
              <Form.Label>Referrer</Form.Label>
              <Form.Control
                type="text"
                name="referrer_code"
                placeholder="Enter Your Referrer"
              />
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button
              variant="primary"
              onClick={() =>
                this.setState({
                  show: false,
                })
              }
            >
              Close
            </Button>
            <Button
              variant="primary"
              onClick={(event) => this.addTrainee(event)}
            >
              Add
            </Button>
          </Modal.Footer>
        </Modal>
      </Fragment>
    );
  }
}

export default TraineeList;
