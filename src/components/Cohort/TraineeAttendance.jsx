import React, { Component, Fragment } from "react";
import { Button, Table, Form, Modal } from "react-bootstrap";
import APIFunction from "../../functions/APIFunction";
import CRUDFunction from "../../functions/CRUDFunction";
import APIurl from "../../api/AppURL";

export class TraineeAttendance extends Component {
  constructor() {
    super();
    this.state = {
      traineeData: [],
    };
  }

  componentDidMount = async () => {
    const traineeData = await CRUDFunction.get(
      `${APIurl.trainee}/${this.props.id}`,
      APIFunction.getHeader()[0]
    );
    if (traineeData) {
      console.log(traineeData);
      this.setState({
        traineeData: [traineeData],
      });
    }
  };

  render() {
    return (
      <Fragment>
        <h3 style={{ textAlign: "center" }}>PCHE Withdraw Participation</h3>
        <div
          style={{
            display: "flex",
            justifyContent: "flex-end",
            flexDirection: "row",
            width: "100%",
          }}
        >
          <Button
            onClick={() => this.setState({ show: true })}
            style={{ margin: "5px" }}
            variant="primary"
          >
            Indicator
          </Button>

          <Button style={{ margin: "5px" }} variant="primary">
            Print
          </Button>
        </div>

        <br />

        <div class="bg-darkgreen p-15-20">
          <h3>List of Trainee</h3>
        </div>
        <Table
          striped
          bordered
          hover
          size="sm"
          style={{
            textAlign: "center",
            boxShadow: "0 0 10px 0 rgba(0, 0, 0, 0.2)",
          }}
        >
          <thead>
            <tr>
              <th>NO</th>
              <th>NAME</th>
              <th>PHONE NO</th>
              <th>STATUS</th>
            </tr>
          </thead>
          <tbody>
            {this.state.traineeData.map((iteration) => {
              return iteration.map((elem, i) => (
                <tr>
                  <td>{i + 1}</td>
                  <td>{elem.trainee_name}</td>
                  <td>{elem.trainee_phoneno}</td>
                  <td>{elem.traine_status}</td>
                </tr>
              ));
            })}
          </tbody>
        </Table>

        <Modal
          show={this.state.show}
          onHide={() =>
            this.setState({
              show: false,
              indicator: false,
              trainee: false,
              print: false,
            })
          }
        >
          <Modal.Header closeButton>
            <Modal.Title>Attendance Indicator</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Input
              fluid
              icon="clock"
              id="code"
              iconPosition="left"
              placeholder="Code"
            />
          </Modal.Body>{" "}
          <Modal.Body>
            <Form.Input
              fluid
              icon="clock"
              id="notes"
              iconPosition="left"
              placeholder="Notes"
            />
          </Modal.Body>{" "}
          <Modal.Footer>
            <Button
              variant="primary"
              onClick={() =>
                this.setState({
                  show: false,
                  discount: false,
                  stateList: false,
                })
              }
            >
              Close
            </Button>
            <Button
              variant="primary"
              onClick={() =>
                this.setState({
                  show: false,
                  indicator: false,
                  trainee: false,
                  print: false,
                })
              }
              onClick={this.alert}
            >
              Add
            </Button>
          </Modal.Footer>
        </Modal>
      </Fragment>
    );
  }
}

export default TraineeAttendance;
