import React, { Component, Fragment } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Table } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import "./../../assets/css/invoice.css";
import APIFunction from "../../functions/APIFunction";
import CRUDFunction from "../../functions/CRUDFunction";
import APIurl from "../../api/AppURL";
import {
  PrintInvoice,
  PrintProforma,
  PrintReceipt,
} from "./../../pages/Dashboard/InvoicePage";

class TableInvoice extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        company_id: this.props.invoiceData.company_id,
        course_id: this.props.invoiceData.course_id,
        invoice_num: this.props.invoiceData.invoice_num,
        invoice_desc: this.props.invoiceData.invoice_desc,
        invoice_date: this.props.invoiceData.invoice_date,
      },
      id: "",
      print: false,
      isBillerExist: false,
      companyDetails: [],
      cohortDetails: [],
    };
    this.handleInput = this.handleInput.bind(this);
  }

  handleInput = (e) => {
    this.setState({
      data: {
        ...this.state.data,
        [e.target.name]: e.target.value,
      },
    });
  };

  getCohortName = (elem) => {
    console.log(elem);
    if (this.props.cohortNames.length > 0) {
      let indexCohortName = this.props.cohortIds.findIndex((x) => elem == x);
      if (indexCohortName !== -1) {
        return this.props.cohortNames[indexCohortName];
      } else {
        return elem;
      }
    }
  };

  getCompanyName = (elem) => {
    if (this.props.companyNames.length > 0) {
      let indexCompanyName = this.props.companyIds.findIndex((x) => elem == x);
      if (indexCompanyName !== -1) {
        return this.props.companyNames[indexCompanyName];
      } else {
        return elem;
      }
    }
  };

  componentDidMount = async () => {
    const billingData = await CRUDFunction.get(
      `${APIurl.billing}`,
      APIFunction.getHeader()[0]
    );
    if (billingData) {
      this.setState({
        billingData: billingData,
      });
    }
  };

  render() {
    return (
      <Fragment>
        <Table
          hover
          responsive
          size="lg"
          style={{
            textAlign: "center",
            boxShadow: "0 0 10px 0 rgba(0, 0, 0, 0.2)",
          }}
          id="tableInvoice"
        >
          <thead style={{ background: "#468C32", color: "white" }}>
            <tr>
              {this.props.header.map((disc) => (
                <th>{disc}</th>
              ))}
            </tr>
          </thead>
          <tbody style={{ border: "0px white", background: "white" }}>
            {this.props.isDataExist &&
              this.props.invoiceData.map((iteration) => {
                return iteration.map((disc, i) => (
                  <tr>
                    <td>{i + 1}</td>
                    <td>{this.getCohortName(disc.cohort_id)}</td>
                    <td>{this.getCompanyName(disc.company_id)}</td>
                    <td>{disc.invoice_num}</td>
                    <td>{disc.invoice_date}</td>
                    <td>{disc.invoice_due}</td>
                    <td>{disc.invoice_amount}</td>
                    <td>
                      <FontAwesomeIcon
                        icon={["fas", "print"]}
                        color="#191919"
                        size="lg"
                        onClick={(e) => {
                          this.setState(
                            {
                              id: i,
                              companyDetails: disc.company,
                              cohortDetails: disc.cohort,
                              isBillerExist: true,
                            },
                            () => {
                              setTimeout(function () {
                                window.print();
                                window.close();
                              }, 250);
                            }
                          );
                        }}
                      />
                      &nbsp;
                      <FontAwesomeIcon
                        icon={["fas", "trash"]}
                        color="red"
                        size="lg"
                        onClick={(e) => {
                          this.props.deleteInvoice(e, disc.id);
                        }}
                      />
                    </td>
                  </tr>
                ));
              })}
            {!this.props.isDataExist && (
              <tr>
                <td>0</td>
                <td>none</td>
                <td>none</td>
                <td>none</td>
                <td>none</td>
                <td>none</td>
                <td></td>
              </tr>
            )}
          </tbody>
        </Table>
        <PrintInvoice
          biller={this.state.billingData}
          invoiceDetail={this.props.invoiceData}
          isBillerExist={this.state.isBillerExist}
          companyDetails={this.state.companyDetails}
          cohortDetails={this.state.cohortDetails}
          id={this.state.id}
        />
        {/* <PrintProforma /> */}
        {/* <PrintReceipt /> */}
      </Fragment>
    );
  }
}

export default navigateHook(TableInvoice);

function navigateHook(TableInvoice) {
  return function WrappedComponent(props) {
    const navigate = useNavigate();
    return <TableInvoice {...props} navigate={navigate} />;
  };
}
