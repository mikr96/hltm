import React, { Component, Fragment } from "react";
import TableInvoice from "../Invoice/TableInvoice";
import { Row, Col, Button, Modal, Dropdown, Form } from "react-bootstrap";
import APIFunction from "../../functions/APIFunction";
import CRUDFunction from "../../functions/CRUDFunction";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import APIurl from "../../api/AppURL";
import Swal from "sweetalert2";

class Invoice extends Component {
  constructor() {
    super();
    this.state = {
      result: "",
      state: false,
      newdata: [],
      showModal: false,
      header: [
        "NO",
        "COHORT",
        "COMPANY",
        "NUMBER",
        "ISSUED DATE",
        "DUE DATE",
        "AMOUNT",
        "ACTION",
      ],
      dataModal: [],
      id: "",
      isDataExist: false,
      invoiceData: [],
      courseId: [],
      companyName: [],
      courseId: [],
      companyName: [],
    };
    this.deleteInvoice = this.deleteInvoice.bind(this);
    this.updateInvoice = this.updateInvoice.bind(this);
    this.getInvoicebyID = this.getInvoicebyID.bind(this);
    this.addInvoice = this.addInvoice.bind(this);
  }

  getInvoicebyID = async (id) => {
    const dataModal = await CRUDFunction.get(
      `${APIurl.invoice}/${id}`,
      APIFunction.getHeader()[0]
    );
    if (dataModal) {
      this.setState({
        dataModal: dataModal,
        showModal: true,
        id: id,
      });
    }
  };

  addInvoice = async (event) => {
    const newdata = await CRUDFunction.create(
      event,
      APIurl.invoice,
      this.state.newdata,
      APIFunction.getHeader()[0]
    );
    if (newdata) {
      this.setState({
        invoiceData: [newdata],
        show: false,
        state: false,
      });
    }
  };

  updateInvoice = async (event, formdata) => {
    const data = await CRUDFunction.update(
      event,
      `${APIurl.invoice}/${this.state.id}`,
      formdata,
      APIFunction.getHeader()[0]
    );
    if (data) {
      console.log("data after update: ", data);
      this.setState({
        showModal: false,
        stateData: [data],
      });
    }
  };

  deleteInvoice = async (event, id) => {
    event.preventDefault();
    const data = await CRUDFunction.delete(
      `${APIurl.invoice}/${id}`,
      APIFunction.getHeader()[0]
    );
    if (data) {
      Swal.fire({
        title: "Are you sure?",
        text: "You want delete your item?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!",
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire("Deleted!", "Your file has been deleted.", "success");
          this.setState({
            invoiceData: [data],
          });
        }
      });
    }
  };

  componentDidMount = async () => {
    try {
      const response = await CRUDFunction.get(
        `${APIurl.invoice}`,
        APIFunction.getHeader()[0]
      );

      const invoiceData = await response;
      console.log(invoiceData);
      if (invoiceData.data.length > 0) {
        this.setState({
          isDataExist: true,
          invoiceData: [invoiceData.data],
          cohortName: this.props.cohortDetail.map((e) => e.cohort_name),
          cohortId: this.props.cohortDetail.map((e) => e.id),
          companyName: this.props.companyDetail.map((e) => e.company_name),
          companyId: this.props.companyDetail.map((e) => e.id),
        });
      }
    } catch (error) {
      console.log(error);
    }
  };

  closeModal = () => {
    this.setState({
      showModal: false,
    });
  };

  render() {
    return (
      <Fragment>
        <Row style={{ marginLeft: "0px", marginRight: "0px" }} id="subheader">
          <Col md={6}>
            <h3 style={{ lineHeight: "2.3" }}>INVOICE LIST</h3>
          </Col>
          <Col md={6} style={{ paddingRight: "0px" }}>
            <div
              style={{
                width: "100%",
                float: "right",
                display: "flex",
                justifyContent: "flex-end",
                paddingRight: "0px",
              }}
            >
              <Button
                onClick={() => {
                  this.setState({ show: true, state: true });
                }}
                className="primary"
              >
                <FontAwesomeIcon
                  icon={["fas", "plus"]}
                  color="white"
                  size="sm"
                />
                &nbsp; CREATE
              </Button>
            </div>
          </Col>
        </Row>

        {this.state.state && (
          <Modal
            show={this.state.show}
            onHide={() => {
              this.setState({
                show: false,
              });
            }}
          >
            <Modal.Header closeButton>
              <Modal.Title>ADD INVOICE</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form>
                <Form.Group
                  className="mb-3"
                  onChange={(e) => {
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        [e.target.name]: parseInt(e.target.value),
                      },
                    });
                  }}
                >
                  <Form.Label>SELECT COHORT</Form.Label>
                  <Form.Select
                    name="course_id"
                    aria-label="Default select example"
                  >
                    <option>Open this select menu</option>
                    {this.props.cohortDetail.map((elem) => (
                      <option value={elem.id}>{elem.cohort_name}</option>
                    ))}
                  </Form.Select>
                </Form.Group>
                <Form.Group
                  className="mb-3"
                  onChange={(e) => {
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        [e.target.name]: parseInt(e.target.value),
                      },
                    });
                  }}
                >
                  <Form.Label>SELECT COMPANY</Form.Label>
                  <Form.Select
                    name="company_id"
                    aria-label="Default select example"
                  >
                    <option>Open this select menu</option>
                    {this.props.companyDetail.map((elem) => (
                      <option value={elem.id}>{elem.company_name}</option>
                    ))}
                  </Form.Select>
                </Form.Group>

                <Form.Group
                  className="mb-3"
                  onChange={(e) => {
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        [e.target.name]: e.target.value,
                      },
                    });
                  }}
                >
                  <Form.Label>Invoice Number</Form.Label>
                  <Form.Control
                    type="string"
                    name="invoice_num"
                    placeholder="Enter invoice number"
                  />
                </Form.Group>

                <Form.Group
                  className="mb-3"
                  onChange={(e) => {
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        [e.target.name]: e.target.value,
                      },
                    });
                  }}
                >
                  <Form.Label>DATE</Form.Label>
                  <Form.Control
                    type="date"
                    name="invoice_date"
                    placeholder="Enter year invoice"
                  />
                </Form.Group>

                <Form.Group
                  className="mb-3"
                  onChange={(e) => {
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        [e.target.name]: e.target.value,
                      },
                    });
                  }}
                >
                  <Form.Label>PAYMENT DUE</Form.Label>
                  <Form.Control
                    type="date"
                    name="invoice_due"
                    placeholder="Enter payment due"
                  />
                </Form.Group>
                <Form.Group
                  className="mb-3"
                  onChange={(e) => {
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        [e.target.name]: e.target.value,
                      },
                    });
                  }}
                >
                  <Form.Label>PAYMENT AMOUNT (RM)</Form.Label>
                  <Form.Control
                    type="number"
                    name="invoice_amount"
                    placeholder="Enter payment amount"
                  />
                </Form.Group>
              </Form>
            </Modal.Body>

            <Modal.Footer>
              <Button
                variant="primary"
                onClick={() =>
                  this.setState({
                    show: false,
                    state: false,
                  })
                }
              >
                Close
              </Button>
              <Button variant="primary" onClick={this.addInvoice}>
                Add
              </Button>
            </Modal.Footer>
          </Modal>
        )}
        <Row>
          <Col md={12}>
            <TableInvoice
              header={this.state.header}
              invoiceData={this.state.invoiceData}
              showModal={this.state.showModal}
              dataModal={this.state.dataModal}
              isDataExist={this.state.isDataExist}
              cohortNames={this.state.cohortName}
              cohortIds={this.state.cohortId}
              companyNames={this.state.companyName}
              companyIds={this.state.companyId}
              updateInvoice={this.updateInvoice}
              getInvoicebyID={this.getInvoicebyID}
              closeModal={this.closeModal}
              deleteInvoice={this.deleteInvoice}
            ></TableInvoice>
          </Col>
        </Row>
      </Fragment>
    );
  }
}

export default Invoice;
