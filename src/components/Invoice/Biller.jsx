import React, { Component, Fragment } from "react";
import { Row, Col, Button, Container } from "react-bootstrap";
import axios from "axios";
import Form from "react-bootstrap/Form";
import APIFunction from "../../functions/APIFunction";
import CRUDFunction from "../../functions/CRUDFunction";
import APIurl from "../../api/AppURL";
import Swal from "sweetalert2";

export class Biller extends Component {
  constructor() {
    super();
    this.state = { billingData: [], biller_name: "" };

    this.updateBiller = this.updateBiller.bind(this);
  }

  updateBiller = async (event, data) => {
    event.preventDefault();
    console.log(data);
    const biller = await CRUDFunction.update(
      event,
      `${APIurl.billing}/1`,
      data,
      APIFunction.getHeader()[0]
    );
    if (biller) {
      Swal.fire({
        position: "top-center",
        icon: "success",
        title: "Successfully updated",
        showConfirmButton: false,
        timer: 2500,
      }).then(() => {
        this.setState({
          biller_name: biller.biller_name,
          biller_address: biller.biller_address,
          biller_phone_no: biller.biller_phone_no,
          biller_website: biller.biller_website,
          biller_ssm: biller.biller_ssm,
        });
      });
    }
  };

  componentDidMount = async () => {
    const billingData = await CRUDFunction.get(
      `${APIurl.billing}`,
      APIFunction.getHeader()[0]
    );
    if (billingData) {
      this.setState({
        isDataExist: true,
        billingData: [billingData],
        biller_name: billingData.biller_name,
        biller_address:
          billingData.biller_address.length < 6
            ? billingData.biller_address
            : JSON.parse(billingData.biller_address),
        biller_phone_no: billingData.biller_phone_no,
        biller_website: billingData.biller_website,
        biller_ssm: billingData.biller_ssm,
      });
    }
  };
  render() {
    return (
      <Fragment>
        <div class="bg-darkGreen p-15-20">
          <h3>BILLER INFORMATIONS</h3>
        </div>

        <div
          style={{ boxShadow: "0 0 10px 0 rgba(0, 0, 0, 0.2)" }}
          className="bg-white p-15-20"
        >
          <Form
            onSubmit={(e) => {
              e.preventDefault();
              this.updateBiller(e, {
                biller_name: e.target.elements.biller_name.value,
                biller_address: JSON.stringify([
                  e.target.elements.line_1.value,
                  e.target.elements.line_2.value,
                  e.target.elements.line_3.value,
                  e.target.elements.city.value,
                ]),
                biller_ssm: e.target.elements.biller_ssm.value,
                biller_website: e.target.elements.biller_website.value,
                biller_phone_no: e.target.elements.biller_phone_no.value,
              });
            }}
          >
            <Row>
              <Form.Group as={Col} controlId="formGridEmail">
                <Form.Label>Biller Name</Form.Label>
                <Form.Control
                  name="biller_name"
                  style={{ marginBottom: "0.5rem" }}
                  defaultValue={this.state.biller_name}
                  onChange={(e) => {
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        [e.target.name]: e.target.value,
                      },
                    });
                  }}
                />

                <Form.Label>SSM No.</Form.Label>
                <Form.Control
                  name="biller_ssm"
                  style={{ marginBottom: "0.5rem" }}
                  defaultValue={this.state.biller_ssm}
                  onChange={(e) => {
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        [e.target.name]: e.target.value,
                      },
                    });
                  }}
                />

                <Form.Label>City</Form.Label>
                <Form.Control
                  name="city"
                  style={{ marginBottom: "0.5rem" }}
                  defaultValue={
                    typeof this.state.biller_address === "object"
                      ? this.state.biller_address[3]
                      : this.state.biller_address
                  }
                  onChange={(e) => {
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        [e.target.name]: e.target.value,
                      },
                    });
                  }}
                />
              </Form.Group>

              <Form.Group as={Col} controlId="formGridPassword">
                <Form.Label>Biller Address</Form.Label>
                <Form.Control
                  name="line_1"
                  placeholder="Line 1"
                  style={{ marginBottom: "0.5rem" }}
                  defaultValue={
                    typeof this.state.biller_address === "object"
                      ? this.state.biller_address[0]
                      : this.state.biller_address
                  }
                  onChange={(e) => {
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        [e.target.name]: e.target.value,
                      },
                    });
                  }}
                />
                <br />
                <Form.Control
                  name="line_2"
                  placeholder="Line 2"
                  style={{ margin: "0.5rem 0rem" }}
                  defaultValue={
                    typeof this.state.biller_address === "object"
                      ? this.state.biller_address[1]
                      : this.state.biller_address
                  }
                  onChange={(e) => {
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        [e.target.name]: e.target.value,
                      },
                    });
                  }}
                />
                <br />

                <Form.Control
                  name="line_3"
                  placeholder="Line 3"
                  style={{ margin: "0.5rem 0rem" }}
                  defaultValue={
                    typeof this.state.biller_address === "object"
                      ? this.state.biller_address[2]
                      : this.state.biller_address
                  }
                  onChange={(e) => {
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        [e.target.name]: e.target.value,
                      },
                    });
                  }}
                />
              </Form.Group>
            </Row>
            <Row>
              <Form.Group as={Col} controlId="formGridEmail">
                <Form.Label>Office Phone No</Form.Label>
                <Form.Control
                  name="biller_phone_no"
                  style={{ marginBottom: "0.5rem" }}
                  defaultValue={this.state.biller_phone_no}
                  onChange={(e) => {
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        [e.target.name]: e.target.value,
                      },
                    });
                  }}
                />
              </Form.Group>
              <Form.Group as={Col} controlId="formGridPassword">
                <Form.Label>Website</Form.Label>
                <Form.Control
                  name="biller_website"
                  defaultValue={this.state.biller_website}
                  onChange={(e) => {
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        [e.target.name]: e.target.value,
                      },
                    });
                  }}
                />
              </Form.Group>
            </Row>
            <br />
            <div
              style={{
                display: "flex",
                justifyContent: "flex-end",
                flexDirection: "row",
                width: "100%",
              }}
            >
              <Button
                type="submit"
                onClick={() => {
                  this.setState({ show: true });
                }}
                style={{ margin: "5px" }}
                variant="primary"
              >
                Update
              </Button>

              {/* <Button style={{ margin: "5px" }} variant="primary">
                Generate
              </Button> */}
            </div>
            <br />
          </Form>
        </div>
      </Fragment>
    );
  }
}

export default Biller;
