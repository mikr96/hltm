import React, { Component } from "react";
import { Accordion } from "react-bootstrap";

export class AccordianPanel extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Accordion>
        <Accordion.Item eventKey={this.props.id}>
          <Accordion.Header>{this.props.question}</Accordion.Header>
          <Accordion.Body>
            <b>{this.props.answer}</b>
          </Accordion.Body>
        </Accordion.Item>
        <br />
      </Accordion>
    );
  }
}

export default AccordianPanel;
