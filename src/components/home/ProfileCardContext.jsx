import React, { Component, Fragment } from "react";
import { Image } from "react-bootstrap";

class ProfileCardContext extends Component {
  constructor(props) {
    super(props);
    console.log(props);
  }

  render() {
    return (
      <Fragment>
        <div
          className="cardDashboard"
          style={{ textAlign: "center", height: "50vh" }}
        >
          <Image
            className="center"
            style={{ width: "80%" }}
            src={this.props.image}
          />
          <div>
            <span>
              <b>{this.props.name}</b>
            </span>
            <br />
            <br />
            <p>{this.props.background}</p>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default ProfileCardContext;
