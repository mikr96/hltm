import React, { Component, Fragment } from "react";
import { Row, Col, Container } from "react-bootstrap";

export class TimeCountdown extends Component {
  constructor() {
    super();
    this.state = {
      time: 5.0,
      minute: "",
      second: "",
      isDataExist: false,
    };
  }

  componentDidMount = () => {
    let fiveMinutes = 60 * 5;
    this.startTimer(fiveMinutes);
  };

  startTimer = (duration) => {
    let timer = duration;
    let minutes, seconds, minute, second;
    setInterval(() => {
      minutes = parseInt(timer / 60);
      seconds = parseInt(timer % 60);

      const minit = minutes < 10 ? "0" + minutes : minutes.toString();
      const saat = seconds < 10 ? "0" + seconds : seconds.toString();

      if (saat) {
        this.setState({
          minute: minit,
          second: saat,
          isDataExist: true,
        });
      }

      if (--timer < 0) {
        timer = duration;
      }
    }, 1000);
  };

  render() {
    const isDataExist = this.state.isDataExist;
    if (isDataExist) {
      return (
        <Fragment>
          <div>
            <br />
            <Row>
              <Col md={12}>
                <h2 style={{ fontSize: "30px", textAlign: "center" }}>
                  <strong>COUNTDOWN</strong>
                </h2>
              </Col>
            </Row>
            <br />
            <Row>
              <Col md={4}>
                <h3 style={{ textAlign: "center" }}>Days</h3>
                <div>
                  <div
                    style={{
                      backgroundColor: "yellow",
                      height: "25vh",
                      margin: "10px",
                      width: "44%",
                      display: "inline-block",
                      position: "relative",
                    }}
                  >
                    <span
                      style={{
                        fontSize: "100px",
                        position: "absolute",
                        left: "50%",
                        top: "50%",
                        transform: "translate(-50%, 0)",
                      }}
                    >
                      {this.state.time}
                    </span>
                  </div>
                  <div
                    style={{
                      backgroundColor: "yellow",
                      height: "25vh",
                      margin: "10px",
                      width: "44%",
                      display: "inline-block",
                      position: "relative",
                    }}
                  >
                    <span
                      style={{
                        fontSize: "100px",
                        position: "absolute",
                        left: "50%",
                        top: "50%",
                        transform: "translate(-50%, 0)",
                      }}
                    >
                      9
                    </span>
                  </div>
                </div>
              </Col>
              <Col md={4}>
                <h3 style={{ textAlign: "center" }}>Minutes</h3>
                <div>
                  <div
                    style={{
                      backgroundColor: "yellow",
                      height: "25vh",
                      margin: "10px",
                      width: "44%",
                      display: "inline-block",
                      position: "relative",
                    }}
                  >
                    <span
                      style={{
                        fontSize: "100px",
                        position: "absolute",
                        left: "50%",
                        top: "50%",
                        transform: "translate(-50%, 0)",
                      }}
                    >
                      {this.state.minute[0]}
                    </span>
                  </div>
                  <div
                    style={{
                      backgroundColor: "yellow",
                      height: "25vh",
                      margin: "10px",
                      width: "44%",
                      display: "inline-block",
                      position: "relative",
                    }}
                  >
                    <span
                      style={{
                        fontSize: "100px",
                        position: "absolute",
                        left: "50%",
                        top: "50%",
                        transform: "translate(-50%, 0)",
                      }}
                    >
                      {this.state.minute[1]}
                    </span>
                  </div>
                </div>
              </Col>
              <Col md={4}>
                <h3 style={{ textAlign: "center" }}>Seconds</h3>
                <div>
                  <div
                    style={{
                      backgroundColor: "yellow",
                      height: "25vh",
                      margin: "10px",
                      width: "44%",
                      display: "inline-block",
                      position: "relative",
                    }}
                  >
                    <span
                      style={{
                        fontSize: "100px",
                        position: "absolute",
                        left: "50%",
                        top: "50%",
                        transform: "translate(-50%, 0)",
                      }}
                    >
                      {this.state.second[0]}
                    </span>
                  </div>
                  <div
                    style={{
                      backgroundColor: "yellow",
                      height: "25vh",
                      margin: "10px",
                      width: "44%",
                      display: "inline-block",
                      position: "relative",
                    }}
                  >
                    <span
                      style={{
                        fontSize: "100px",
                        position: "absolute",
                        left: "50%",
                        top: "50%",
                        transform: "translate(-50%, 0)",
                      }}
                    >
                      {this.state.second[1]}
                    </span>
                  </div>
                </div>
              </Col>
            </Row>
          </div>
        </Fragment>
      );
    } else {
      return (
        <Fragment>
          <div>
            <br />
            <Row>
              <Col md={12}>
                <h2 style={{ fontSize: "30px" }}>
                  <strong>Countdown</strong>
                </h2>
              </Col>
            </Row>
            <br />
            <Row>
              <Col md={4}>
                <h3 style={{ textAlign: "center" }}>Days</h3>
                <div>
                  <div
                    style={{
                      backgroundColor: "yellow",
                      height: "25vh",
                      margin: "10px",
                      width: "44%",
                      display: "inline-block",
                      position: "relative",
                    }}
                  >
                    <span
                      style={{
                        fontSize: "100px",
                        position: "absolute",
                        left: "50%",
                        top: "50%",
                        transform: "translate(-50%, 0)",
                      }}
                    >
                      0
                    </span>
                  </div>
                  <div
                    style={{
                      backgroundColor: "yellow",
                      height: "25vh",
                      margin: "10px",
                      width: "44%",
                      display: "inline-block",
                      position: "relative",
                    }}
                  >
                    <span
                      style={{
                        fontSize: "100px",
                        position: "absolute",
                        left: "50%",
                        top: "50%",
                        transform: "translate(-50%, 0)",
                      }}
                    >
                      0
                    </span>
                  </div>
                </div>
              </Col>
              <Col md={4}>
                <h3 style={{ textAlign: "center" }}>Minutes</h3>
                <div>
                  <div
                    style={{
                      backgroundColor: "yellow",
                      height: "25vh",
                      margin: "10px",
                      width: "44%",
                      display: "inline-block",
                      position: "relative",
                    }}
                  >
                    <span
                      style={{
                        fontSize: "100px",
                        position: "absolute",
                        left: "50%",
                        top: "50%",
                        transform: "translate(-50%, 0)",
                      }}
                    >
                      0
                    </span>
                  </div>
                  <div
                    style={{
                      backgroundColor: "yellow",
                      height: "25vh",
                      margin: "10px",
                      width: "44%",
                      display: "inline-block",
                      position: "relative",
                    }}
                  >
                    <span
                      style={{
                        fontSize: "100px",
                        position: "absolute",
                        left: "50%",
                        top: "50%",
                        transform: "translate(-50%, 0)",
                      }}
                    >
                      0
                    </span>
                  </div>
                </div>
              </Col>
              <Col md={4}>
                <h3 style={{ textAlign: "center" }}>Seconds</h3>
                <div>
                  <div
                    style={{
                      backgroundColor: "yellow",
                      height: "25vh",
                      margin: "10px",
                      width: "44%",
                      display: "inline-block",
                      position: "relative",
                    }}
                  >
                    <span
                      style={{
                        fontSize: "100px",
                        position: "absolute",
                        left: "50%",
                        top: "50%",
                        transform: "translate(-50%, 0)",
                      }}
                    >
                      0
                    </span>
                  </div>
                  <div
                    style={{
                      backgroundColor: "yellow",
                      height: "25vh",
                      margin: "10px",
                      width: "44%",
                      display: "inline-block",
                      position: "relative",
                    }}
                  >
                    <span
                      style={{
                        fontSize: "100px",
                        position: "absolute",
                        left: "50%",
                        top: "50%",
                        transform: "translate(-50%, 0)",
                      }}
                    >
                      0
                    </span>
                  </div>
                </div>
              </Col>
            </Row>
          </div>
        </Fragment>
      );
    }
  }
}

export default TimeCountdown;
