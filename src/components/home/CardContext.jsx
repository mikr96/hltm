import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import { Image } from "react-bootstrap";
import { useNavigate } from "react-router-dom";

class CardContext extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Fragment>
        <div className="cardDashboard" style={{ textAlign: "center" }}>
          <div className="img-wrapper">
            <div className="img-overlay"></div>;
            <Image
              style={{ width: "100%" }}
              src={this.props.image}
              onClick={() => {
                this.props.navigate(`${this.props.url}`, { test: "ayam" });
              }}
            />
          </div>
        </div>
      </Fragment>
    );
  }
}

export default navigateHook(CardContext);

function navigateHook(CardContext) {
  return function WrappedComponent(props) {
    const navigate = useNavigate();
    return <CardContext {...props} navigate={navigate} />;
  };
}
