import React, { Component, Fragment } from "react";
import { Carousel, Image } from "react-bootstrap";
import poster from "../../assets/images/800x400.jpg";

class ItemSlider extends Component {
  render() {
    return (
      <div style={{ background: "white" }}>
        <Carousel
          className="p-0 d-flex justify-content-center"
          style={{
            height: "100vh",
            overflowY: "auto",
          }}
        >
          <Carousel.Item
            style={{
              top: "50%",
              position: "absolute",
              width: "100%",
              transform: "translateY(-50%)",
            }}
          >
            <Image
              className="d-block w-100"
              src="https://training.holisticslab.my/wp-content/uploads/2021/01/2021-Slider-training-scaled.jpg"
              alt="First slide"
            />
          </Carousel.Item>
          <Carousel.Item
            style={{
              top: "50%",
              position: "absolute",
              width: "100%",
              transform: "translateY(-50%)",
            }}
          >
            <Image
              className="d-block w-100"
              src="https://training.holisticslab.my/wp-content/uploads/2019/05/OnPaste.20181205-143625.png"
              alt="Second slide"
            />
          </Carousel.Item>
          <Carousel.Item
            style={{
              top: "50%",
              position: "absolute",
              width: "100%",
              transform: "translateY(-50%)",
            }}
          >
            <Image
              className="d-block w-100"
              src="https://training.holisticslab.my/wp-content/uploads/2019/05/OnPaste.20181205-143625.png"
              alt="Third slide"
            />
          </Carousel.Item>
        </Carousel>
      </div>
    );
  }
}

export default ItemSlider;
