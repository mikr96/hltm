import React, { Component, Fragment } from "react";
import { Container, Row, Col, Button, Image } from "react-bootstrap";
import { Link } from "react-router-dom";
import Logo from "../../assets/images/colorLogo.png";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import MegaMenuMobile from "../Home/MegaMenuMobile";

class HeaderMobile extends Component {
  constructor() {
    super();
    this.state = {
      SideNavState: "sideNavClose",
      ContentOverState: "ContentOverlayClose",
    };
  }
  MenuBarClickHandler = () => {
    this.SideNavOpenClose();
  };

  ContentOverlayClickHandler = () => {
    this.SideNavOpenClose();
  };

  SideNavOpenClose = () => {
    let SideNavState = this.state.SideNavState;
    if (SideNavState === "sideNavOpen") {
      this.setState({
        SideNavState: "sideNavClose",
        ContentOverState: "ContentOverlayClose",
      });
    } else {
      this.setState({
        SideNavState: "sideNavOpen",
        ContentOverState: "ContentOverlayOpen",
      });
    }
  };
  render() {
    return (
      <Fragment>
        <div className="TopSectionDown">
          <Container
            fluid={"true"}
            className="fixed-top shadow-sm p-2 mb-0 bg-white"
          >
            <Row style={{ alignItems: "center" }}>
              <Col lg={4} md={4} sm={12} xs={12}>
                <Button
                  variant="secondary"
                  onClick={this.MenuBarClickHandler}
                  className="btn"
                >
                  <FontAwesomeIcon
                    icon={["fa", "bars"]}
                    color="white"
                    size="lg"
                  />
                </Button>

                <Link to="/">
                  <Image className="w-40" src={Logo} />{" "}
                </Link>

                <Button
                  className="cart-btn"
                  variant="secondary"
                  style={{ float: "right", marginTop: "4px" }}
                >
                  SIGN IN
                </Button>
              </Col>
            </Row>
          </Container>

          <div className={this.state.SideNavState}>
            <MegaMenuMobile />
          </div>

          <div
            onClick={this.ContentOverlayClickHandler}
            className={this.state.ContentOverState}
          ></div>
        </div>
      </Fragment>
    );
  }
}

export default HeaderMobile;
