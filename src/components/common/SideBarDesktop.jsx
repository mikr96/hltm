import React, { Component } from "react";
import {
  Input,
  Menu,
  Segment,
  Dropdown,
  Checkbox,
  Grid,
  Header,
  Icon,
  Image,
  Sidebar,
  Dimmer,
  Loader,
  Table,
} from "semantic-ui-react";
import "../../assets/css/dashboard.css";

class SideBarDesktop extends Component {
  render() {
    return (
      <div className="sideBar" id="sidebar">
        <Menu pointing secondary vertical>
          <Menu.Item header icon="home" name="Dashboard" to="/" />
          <div className="ui divider"></div>
          <Menu.Item header>Setting</Menu.Item>
          <Menu.Item icon="user" name="Profile" to="/" />
          <Menu.Item icon="id card" name="Administrator" to="/" />
          <div className="ui divider"></div>
          <Menu.Item header>Courses</Menu.Item>
          <Menu.Item icon="archive" name="Course List" to="/" />
          <Menu.Item icon="question circle" name="Course Info" to="/" />
          <div className="ui divider"></div>
          <Menu.Item header>Cohort</Menu.Item>
          <Menu.Item icon="calendar" name="Cohort List" to="/" />
          <Menu.Item icon="question circle" name="Cohort Info" to="/" />
          <Menu.Item icon="user" name="Trainee List" to="/" />
          <div className="ui divider"></div>
          <Menu.Item header>Bill & Invoice</Menu.Item>
          <Menu.Item icon="setting" name="Invoice Setting" to="/" />
          <Menu.Item
            icon="money bill alternate"
            name="Biller Informations"
            to="/"
          />
        </Menu>
      </div>
    );
  }
}

export default SideBarDesktop;
