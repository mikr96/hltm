import React, { Component, Fragment } from "react";
import { InputGroup, Image, FormControl, Button } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import logo from "../../assets/images/whiteLogo.png";

class FooterDesktop extends Component {
  constructor(props) {
    super(props);
    this.state = {
      role: this.props.role,
    };
  }
  render() {
    if (this.state.role == "home") {
      return (
        <Fragment>
          <div className="container py-5 bg-darkGreen">
            <div className="row py-4">
              <div className="col-lg-4 col-md-6 mb-4 mb-lg-0">
                <div className="plr-20">
                  <Image src={logo} className="w-40" />
                  <p className="font-italic pt-10">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                    sed do eiusmod tempor incididunt.
                  </p>
                  <ul className="list-inline mt-4">
                    <li className="list-inline-item">
                      <a href="#" target="_blank" title="twitter">
                        <FontAwesomeIcon
                          icon={["fab", "twitter"]}
                          color="white"
                          size="lg"
                        />
                      </a>
                    </li>
                    <li className="list-inline-item ml-5">
                      <a href="#" target="_blank" title="facebook">
                        <FontAwesomeIcon
                          icon={["fab", "facebook"]}
                          color="white"
                          size="lg"
                        />
                      </a>
                    </li>
                    <li className="list-inline-item ml-5">
                      <a href="#" target="_blank" title="instagram">
                        <FontAwesomeIcon
                          icon={["fab", "instagram"]}
                          color="white"
                          size="lg"
                        />
                      </a>
                    </li>
                    <li className="list-inline-item ml-5">
                      <a href="#" target="_blank" title="pinterest">
                        <FontAwesomeIcon
                          icon={["fab", "pinterest"]}
                          color="white"
                          size="lg"
                        />
                      </a>
                    </li>
                    <li className="list-inline-item ml-5">
                      <a href="#" target="_blank" title="vimeo">
                        <FontAwesomeIcon
                          icon={["fab", "vimeo"]}
                          color="white"
                          size="lg"
                        />
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="col-lg-1 col-md-6 mb-lg-0"></div>
              <div className="col-lg-3 col-md-6 mb-4 mb-lg-0">
                <h2 className="text-uppercase font-weight-bold mb-4">
                  Company
                </h2>
                <ul className="list-unstyled mb-0">
                  <li className="mb-2">
                    <a href="#" className="color-white">
                      Login
                    </a>
                  </li>
                  <li className="mb-2">
                    <a href="#" className="color-white">
                      Register
                    </a>
                  </li>
                  <li className="mb-2">
                    <a href="#" className="color-white">
                      Wishlist
                    </a>
                  </li>
                  <li className="mb-2">
                    <a href="#" className="color-white">
                      Our Products
                    </a>
                  </li>
                </ul>
              </div>
              <div className="col-lg-4 col-md-6 mb-lg-0">
                <div className="plr-20">
                  <h2 className="text-uppercase font-weight-bold mb-4">
                    Newsletter
                  </h2>
                  <p className=" mb-4">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At
                    itaque temporibus.
                  </p>
                  <div className="input-group">
                    <InputGroup className="mb-3">
                      <FormControl
                        placeholder="Enter your email address"
                        aria-label="Recipient's username"
                        aria-describedby="button-addon1"
                        className="form-control border-0 shadow-0"
                      />
                      <Button variant="outline-secondary" id="button-addon2">
                        Button
                      </Button>
                    </InputGroup>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="bg-darkGreen py-4">
            <div className="container text-center">
              <p className=" mb-0 py-2">
                © 2021 Holistics Lab All rights reserved.
              </p>
            </div>
          </div>
        </Fragment>
      );
    } else {
      return (
        <Fragment>
          <div className="bg-darkBlue py-2 footer">
            <div className="container text-center">
              <p className=" mb-0 py-2">
                © 2021 Holistics Lab All rights reserved.
              </p>
            </div>
          </div>
        </Fragment>
      );
    }
  }
}

export default FooterDesktop;
