import React, { Component, Fragment } from "react";
import "./../../assets/css/homeHeader.css";
import logo2 from "../../assets/images/colorLogo.png";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  Image,
  Dropdown,
  DropdownButton,
  Navbar,
  Container,
  Nav,
  Row,
  Col,
  Button,
} from "react-bootstrap";

class HeaderDesktop extends Component {
  constructor(props) {
    super(props);
    this.state = {
      role: this.props.role,
    };
  }

  render() {
    if (this.state.role == "home") {
      return (
        <Fragment>
          <Row>
            <Navbar
              style={{
                color: "black !important",
                background: "white",
                width: "100%",
                marginRight: "10px",
                boxShadow: "0 0 10px 0 rgba(57, 78, 234, 0.1)",
              }}
            >
              <Col md={3}>
                <Navbar.Brand href="/">
                  <Image src={logo2} className="logo" />
                </Navbar.Brand>
              </Col>
              <Col md={2}></Col>
              <Col md={1} className="d-flex justify-content-center">
                <Nav.Item>
                  <Nav.Link
                    style={{
                      fontWeight: "500",
                      fontSize: "16px",
                    }}
                    href="/"
                  >
                    <p
                      className="header-home"
                      style={{ color: "black", display: "inline-block" }}
                    >
                      HOME
                    </p>
                  </Nav.Link>
                </Nav.Item>
              </Col>
              <Col md={1} className="d-flex justify-content-center">
                <Nav.Item>
                  <Nav.Link
                    style={{
                      fontWeight: "500",
                      fontSize: "16px",
                    }}
                    href="/course"
                  >
                    <p
                      className="header-home"
                      style={{ color: "black", display: "inline-block" }}
                    >
                      COURSE
                    </p>
                  </Nav.Link>
                </Nav.Item>
              </Col>
              <Col md={1} className="d-flex justify-content-center">
                <Nav.Item>
                  <Nav.Link
                    style={{
                      fontWeight: "500",
                      fontSize: "16px",
                    }}
                    href="/trainers"
                  >
                    <p
                      className="header-home"
                      style={{ color: "black", display: "inline-block" }}
                    >
                      TRAINERS
                    </p>
                  </Nav.Link>
                </Nav.Item>
              </Col>
              <Col md={1} className="d-flex justify-content-center">
                <Nav.Item>
                  <Nav.Link
                    style={{
                      fontWeight: "500",
                      fontSize: "16px",
                    }}
                    href="/alumni"
                  >
                    <p
                      className="header-home"
                      style={{ color: "black", display: "inline-block" }}
                    >
                      ALUMNI
                    </p>
                  </Nav.Link>
                </Nav.Item>
              </Col>
              <Col md={1} className="d-flex justify-content-center">
                <Nav.Item>
                  <Nav.Link
                    style={{
                      fontWeight: "500",
                      fontSize: "16px",
                    }}
                    href="faq"
                  >
                    <p
                      className="header-home"
                      style={{ color: "black", display: "inline-block" }}
                    >
                      FAQ
                    </p>
                  </Nav.Link>
                </Nav.Item>
              </Col>
              <Col md={1} className="d-flex justify-content-center">
                <Nav.Item>
                  <Nav.Link
                    style={{
                      fontWeight: "500",
                      fontSize: "16px",
                    }}
                    href="/contact"
                  >
                    <p
                      className="header-home"
                      style={{ color: "black", display: "inline-block" }}
                    >
                      CONTACT
                    </p>
                  </Nav.Link>
                </Nav.Item>
              </Col>
              <Col
                md={1}
                style={{
                  fontWeight: "500",
                  fontSize: "16px",
                }}
                className="d-flex justify-content-center"
              >
                <Link to="login" className="color-white">
                  <Nav.Item className="button-header bg-darkGreen p-3">
                    LOG IN
                  </Nav.Item>
                </Link>
              </Col>
            </Navbar>
          </Row>
        </Fragment>
      );
    } else {
      return (
        <Fragment>
          <Navbar
            fixed="top"
            bg="white"
            style={{
              paddingRight: "30px",
              paddingLeft: "30px",
              boxShadow: "0 0 6px 2px rgba(0, 0, 0, 0.1)",
              borderBottom: "2px solid transparent",
            }}
            id="header"
          >
            <Navbar.Brand>
              <FontAwesomeIcon
                icon={["fas", "align-justify"]}
                color="black"
                size="lg"
                onClick={() => this.props.onChangeStatus()}
              />
              <Image src={logo2} className="logo" style={{ width: "20%" }} />
            </Navbar.Brand>
            <Navbar.Toggle />
            <Navbar.Collapse
              className="justify-content-end"
              style={{ width: "100%" }}
            >
              <Navbar.Text>
                Signed in as: &nbsp;
                <p
                  style={{
                    color: "black",
                    display: "inline-block",
                    textTransform: "capitalize",
                  }}
                >
                  {this.props.role}
                </p>
              </Navbar.Text>
              <FontAwesomeIcon
                icon={["fas", "user"]}
                color="black"
                size="lg"
                onClick={() => this.props.onChangeStatus()}
                style={{ margin: "10px" }}
              />
            </Navbar.Collapse>
          </Navbar>
        </Fragment>
      );
    }
  }
}

export default HeaderDesktop;
