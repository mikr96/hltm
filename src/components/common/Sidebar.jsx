import React, { Component, Fragment } from "react";
import { Navigate } from "react-router";
import { ListGroup, Image, Row } from "react-bootstrap";
import { Link } from "react-router-dom";

class Sidebar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      authenticate: true,
    };
  }

  render = () => {
    console.log({ status: this.props.status });
    if (!localStorage.getItem("token") && !this.state.authenticate) {
      return <Navigate to={"/login"} />;
    }
    return (
      <div
        id="sidebar"
        className={this.props.status ? "no-sidebar" : "sidebarMenu"}
        style={{ textAlign: "center" }}
      >
        <br />
        <br />
        <p className="titleSubNav" style={{ color: "white" }}>
          TRAINING MANAGEMENT SYSTEM
        </p>
        <br />
        <br />
        {this.props.role == "admin" && (
          <ListGroup
            variant="flush"
            style={{
              justifyContent: "center",
              textAlign: "center",
              border: "0px",
            }}
          >
            <ListGroup.Item
              className="d-flex justify-content-center"
              style={{
                backgroundColor: "#0f1a26",
                color: "#fff",
              }}
            >
              <Link
                to=""
                style={{
                  color: "#fff",
                  display: "flex",
                }}
              >
                <div className="subHeader">
                  <p className="titleSubNav">HOME</p>
                </div>
              </Link>
            </ListGroup.Item>
            <ListGroup.Item
              className="d-flex justify-content-center"
              style={{
                backgroundColor: "#0f1a26",
                color: "#fff",
              }}
            >
              <Link
                to="course"
                style={{
                  color: "#fff",
                  display: "flex",
                }}
              >
                <div className="subHeader">
                  <p className="titleSubNav">COURSE</p>
                </div>
              </Link>
            </ListGroup.Item>
            <ListGroup.Item
              className="d-flex justify-content-center"
              style={{
                backgroundColor: "#0f1a26",
                color: "#fff",
              }}
            >
              <Link
                to="cohort"
                style={{
                  color: "#fff",
                  display: "flex",
                }}
              >
                <div className="subHeader">
                  <p className="titleSubNav">COHORT</p>
                </div>
              </Link>
            </ListGroup.Item>
            <ListGroup.Item
              className="d-flex justify-content-center"
              style={{
                backgroundColor: "#0f1a26",
                color: "#fff",
              }}
            >
              <Link
                to="invoice"
                style={{
                  color: "#fff",
                  display: "flex",
                }}
              >
                <div className="subHeader">
                  <p className="titleSubNav">INVOICE</p>
                </div>
              </Link>
            </ListGroup.Item>
            <ListGroup.Item
              className="d-flex justify-content-center"
              style={{
                backgroundColor: "#0f1a26",
                color: "#fff",
              }}
            >
              <Link
                to="company"
                style={{
                  color: "#fff",
                  display: "flex",
                }}
              >
                <div className="subHeader">
                  <p className="titleSubNav">COMPANY</p>
                </div>
              </Link>
            </ListGroup.Item>
            <ListGroup.Item
              className="d-flex justify-content-center"
              style={{
                backgroundColor: "#0f1a26",
                color: "#fff",
              }}
            >
              <Link
                to="setting"
                style={{
                  color: "#fff",
                  display: "flex",
                }}
              >
                <div className="subHeader">
                  <p className="titleSubNav">SETTING</p>
                </div>
              </Link>
            </ListGroup.Item>
            <ListGroup.Item
              className="d-flex justify-content-center"
              style={{
                backgroundColor: "#0f1a26",
                color: "#fff",
              }}
              action
              onClick={() => {
                localStorage.clear();
                this.setState({ authenticate: false });
              }}
            >
              <div className="subHeader">
                <p className="titleSubNav">LOGOUT</p>
              </div>
            </ListGroup.Item>
          </ListGroup>
        )}
        {this.props.role !== "admin" && (
          <ListGroup
            variant="flush"
            style={{
              justifyContent: "center",
              textAlign: "center",
              border: "0px",
            }}
          >
            <ListGroup.Item
              className="d-flex justify-content-center"
              style={{
                backgroundColor: "#0f1a26",
                color: "#fff",
              }}
            >
              <Link
                to=""
                style={{
                  color: "#fff",
                  display: "flex",
                }}
              >
                <div className="subHeader">
                  <p className="titleSubNav">HOME</p>
                </div>
              </Link>
            </ListGroup.Item>
            <ListGroup.Item
              className="d-flex justify-content-center"
              style={{
                backgroundColor: "#0f1a26",
                color: "#fff",
              }}
            >
              <Link
                to="clientprofile"
                style={{
                  color: "#fff",
                  display: "flex",
                }}
              >
                <div className="subHeader">
                  <p className="titleSubNav">PROFILE</p>
                </div>
              </Link>
            </ListGroup.Item>
            <ListGroup.Item
              className="d-flex justify-content-center"
              style={{
                backgroundColor: "#0f1a26",
                color: "#fff",
              }}
            >
              <Link
                to="clientcourse"
                style={{
                  color: "#fff",
                  display: "flex",
                }}
              >
                <div className="subHeader">
                  <p className="titleSubNav">COURSE</p>
                </div>
              </Link>
            </ListGroup.Item>
            <ListGroup.Item
              className="d-flex justify-content-center"
              style={{
                backgroundColor: "#0f1a26",
                color: "#fff",
              }}
            >
              <Link
                to="clientattendance"
                style={{
                  color: "#fff",
                  display: "flex",
                }}
              >
                <div className="subHeader">
                  <p className="titleSubNav">ATTENDANCE</p>
                </div>
              </Link>
            </ListGroup.Item>
            <ListGroup.Item
              className="d-flex justify-content-center"
              style={{
                backgroundColor: "#0f1a26",
                color: "#fff",
              }}
            >
              <Link
                to="clientinvoice"
                style={{
                  color: "#fff",
                  display: "flex",
                }}
              >
                <div className="subHeader">
                  <p className="titleSubNav">INVOICE</p>
                </div>
              </Link>
            </ListGroup.Item>
            <ListGroup.Item
              className="d-flex justify-content-center"
              style={{
                backgroundColor: "#0f1a26",
                color: "#fff",
              }}
              action
              onClick={() => {
                localStorage.clear();
                this.setState({ authenticate: false });
              }}
            >
              <div className="subHeader">
                <p className="titleSubNav">LOGOUT</p>
              </div>
            </ListGroup.Item>
          </ListGroup>
        )}
        ;
      </div>
    );
  };
}

export default Sidebar;
