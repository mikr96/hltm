import React, { Component, Fragment } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Table, Modal, Form, Button, Pagination } from "react-bootstrap";
import APIurl from "../../api/AppURL";
import axios from "axios";

class TableContext extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        company_name: this.props.dataModal.company_name,
        company_address: this.props.dataModal.company_address,
        company_branch: this.props.dataModal.company_branch,
        company_register_no: this.props.dataModal.company_register_no,
        company_type: this.props.dataModal.company_type,
        company_details: this.props.dataModal.company_details,
      },
      items: [],
    };
    this.handleInput = this.handleInput.bind(this);
    this.viewAddress = this.viewAddress.bind(this);
  }

  componentDidMount = () => {
    let active = 1;
    let items = [];
    for (let number = 1; number <= 5; number++) {
      items.push(
        <Pagination.Item key={number} active={number === active}>
          {number}
        </Pagination.Item>
      );
    }
    this.setState({ items: items });
  };

  handleInput = (e) => {
    this.setState({
      data: {
        ...this.state.data,
        [e.target.name]: e.target.value,
      },
    });
  };

  viewAddress = (array) => {
    console.log(array.length);
    if (array.length > 3019) {
      const address = JSON.parse(array).join("\n");
      return address;
    } else {
      return array;
    }
  };

  render = () => {
    let element,
      row = [];
    let length = Object.keys(this.props.data).length;
    if (this.props.data.id == 0) {
      length = 1;
    }
    for (let i = 0; i < length; i++) {
      element = (
        <tr data-index={i}>
          <td>{i + 1}</td>
          <td>{this.props.data[i].company_name}</td>
          <td>{this.viewAddress(this.props.data[i].company_address)}</td>
          <td>{this.props.data[i].company_branch}</td>
          <td>{this.props.data[i].company_register_no}</td>
          <td>{this.props.data[i].company_type}</td>
          <td>{this.props.data[i].company_details}</td>
          <td>
            {" "}
            <FontAwesomeIcon
              icon={["fas", "pen"]}
              color="blue"
              size="lg"
              onClick={(e) => {
                this.props.getCompanybyID(e, this.props.data[i].id);
              }}
            />
            <FontAwesomeIcon
              icon={["fas", "trash"]}
              color="red"
              size="lg"
              onClick={(e) => {
                this.props.deleteCompany(e, this.props.data[i].id);
              }}
            />
          </td>
        </tr>
      );
      row.push(element);
    }
    return (
      <Fragment>
        <br />
        <Table
          hover
          responsive
          size="lg"
          style={{
            textAlign: "center",
            boxShadow: "0 0 10px 0 rgba(0, 0, 0, 0.2)",
          }}
        >
          <thead style={{ background: "#468C32", color: "white" }}>
            <tr>
              {this.props.header.map((com) => (
                <th>{com}</th>
              ))}
            </tr>
          </thead>
          <tbody style={{ border: "0px white", background: "white" }}>
            {row.map((e) => e)}
          </tbody>
        </Table>
        <Pagination style={{ justifyContent: "center" }}>
          <Pagination.First />
          <Pagination.Prev />
          {this.state.items}
          <Pagination.Next />
          <Pagination.Last />
        </Pagination>
        <Modal show={this.props.showModal} onHide={!this.props.showModal}>
          <Form
            method="PUT"
            onSubmit={(event) => {
              this.props.updateCompany(event, {
                company_name: event.target.elements.company_name.value,
                company_address: event.target.elements.company_address.value,
                company_branch: event.target.elements.company_branch.value,
                company_register_no:
                  event.target.elements.company_register_no.value,
                company_type: event.target.elements.company_type.value,
                company_details: event.target.elements.company_details.value,
              });
            }}
          >
            <Modal.Header closeButton>
              <Modal.Title>EDIT COMPANY</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form.Group className="mb-3" onChange={this.handleInput}>
                <Form.Label>COMPANY NAME</Form.Label>
                <Form.Control
                  type="string"
                  name="company_name"
                  defaultValue={this.props.dataModal.company_name}
                />
              </Form.Group>
              <Form.Group className="mb-3" onChange={this.handleInput}>
                <Form.Label>COMPANY ADDRESS</Form.Label>
                <Form.Control
                  type="string"
                  name="company_address"
                  defaultValue={this.props.dataModal.company_address}
                />
              </Form.Group>
              <Form.Group className="mb-3" onChange={this.handleInput}>
                <Form.Label>COMPANY BRANCH</Form.Label>

                <Form.Control
                  type="string"
                  name="company_branch"
                  defaultValue={this.props.dataModal.company_branch}
                />
              </Form.Group>
              <Form.Group className="mb-3" onChange={this.handleInput}>
                <Form.Label>COMPANY REGISTERED NUMBER</Form.Label>
                <Form.Control
                  type="string"
                  name="company_register_no"
                  defaultValue={this.props.dataModal.company_register_no}
                />
              </Form.Group>
              <Form.Group className="mb-3" onChange={this.handleInput}>
                <Form.Label>COMPANY TYPE</Form.Label>
                <Form.Control
                  as="textarea"
                  rows={3}
                  name="company_type"
                  defaultValue={this.props.dataModal.company_type}
                />
              </Form.Group>
              <Form.Group className="mb-3" onChange={this.handleInput}>
                <Form.Label>COMPANY DETAILS</Form.Label>
                <Form.Control
                  type="string"
                  name="company_details"
                  defaultValue={this.props.dataModal.company_details}
                />
              </Form.Group>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="primary" onClick={this.props.closeModal}>
                Close
              </Button>
              <Button variant="primary" type="submit">
                Update
              </Button>
            </Modal.Footer>
          </Form>
        </Modal>
      </Fragment>
    );
  };
}

export default TableContext;
