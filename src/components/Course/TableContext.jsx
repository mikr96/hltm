import React, { Component, Fragment } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Table, Modal, Form, Button } from "react-bootstrap";
import { Navigate } from "react-router";
import APIurl from "../../api/AppURL";
import APIFunction from "../../functions/APIFunction";
import CRUDFunction from "../../functions/CRUDFunction";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

const MySwal = withReactContent(Swal);
class TableContext extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        course_name: this.props.dataModal.course_name,
        course_desc: this.props.dataModal.course_desc,
        course_fee: this.props.dataModal.course_fee,
        course_link: this.props.dataModal.course_link,
        max_student: this.props.dataModal.max_student,
        course_image: this.props.dataModal.course_image,
      },
      register: false,
      success: false,
    };
    this.handleInput = this.handleInput.bind(this);
    this.registerStaff = this.registerStaff.bind(this);
  }

  handleInput = (e) => {
    this.setState({
      data: {
        ...this.state.data,
        [e.target.name]: e.target.value,
      },
    });
  };

  registerStaff = async (event) => {
    console.log("test");
    event.preventDefault();
    const response = await CRUDFunction.get(
      `${APIurl.mail}`,
      APIFunction.getHeader()[0]
    );
    if (response) {
      console.log(response);
      MySwal.fire({
        title: <p>{response.message}</p>,
        footer: "Copyright 2022",
        onOpen: () => {
          // MySwal` is a subclass of `Swal`
          //   with all the same instance & static methods
          MySwal.clickConfirm();
        },
      }).then(() => {
        this.setState({ success: true });
      });
    }
  };

  render = () => {
    /// After Login Navigate to Profile Page
    if (this.state.success) {
      return <Navigate to={"/layoutDashboard/company"} />;
    }
    return (
      <Fragment>
        <br />
        {!this.state.register && (
          <Table
            hover
            responsive
            size="lg"
            style={{
              textAlign: "center",
              boxShadow: "0 0 10px 0 rgba(0, 0, 0, 0.2)",
            }}
          >
            <thead style={{ background: this.props.color, color: "white" }}>
              <tr>
                {this.props.header.map((res) => (
                  <th>{res}</th>
                ))}
              </tr>
            </thead>
            <tbody style={{ border: "0px white", background: "white" }}>
              {this.props.isDataExist &&
                this.props.role == "admin" &&
                this.props.courseData.map((iteration) => {
                  return iteration.map((elem, i) => (
                    <tr>
                      <td>{i + 1}</td>
                      <td>{elem.course_name}</td>
                      <td>{elem.course_desc}</td>
                      <td>{elem.course_fee}</td>
                      <td>{elem.course_image}</td>
                      <td>
                        {" "}
                        <FontAwesomeIcon
                          icon={["fas", "pen"]}
                          color="blue"
                          size="lg"
                          onClick={(e) => {
                            this.props.getCoursebyID(e, elem.id);
                          }}
                        />
                        <FontAwesomeIcon
                          icon={["fas", "trash"]}
                          color="red"
                          size="lg"
                          onClick={(e) => {
                            this.props.deleteCourse(e, elem.id);
                          }}
                        />
                      </td>
                    </tr>
                  ));
                })}
              {this.props.isDataExist &&
                this.props.role == "client" &&
                this.props.courseData.map((iteration) => {
                  return iteration.map((elem, i) => (
                    <tr>
                      <td>{i + 1}</td>
                      <td>{elem.course_name}</td>
                      <td>
                        {/* <span>
                          Cohort Includes :{" "}
                          <strong>{elem.cohorts[0].cohort_include}</strong>{" "}
                        </span>
                        <br />
                        <span>
                          Cohort Includes :{" "}
                          <strong> {elem.cohorts[0].cohort_include}</strong>{" "}
                        </span> */}
                      </td>
                      <td>
                        <a onClick={() => this.setState({ register: true })}>
                          Register
                        </a>
                      </td>
                    </tr>
                  ));
                })}
              {!this.props.isDataExist && this.props.role == "admin" && (
                <tr>
                  <td>None</td>
                  <td>None</td>
                  <td>None</td>
                  <td>None</td>
                  <td>None</td>
                  <td>None</td>
                </tr>
              )}
              {!this.props.isDataExist && this.props.role == "client" && (
                <tr>
                  <td>None</td>
                  <td>None</td>
                  <td>None</td>
                  <td>None</td>
                </tr>
              )}
            </tbody>
          </Table>
        )}
        <Modal show={this.props.showModal} onHide={!this.props.showModal}>
          <Form
            method="PUT"
            onSubmit={(event) => {
              console.log("e", event.target.elements.course_name.value);
              this.props.updateCourse(event, {
                course_name: event.target.elements.course_name.value,
                course_desc: event.target.elements.course_desc.value,
                course_fee: event.target.elements.course_fee.value,
                course_link: event.target.elements.course_link.value,
                max_student: event.target.elements.max_student.value,
                course_image: event.target.elements.course_image.value,
              });
            }}
          >
            <Modal.Header closeButton>
              <Modal.Title>EDIT COURSE</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form.Group className="mb-3" onChange={this.handleInput}>
                <Form.Label>COURSE NAME</Form.Label>
                <Form.Control
                  type="string"
                  name="course_name"
                  defaultValue={this.props.dataModal.course_name}
                />
              </Form.Group>
              <Form.Group className="mb-3" onChange={this.handleInput}>
                <Form.Label>COURSE FEE</Form.Label>

                <Form.Control
                  type="string"
                  name="course_fee"
                  defaultValue={this.props.dataModal.course_fee}
                />
              </Form.Group>
              <Form.Group className="mb-3" onChange={this.handleInput}>
                <Form.Label>COURSE LINK</Form.Label>
                <Form.Control
                  type="string"
                  name="course_link"
                  defaultValue={this.props.dataModal.course_link}
                />
              </Form.Group>
              <Form.Group className="mb-3" onChange={this.handleInput}>
                <Form.Label>COURSE DESCRIPTION</Form.Label>
                <Form.Control
                  as="textarea"
                  rows={3}
                  name="course_desc"
                  defaultValue={this.props.dataModal.course_desc}
                />
              </Form.Group>
              <Form.Group className="mb-3" onChange={this.handleInput}>
                <Form.Label>MAX STUDENT</Form.Label>
                <Form.Control
                  type="string"
                  name="max_student"
                  defaultValue={this.props.dataModal.max_student}
                />
              </Form.Group>
              <Form.Group className="mb-3" onChange={this.handleInput}>
                <Form.Label>COURSE IMAGE</Form.Label>
                <Form.Control
                  type="string"
                  name="course_image"
                  defaultValue={this.props.dataModal.course_image}
                />
              </Form.Group>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="primary" onClick={this.props.closeModal}>
                Close
              </Button>
              <Button variant="primary" type="submit">
                Update
              </Button>
            </Modal.Footer>
          </Form>
        </Modal>
        {this.state.register == true && (
          <Form onSubmit={this.registerStaff}>
            <Table
              hover
              responsive
              size="lg"
              style={{
                textAlign: "center",
                boxShadow: "0 0 10px 0 rgba(0, 0, 0, 0.2)",
              }}
            >
              <thead style={{ background: this.props.color, color: "white" }}>
                <tr>
                  <th>NO</th>
                  <th>STAFF NAME</th>
                  <th>ACTION</th>
                </tr>
              </thead>
              <tbody style={{ border: "0px white", background: "white" }}>
                <tr>
                  <td>1</td>
                  <td>Athari Ayie</td>
                  <td>
                    <Form.Check type="checkbox" id="default-checkbox" />
                  </td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>Syafaat</td>
                  <td>
                    <Form.Check type="checkbox" id="default-checkbox" />
                  </td>
                </tr>
              </tbody>
            </Table>
            <Button
              style={{ display: "flex", float: "right" }}
              type="submit"
              className="primary"
            >
              Register
            </Button>
          </Form>
        )}
      </Fragment>
    );
  };
}

export default TableContext;
