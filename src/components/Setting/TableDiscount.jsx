import React, { Component, Fragment } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Table } from "react-bootstrap";
import { useNavigate } from "react-router-dom";

class TableDiscount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        cohort_id: this.props.dataModal.cohort_id,
        discount_value: this.props.dataModal.discount_value,
        discount_code: this.props.dataModal.discount_code,
        limit_discount: this.props.dataModal.limit_discount,
        discount_start_date: this.props.dataModal.discount_start_date,
        discount_end_date: this.props.dataModal.discount_end_date,
        discount_desc: this.props.dataModal.discount_desc,
      },
    };
    this.handleInput = this.handleInput.bind(this);
  }

  handleInput = (e) => {
    this.setState({
      data: {
        ...this.state.data,
        [e.target.name]: e.target.value,
      },
    });
  };

  getCourseName = (elem) => {
    let indexCourseName = this.props.courseIds.findIndex((x) => elem == x);
    return this.props.courseNames[indexCourseName];
  };

  render() {
    const isDataExist = this.props.isDataExist;
    if (isDataExist) {
      return (
        <Fragment>
          <br />
          <Table
            hover
            responsive
            size="lg"
            style={{
              textAlign: "center",
              boxShadow: "0 0 10px 0 rgba(0, 0, 0, 0.2)",
            }}
          >
            <thead style={{ background: "#468C32", color: "white" }}>
              <tr>
                {this.props.header.map((disc) => (
                  <th>{disc}</th>
                ))}
              </tr>
            </thead>
            <tbody style={{ border: "0px white", background: "white" }}>
              {this.props.stateData[0].length > 0 &&
                this.props.stateData.map((iteration) => {
                  return iteration.map((disc, i) => (
                    <tr>
                      <td>{i + 1}</td>
                      <td>{this.getCourseName(disc.course_id)}</td>
                      <td>{disc.discount_value}</td>
                      <td>{disc.discount_code}</td>
                      <td>{disc.limit_discount}</td>
                      <td>{disc.discount_start_date}</td>
                      <td>{disc.discount_end_date}</td>
                      <td>{disc.discount_desc}</td>
                      <td>
                        <FontAwesomeIcon
                          icon={["fas", "trash"]}
                          color="red"
                          size="lg"
                          onClick={(e) => {
                            this.props.deleteDiscount(e, disc.id);
                          }}
                        />
                      </td>
                    </tr>
                  ));
                })}
              {this.props.stateData[0].length <= 0 && (
                <tr>
                  <td>None</td>
                  <td>None</td>
                  <td>None</td>
                  <td>None</td>
                  <td>None</td>
                  <td>None</td>
                  <td>None</td>
                  <td>None</td>
                  <td>None</td>
                </tr>
              )}
            </tbody>
          </Table>
        </Fragment>
      );
    } else {
      return (
        <Fragment>
          <br />
          <Table hover responsive size="lg" style={{ textAlign: "center" }}>
            <thead style={{ background: "#468C32", color: "white" }}>
              <tr>
                {this.props.header.map((disc) => (
                  <th>{disc}</th>
                ))}
              </tr>
            </thead>
            <tbody style={{ border: "0px white", background: "white" }}>
              <tr>
                <td>None</td>
                <td>None</td>
                <td>None</td>
                <td>None</td>
                <td>None</td>
                <td>None</td>
                <td>None</td>
                <td>None</td>
                <td>None</td>
              </tr>
            </tbody>
          </Table>
        </Fragment>
      );
    }
  }
}

export default navigateHook(TableDiscount);

function navigateHook(TableDiscount) {
  return function WrappedComponent(props) {
    const navigate = useNavigate();
    return <TableDiscount {...props} navigate={navigate} />;
  };
}
