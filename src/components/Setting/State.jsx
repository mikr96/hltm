import React, { Component, Fragment } from "react";
import TableState from "../Setting/TableState";
import { Row, Col, Button, Modal, Dropdown, Form } from "react-bootstrap";
import APIFunction from "../../functions/APIFunction";
import CRUDFunction from "../../functions/CRUDFunction";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import APIurl from "../../api/AppURL";
import Swal from "sweetalert2";

class State extends Component {
  constructor() {
    super();
    this.state = {
      result: "",
      state: false,
      newdata: [],
      show: false,
      header: ["NO", "STATE CODE", "STATE NAME", "ACTION"],
      dataModal: [],
      id: "",
      isDataExist: false,
    };
    this.addState = this.addState.bind(this);
    this.deleteState = this.deleteState.bind(this);
    this.getStatebyID = this.getStatebyID.bind(this);
  }

  getStatebyID = async (e, id) => {
    const dataModal = await CRUDFunction.get(
      `${APIurl.state}/${id}`,
      APIFunction.getHeader()[0]
    );
    console.log("datamodal:", dataModal);
    if (dataModal) {
      this.setState({
        dataModal: dataModal,
        showModal: true,
        id: id,
      });
    }
  };

  addState = async (event) => {
    event.preventDefault();
    console.log("newData:", this.state.newdata);
    const newdata = await CRUDFunction.create(
      event,
      APIurl.state,
      this.state.newdata,
      APIFunction.getHeader()[0]
    );
    if (newdata) {
      console.log(newdata);
      this.setState({
        stateData: [newdata],
        show: false,
        state: false,
      });
    }
  };

  deleteState = async (event, id) => {
    event.preventDefault();
    const data = await CRUDFunction.delete(
      `${APIurl.state}/${id}`,
      APIFunction.getHeader()[0]
    );
    if (data) {
      Swal.fire({
        title: "Are you sure?",
        text: "You want delete your item?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!",
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire("Deleted!", "Your file has been deleted.", "success");
          this.setState({
            stateData: [data],
          });
        }
      });
    }
  };

  componentDidMount = async () => {
    const stateData = await CRUDFunction.get(
      `${APIurl.state}`,
      APIFunction.getHeader()[0]
    );
    if (stateData) {
      this.setState({
        isDataExist: true,
        stateData: [stateData.data],
      });
    }
  };

  closeModal = () => {
    this.setState({
      showModal: false,
    });
  };

  render() {
    return (
      <Fragment>
        <br />
        <Row>
          <div>
            <Row style={{ marginLeft: "0px", marginRight: "0px" }}>
              <Col md={6}>
                <h3 style={{ lineHeight: "2" }}>STATE LIST</h3>
              </Col>
              <Col md={6} style={{ paddingRight: "0px" }}>
                <div
                  style={{
                    width: "100%",
                    float: "right",
                    display: "flex",
                    justifyContent: "flex-end",
                    paddingRight: "0px",
                  }}
                >
                  <Button
                    onClick={() => {
                      this.setState({ show: true, state: true });
                    }}
                    className="primary"
                  >
                    <FontAwesomeIcon
                      icon={["fas", "plus"]}
                      color="white"
                      size="sm"
                    />
                    &nbsp; Add State
                  </Button>
                </div>
              </Col>
            </Row>
            <TableState
              header={this.state.header}
              stateData={this.state.stateData}
              showModal={this.state.showModal}
              dataModal={this.state.dataModal}
              isDataExist={this.state.isDataExist}
              getStatebyID={this.getStatebyID}
              closeModal={this.closeModal}
              deleteState={this.deleteState}
            ></TableState>
          </div>
        </Row>
        <br />
        {this.state.state && (
          <Modal
            show={this.state.show}
            onHide={() => {
              this.setState({
                show: false,
              });
            }}
          >
            <Modal.Header closeButton>
              <Modal.Title>ADD STATE</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form>
                <Form.Group
                  className="mb-3"
                  onChange={(e) => {
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        [e.target.name]: e.target.value,
                      },
                    });
                  }}
                >
                  <Form.Label>STATE CODE</Form.Label>
                  <Form.Control
                    type="number"
                    name="state_code"
                    placeholder="Enter state code"
                  />
                </Form.Group>

                <Form.Group
                  className="mb-3"
                  onChange={(e) => {
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        [e.target.name]: e.target.value,
                      },
                    });
                  }}
                >
                  <Form.Label>STATE NAME</Form.Label>
                  <Form.Control
                    type="string"
                    name="state_name"
                    placeholder="Enter state name"
                  />
                </Form.Group>
              </Form>
            </Modal.Body>

            <Modal.Footer>
              <Button
                variant="primary"
                onClick={() =>
                  this.setState({
                    show: false,
                    state: false,
                  })
                }
              >
                Close
              </Button>
              <Button
                variant="primary"
                onClick={(event) => this.addState(event)}
              >
                Add
              </Button>
            </Modal.Footer>
          </Modal>
        )}
      </Fragment>
    );
  }
}

export default State;
