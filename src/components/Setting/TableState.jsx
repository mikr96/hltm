import React, { Component, Fragment } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Table, Modal } from "react-bootstrap";
import { useNavigate } from "react-router-dom";

class TableState extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        state_code: this.props.dataModal.state_code,
        state_name: this.props.dataModal.state_name,
      },
    };
    this.handleInput = this.handleInput.bind(this);
  }

  handleInput = (e) => {
    console.log("propsmodal: ", this.state.data);
    this.setState({
      data: {
        ...this.state.data,
        [e.target.name]: e.target.value,
      },
    });
  };

  render() {
    const isDataExist = this.props.isDataExist;
    if (isDataExist) {
      return (
        <Fragment>
          <br />
          <Table
            hover
            responsive
            size="lg"
            style={{
              textAlign: "center",
              boxShadow: "0 0 10px 0 rgba(0, 0, 0, 0.2)",
            }}
          >
            <thead style={{ background: "#468C32", color: "white" }}>
              <tr>
                {this.props.header.map((stat) => (
                  <th>{stat}</th>
                ))}
              </tr>
            </thead>
            <tbody style={{ border: "0px white", background: "white" }}>
              {this.props.stateData.map((iteration) => {
                console.log("iteration:", iteration);
                return iteration.map((stat, i) => (
                  <tr>
                    <td>{i + 1}</td>
                    <td>{stat.state_code}</td>
                    <td>{stat.state_name}</td>

                    <td>
                      <FontAwesomeIcon
                        icon={["fas", "trash"]}
                        color="red"
                        size="lg"
                        onClick={(e) => {
                          this.props.deleteState(e, stat.id);
                        }}
                      />
                    </td>
                  </tr>
                ));
              })}
            </tbody>
          </Table>
          <Modal
            show={this.props.showModal}
            onHide={!this.props.showModal}
          ></Modal>
        </Fragment>
      );
    } else {
      return (
        <Fragment>
          <br />
          <Table hover responsive size="lg" style={{ textAlign: "center" }}>
            <thead style={{ background: "#468C32", color: "white" }}>
              <tr>
                {this.props.header.map((stat) => (
                  <th>{stat}</th>
                ))}
              </tr>
            </thead>
            <tbody style={{ border: "0px white", background: "white" }}>
              <tr></tr>
            </tbody>
          </Table>
        </Fragment>
      );
    }
  }
}

export default navigateHook(TableState);

function navigateHook(TableState) {
  return function WrappedComponent(props) {
    const navigate = useNavigate();
    return <TableState {...props} navigate={navigate} />;
  };
}
