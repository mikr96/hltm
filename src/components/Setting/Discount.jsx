import React, { Component, Fragment } from "react";
import { Row, Col, Button, Modal, Form } from "react-bootstrap";
import APIFunction from "../../functions/APIFunction";
import CRUDFunction from "../../functions/CRUDFunction";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import APIurl from "../../api/AppURL";
import Swal from "sweetalert2";
import TableDiscount from "../Setting/TableDiscount";

class Discount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      result: "",
      state: false,
      newdata: [],
      show: false,
      header: [
        "NO",
        "COHORT",
        "DISCOUNT",
        "CODE",
        "LIMIT",
        "START DATE",
        "END DATE",
        "DISCOUNT DESCRIPTION",
        "ACTION",
      ],
      dataModal: [],
      id: "",
      isDataExist: false,
    };
    this.addDiscount = this.addDiscount.bind(this);
    this.deleteDiscount = this.deleteDiscount.bind(this);
    this.getDiscountbyID = this.getDiscountbyID.bind(this);
  }

  getDiscountbyID = async (e, id) => {
    const dataModal = await CRUDFunction.get(
      `${APIurl.discount}/${id}`,
      APIFunction.getHeader()[0]
    );
    console.log("datamodal:", dataModal);
    if (dataModal) {
      this.setState({
        dataModal: dataModal,
        showModal: true,
        id: id,
      });
    }
  };

  addDiscount = async (event) => {
    event.preventDefault();
    console.log("newData:", this.state.newdata);
    const newdata = await CRUDFunction.create(
      event,
      APIurl.discount,
      this.state.newdata,
      APIFunction.getHeader()[0]
    );
    if (newdata) {
      console.log(newdata);
      this.setState({
        stateData: [newdata],
        show: false,
        state: false,
      });
    }
  };

  deleteDiscount = async (event, id) => {
    event.preventDefault();
    const data = await CRUDFunction.delete(
      `${APIurl.discount}/${id}`,
      APIFunction.getHeader()[0]
    );
    if (data) {
      Swal.fire({
        title: "Are you sure?",
        text: "You want delete your item?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!",
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire("Deleted!", "Your file has been deleted.", "success");
          this.setState({
            stateData: [data],
          });
        }
      });
    }
  };

  componentDidMount = async () => {
    const stateData = await CRUDFunction.get(
      `${APIurl.discount}`,
      APIFunction.getHeader()[0]
    );
    if (stateData) {
      this.setState({
        isDataExist: true,
        stateData: [stateData.data],
        courseName: this.props.courseDetail.map((e) => e.course_name),
        courseId: this.props.courseDetail.map((e) => e.id),
      });
    }
  };

  closeModal = () => {
    this.setState({
      showModal: false,
    });
  };

  render() {
    return (
      <Fragment>
        <br />
        <Row>
          <div>
            <Row style={{ marginLeft: "0px", marginRight: "0px" }}>
              <Col md={6}>
                <h3 style={{ lineHeight: "2" }}>DISCOUNT LIST</h3>
              </Col>
              <Col md={6} style={{ paddingRight: "0px" }}>
                <div
                  style={{
                    width: "100%",
                    float: "right",
                    display: "flex",
                    justifyContent: "flex-end",
                    paddingRight: "0px",
                  }}
                >
                  <Button
                    onClick={() => {
                      this.setState({ show: true, state: true });
                    }}
                    className="primary"
                  >
                    <FontAwesomeIcon
                      icon={["fas", "plus"]}
                      color="white"
                      size="sm"
                    />
                    &nbsp; Add Discount
                  </Button>
                </div>
              </Col>
            </Row>
            <TableDiscount
              header={this.state.header}
              stateData={this.state.stateData}
              showModal={this.state.showModal}
              dataModal={this.state.dataModal}
              isDataExist={this.state.isDataExist}
              getDiscountbyID={this.getDiscountbyID}
              closeModal={this.closeModal}
              deleteDiscount={this.deleteDiscount}
              courseNames={this.state.courseName}
              courseIds={this.state.courseId}
            ></TableDiscount>
          </div>
        </Row>
        <br />
        {this.state.state && (
          <Modal
            show={this.state.show}
            onHide={() => {
              this.setState({
                show: false,
              });
            }}
          >
            <Modal.Header closeButton>
              <Modal.Title>ADD DISCOUNT</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form>
                <Form.Group
                  className="mb-3"
                  onChange={(e) => {
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        [e.target.name]: parseInt(e.target.value),
                      },
                    });
                  }}
                >
                  <Form.Label>SELECT COURSE</Form.Label>
                  <Form.Select
                    name="course_id"
                    aria-label="Default select example"
                  >
                    <option>Open this select menu</option>
                    {this.props.courseDetail.map((elem) => (
                      <option value={elem.id}>{elem.course_name}</option>
                    ))}
                  </Form.Select>
                </Form.Group>
                <Form.Group
                  className="mb-3"
                  onChange={(e) => {
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        [e.target.name]: e.target.value,
                      },
                    });
                  }}
                >
                  <Form.Label>DISCOUNT</Form.Label>
                  <Form.Control
                    type="number"
                    name="discount_value"
                    placeholder="Enter discount value"
                  />
                </Form.Group>
                <Form.Group
                  className="mb-3"
                  onChange={(e) => {
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        [e.target.name]: e.target.value,
                      },
                    });
                  }}
                >
                  <Form.Label>CODE</Form.Label>
                  <Form.Control
                    type="string"
                    name="discount_code"
                    placeholder="Enter discount code"
                  />
                </Form.Group>
                <Form.Group
                  className="mb-3"
                  onChange={(e) => {
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        [e.target.name]: e.target.value,
                      },
                    });
                  }}
                >
                  <Form.Label>LIMIT</Form.Label>
                  <Form.Control
                    type="number"
                    name="limit_discount"
                    placeholder="Enter discount limit"
                  />
                </Form.Group>

                <Row>
                  <Col>
                    <Form.Group
                      inline
                      className="mb-3"
                      onChange={(e) => {
                        this.setState({
                          newdata: {
                            ...this.state.newdata,
                            [e.target.name]: e.target.value,
                          },
                        });
                      }}
                    >
                      <Form.Label>START DATE</Form.Label>
                      <Form.Control
                        type="date"
                        name="discount_start_date"
                        placeholder="Enter discount start date"
                      />
                    </Form.Group>
                  </Col>

                  <Col>
                    <Form.Group
                      className="mb-3"
                      onChange={(e) => {
                        this.setState({
                          newdata: {
                            ...this.state.newdata,
                            [e.target.name]: e.target.value,
                          },
                        });
                      }}
                    >
                      <Form.Label>END DATE</Form.Label>
                      <Form.Control
                        type="date"
                        name="discount_end_date"
                        placeholder="Enter discount start date"
                      />
                    </Form.Group>
                  </Col>
                </Row>
                <Form.Group
                  className="mb-3"
                  onChange={(e) => {
                    this.setState({
                      newdata: {
                        ...this.state.newdata,
                        [e.target.name]: e.target.value,
                      },
                    });
                  }}
                >
                  <Form.Label>DISCOUNT DESCRIPTION</Form.Label>
                  <Form.Control
                    type="string"
                    name="discount_desc"
                    placeholder="Enter discount description"
                  />
                </Form.Group>
              </Form>
            </Modal.Body>

            <Modal.Footer>
              <Button
                variant="primary"
                onClick={() =>
                  this.setState({
                    show: false,
                    state: false,
                  })
                }
              >
                Close
              </Button>
              <Button
                variant="primary"
                onClick={(event) => this.addDiscount(event)}
              >
                Add
              </Button>
            </Modal.Footer>
          </Modal>
        )}
      </Fragment>
    );
  }
}

export default Discount;
