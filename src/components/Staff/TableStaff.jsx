import React, { Component, Fragment } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Table, Modal, Form, Button } from "react-bootstrap";
import APIurl from "../../api/AppURL";
import axios from "axios";

export class TableStaff extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        staff_name: this.props.dataModal.staff_name,
        staff_ic: this.props.dataModal.staff_ic,
        staff_phoneno: this.props.dataModal.staff_phoneno,
        staff_email: this.props.dataModal.staff_email,
      },
    };
    this.handleInput = this.handleInput.bind(this);
  }
  handleInput = (e) => {
    this.setState({
      data: {
        ...this.state.data,
        [e.target.name]: e.target.value,
      },
    });
  };

  render = () => {
    return (
      <Fragment>
        <br />
        <Table
          hover
          responsive
          size="lg"
          style={{
            textAlign: "center",
            boxShadow: "0 0 10px 0 rgba(0, 0, 0, 0.2)",
          }}
        >
          <thead style={{ background: "#468C32", color: "white" }}>
            <tr>
              {this.props.headerStaff.map((res) => (
                <th>{res}</th>
              ))}
            </tr>
          </thead>
          <tbody style={{ border: "0px white", background: "white" }}>
            {this.props.isStaffExist &&
              this.props.role === "company" &&
              this.props.staffData.map((iteration) => {
                return iteration.map((elem, i) => (
                  <tr>
                    <td>{i + 1}</td>
                    <td>{elem.staff_name}</td>
                    <td>{elem.staff_ic}</td>
                    <td>{elem.staff_email}</td>
                    <td>{elem.staff_phoneno}</td>

                    <td>
                      {" "}
                      <FontAwesomeIcon
                        icon={["fas", "pen"]}
                        color="blue"
                        size="lg"
                        onClick={(e) => {
                          this.props.getStaffbyID(e, elem.id);
                        }}
                      />
                      <FontAwesomeIcon
                        icon={["fas", "trash"]}
                        color="red"
                        size="lg"
                        onClick={(e) => {
                          this.props.deleteStaff(e, elem.id);
                        }}
                      />
                    </td>
                  </tr>
                ));
              })}
            {!this.props.isStaffExist && this.props.role == "company" && (
              <tr>
                <td>None</td>
                <td>None</td>
                <td>None</td>
                <td>None</td>
                <td>None</td>
                <td>None</td>
              </tr>
            )}
          </tbody>
        </Table>
        <Modal show={this.props.showModal} onHide={!this.props.showModal}>
          <Form
            method="PUT"
            onSubmit={(event) => {
              console.log("e", event.target.elements.staff_name.value);
              this.props.updateStaff(event, {
                staff_name: event.target.elements.staff_name.value,
                staff_ic: event.target.elements.staff_ic.value,
                staff_phoneno: event.target.elements.staff_phoneno.value,
                staff_email: event.target.elements.staff_email.value,
              });
            }}
          >
            <Modal.Header closeButton>
              <Modal.Title>EDIT STAFF</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form.Group className="mb-3" onChange={this.handleInput}>
                <Form.Label>STAFF NAME</Form.Label>
                <Form.Control
                  type="string"
                  name="staff_name"
                  defaultValue={this.props.dataModal.staff_name}
                />
              </Form.Group>
              <Form.Group className="mb-3" onChange={this.handleInput}>
                <Form.Label>STAFF IC</Form.Label>
                <Form.Control
                  type="string"
                  name="staff_ic"
                  defaultValue={this.props.dataModal.staff_ic}
                />
              </Form.Group>
              <Form.Group className="mb-3" onChange={this.handleInput}>
                <Form.Label>STAFF PHONE NO.</Form.Label>

                <Form.Control
                  type="string"
                  name="staff_phoneno"
                  defaultValue={this.props.dataModal.staff_phoneno}
                />
              </Form.Group>
              <Form.Group className="mb-3" onChange={this.handleInput}>
                <Form.Label>STAFF EMAIL</Form.Label>
                <Form.Control
                  type="string"
                  name="staff_email"
                  defaultValue={this.props.dataModal.staff_email}
                />
              </Form.Group>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="primary" onClick={this.props.closeModal}>
                Close
              </Button>
              <Button variant="primary" type="submit">
                Update
              </Button>
            </Modal.Footer>
          </Form>
        </Modal>
      </Fragment>
    );
  };
}

export default TableStaff;
