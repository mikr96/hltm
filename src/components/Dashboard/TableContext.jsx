import React, { Component, Fragment } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "../../assets/css/calendar.css";
import { Table } from "react-bootstrap";

class TableContext extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        course_name: "",
        course_desc: "",
        course_fee: "",
        max_student: 0,
      },
      show: this.props.show,
      newStaffData: [],
      staffData: [],
      staff: false,
      company_id: "",
    };
    this.handleInput = this.handleInput.bind(this);
  }

  handleInput = (e) => {
    console.log(e.target.value);
    this.setState({
      data: {
        ...this.state.data,
        [e.target.name]: e.target.value,
      },
    });
  };

  render = () => {
    return (
      <Fragment>
        <br />
        <Table
          hover
          responsive
          size="lg"
          style={{
            textAlign: "center",
            boxShadow: "0 0 10px 0 rgba(0, 0, 0, 0.2)",
          }}
        >
          <thead style={{ background: "#468C32", color: "white" }}>
            <tr>
              {this.props.header.map((res) => (
                <th>{res}</th>
              ))}
            </tr>
          </thead>
          <tbody style={{ border: "0px white", background: "white" }}>
            {this.props.isDataExist &&
              this.props.role == "admin" &&
              this.props.cohortDetail.map((iteration) => {
                return iteration.map((elem, i) => (
                  <tr>
                    <td>{i + 1}</td>
                    <td>{elem.cohort_name}</td>
                    <td>
                      <strong>From:</strong>&nbsp;
                      {elem.cohort_date_start}
                      <br />
                      <strong>To:</strong>&nbsp;
                      {elem.cohort_date_end}
                    </td>
                    <td>Ongoing</td>
                  </tr>
                ));
              })}
            {!this.props.isDataExist && this.props.role == "admin" && (
              <tr>
                <td>0</td>
                <td>none</td>
                <td>
                  <strong>From:</strong>&nbsp; none
                  <br />
                  <strong>To:</strong>&nbsp; none
                </td>
                <td>none</td>
              </tr>
            )}
            {this.props.isStaffExist &&
              this.props.role == "company" &&
              this.props.staffData.map((iteration) => {
                return iteration.map((elem, i) => (
                  <tr>
                    <td>{i + 1}</td>
                    <td>
                      <strong>{elem.staff_name}</strong>
                    </td>
                    <td>
                      <strong>{elem.staff_ic}</strong>
                    </td>
                    <td>
                      <strong>{elem.staff_email}</strong>{" "}
                    </td>
                    <td>
                      <strong> {elem.staff_phoneno}</strong>{" "}
                    </td>
                    <td>
                      {" "}
                      <FontAwesomeIcon
                        icon={["fas", "pen"]}
                        color="blue"
                        size="lg"
                        onClick={(e) => {
                          this.props.getStaffbyID(e, elem.id);
                        }}
                      />
                      <FontAwesomeIcon
                        icon={["fas", "trash"]}
                        color="red"
                        size="lg"
                        onClick={(e) => {
                          this.props.deleteStaff(e, elem.id);
                        }}
                      />
                    </td>
                  </tr>
                ));
              })}
            {!this.props.isStaffExist && this.props.role == "company" && (
              <tr>
                <td>0</td>
                <td>None</td>
                <td>None</td>
                <td>None</td>
                <td>None</td>
                <td>None</td>
              </tr>
            )}
          </tbody>
        </Table>
      </Fragment>
    );
  };
}

export default TableContext;
