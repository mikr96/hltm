import React, { Component, Fragment } from "react";

class CardContext extends Component {
  constructor(props) {
    super(props);
    console.log(props);
  }

  render() {
    return (
      <Fragment>
        <div className="cardDashboard" style={{ textAlign: "center" }}>
          <h4>{this.props.title}</h4>
          <h2>{this.props.content}</h2>
        </div>
      </Fragment>
    );
  }
}

export default CardContext;
