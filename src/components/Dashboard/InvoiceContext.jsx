import React, { Component, Fragment } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Row, Button, Modal, Table } from "react-bootstrap";
import { Form } from "semantic-ui-react";
import Swal from "sweetalert2";

class TableContext extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.id,
    };
  }
  delete() {
    Swal.fire({
      title: "Are you sure?",
      text: "You want delete your item?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire("Deleted!", "Your file has been deleted.", "success");
      }
    });
  }

  render() {
    return (
      <Fragment>
        <Row>
          <div
            style={{
              display: "flex",
              justifyContent: "flex-end",
            }}
          >
            <Button
              variant="primary"
              onClick={() => this.setState({ show: true, stateList: true })}
            >
              + Add
            </Button>
          </div>
        </Row>
        {this.state.stateList && (
          <Modal
            show={this.state.show}
            onHide={() =>
              this.setState({ show: false, discount: false, stateList: false })
            }
          >
            <Modal.Header closeButton>
              <Modal.Title>Add Invoice</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form.Input
                fluid
                icon="file"
                id="invoiceprefix"
                iconPosition="left"
                placeholder="Invoice Prefix"
              />
            </Modal.Body>
            <Modal.Body>
              <Form.Input
                fluid
                icon="file"
                id="lastindex"
                iconPosition="left"
                placeholder="Last Index"
              />
            </Modal.Body>
            <Modal.Body>
              <Form.Input
                fluid
                icon="file"
                id="year"
                iconPosition="left"
                placeholder="Year"
              />
            </Modal.Body>
            <Modal.Footer>
              <Button
                variant="primary"
                onClick={() =>
                  this.setState({
                    show: false,
                    discount: false,
                    stateList: false,
                  })
                }
              >
                Close
              </Button>
              <Button
                variant="primary"
                onClick={() =>
                  this.setState({
                    show: false,
                    discount: false,
                    stateList: false,
                  })
                }
                onClick={this.alert}
              >
                Add
              </Button>
            </Modal.Footer>
          </Modal>
        )}

        <div class="header2">
          <h3>Invoice Setting</h3>
        </div>
        <Table striped bordered hover size="lg" style={{ textAlign: "center" }}>
          <thead>
            <tr>
              <th>No</th>
              <th>Prefix</th>
              <th>Last Index</th>
              <th>Year</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>HL</td>
              <td>68</td>
              <td>2021</td>
              <td>
                {" "}
                <FontAwesomeIcon icon={["fas", "pen"]} color="blue" size="lg" />
                <FontAwesomeIcon
                  icon={["fas", "trash"]}
                  color="red"
                  size="lg"
                  onClick={this.delete}
                />
              </td>
            </tr>
            <tr>
              <td>2</td>
              <td>HL </td>
              <td>127</td>
              <td>2021</td>
              <td>
                {" "}
                <FontAwesomeIcon icon={["fas", "pen"]} color="blue" size="lg" />
                <FontAwesomeIcon
                  icon={["fas", "trash"]}
                  color="red"
                  size="lg"
                  onClick={this.delete}
                />
              </td>
            </tr>
            <tr>
              <td>3</td>
              <td>HL</td>
              <td>344</td>
              <td>2021</td>
              <td>
                {" "}
                <FontAwesomeIcon icon={["fas", "pen"]} color="blue" size="lg" />
                <FontAwesomeIcon
                  icon={["fas", "trash"]}
                  color="red"
                  size="lg"
                  onClick={this.delete}
                />
              </td>
            </tr>
            <tr>
              <td>4</td>
              <td>HL</td>
              <td>449</td>
              <td>2021</td>
              <td>
                {" "}
                <FontAwesomeIcon icon={["fas", "pen"]} color="blue" size="lg" />
                <FontAwesomeIcon
                  icon={["fas", "trash"]}
                  color="red"
                  size="lg"
                  onClick={this.delete}
                />
              </td>
            </tr>
          </tbody>
        </Table>
      </Fragment>
    );
  }
}

export default TableContext;
