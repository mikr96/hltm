import React, { Component } from 'react';
import { ResponsiveContainer } from 'recharts';
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend
} from 'recharts';
import './LineChartComponent.css';

class LineChartComponent extends Component {
  render() {
    const data = [
      { name: '2014', PCHE: 1, PCIHA: 8, HAP:15, SPMPPHM20: 22, HCP:29, SPMMS1:36, SMHMS:3, SHOEM:1 },
      { name: '2015', PCHE: 2, PCIHA: 9, HAP:16, SPMPPHM20: 23, HCP:30, SPMMS1:37, SMHMS:4, SHOEM:23},
      { name: '2016', PCHE: 33, PCIHA: 10, HAP:17, SPMPPHM20: 24, HCP:31, SPMMS1:38, SMHMS:5, SHOEM:78 },
      { name: '2017', PCHE: 45, PCIHA: 11, HAP:18, SPMPPHM20: 25, HCP:32, SPMMS1:39, SMHMS:5, SHOEM:2},
      { name: '2018', PCHE: 5, PCIHA: 12, HAP:19, SPMPPHM20: 26, HCP:33, SPMMS1:40, SMHMS:6, SHOEM:12 },
      { name: '2019', PCHE: 60, PCIHA: 13, HAP:20, SPMPPHM20: 27, HCP:34, SPMMS1:1, SMHMS:7, SHOEM:13 },
      { name: '2021', PCHE: 10, PCIHA: 14, HAP:21, SPMPPHM20: 37, HCP:35, SPMMS1:28, SMHMS:90, SHOEM:67 }
    ];

    return (
      <ResponsiveContainer>
        <LineChart
          data={data}
          margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
        >
          <XAxis dataKey="name" />
          <YAxis />
          <CartesianGrid strokeDasharray="3 3" />
          <Tooltip />
          <Legend />
          <Line
            type="monotone"
            dataKey="PCHE"
            stroke="#8884d8"
            activeDot={{ r: 8 }}
          />
          <Line type="monotone" dataKey="PCIHA" stroke="#82ca9d" />
          <Line type="monotone" dataKey="HAP" stroke="#228B22" />
          <Line type="monotone" dataKey="SPMPPHM20" stroke="#006400" />
          <Line type="monotone" dataKey="HCP" stroke="#6B8E23" />
          <Line type="monotone" dataKey="SPMMS1" stroke="#6B8E23" />
          <Line type="monotone" dataKey="SMHMS" stroke="#6B8E23" />
          <Line type="monotone" dataKey="SHOEM" stroke="#6B8E23" />
        </LineChart>
      </ResponsiveContainer>
    );
  }
}

export default LineChartComponent;
