import React, { Component } from "react";
import FullCalendar from "@fullcalendar/react"; // must go before plugins
import dayGridPlugin from "@fullcalendar/daygrid"; // a plugin!

export class CalendarContext extends Component {
  render() {
    return (
      <div>
        <br />
        <FullCalendar plugins={[dayGridPlugin]} initialView="dayGridMonth" />
        <br />
        <br />
      </div>
    );
  }
}

export default CalendarContext;
