import React, { Component, Fragment } from "react";
import { Table, Dropdown, Button, Form, Modal, Col } from "react-bootstrap";
import { useNavigate } from "react-router-dom";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

class CohortDetailsContext extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.id,
    };
  }

  navigating() {
    console.log("Hello World");
    console.log(this.props.navigate);
  }

  render() {
    console.log("Hello World");
    console.log(this.props.navigate);
    return (
      <Fragment>
        <div class="header2">
          <h3>Trainee List</h3>
        </div>
        <Table
          variant="light"
          striped
          bordered
          hover
          size="sm"
          style={{ textAlign: "center" }}
        >
          <thead>
            <tr>
              <th>No</th>
              <th>Trainee</th>
              <th>Company</th>
              <th>Contact</th>
              <th>Cohort Info</th>
              <th>Join Status</th>
              <th>Payment Status</th>
              <th>HRDC</th>
              <th>HRDC Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>
                <strong>Leong Mei Ling</strong>
                <br />
                <small>700128015744</small>
                <br />
                <strong>Managing Director</strong>
              </td>
              <td>Swissen Trading and Transport S/B</td>
              <td>
                <span>
                  Email : <strong>kitnling@yahoo.com</strong>{" "}
                </span>
                <br />
                <span>
                  Contact No : <strong> 0132479468</strong>{" "}
                </span>
              </td>
              <td>
                <span>
                  Promo Code : <strong></strong>{" "}
                </span>
                <br />
                <span>
                  Special Diet : <strong>No</strong>{" "}
                </span>
                <br />
                <span>
                  Referrer : <strong>No</strong>{" "}
                </span>
                <br />
              </td>
              <td>X </td>
              <td>/</td>
              <td>/</td>
              <td>SEND</td>
              <td>
                {" "}
                <Dropdown>
                  <Dropdown.Toggle
                    variant="success"
                    id="dropdown-basic"
                  ></Dropdown.Toggle>

                  <Dropdown.Menu>
                    <Dropdown.Item
                      onClick={() => {
                        this.props.navigate("/layoutDashboard/attendance");
                      }}
                    >
                      Attendance
                    </Dropdown.Item>
                    <Dropdown.Item>
                      <Button
                        onClick={() =>
                          this.setState({ show: true, stateList: true })
                        }
                        className="primary"
                      >
                        <FontAwesomeIcon
                          icon={["fas", "calendar"]}
                          color="white"
                          size="sm"
                        />
                        &nbsp; Status Change
                      </Button>
                    </Dropdown.Item>
                    <Dropdown.Item>
                      <Button
                        onClick={() =>
                          this.setState({ show: true, stateList: true })
                        }
                        className="primary"
                      >
                        <FontAwesomeIcon
                          icon={["fas", "plus"]}
                          color="white"
                          size="sm"
                        />
                        &nbsp; Profoma Invoice
                      </Button>
                    </Dropdown.Item>
                    <Dropdown.Item href="#/action-3">
                      Resend Profoma
                    </Dropdown.Item>
                    <Dropdown.Item href="#/action-3">
                      Generate Invoice
                    </Dropdown.Item>
                    <Dropdown.Item href="#/action-3">Send HRDC</Dropdown.Item>
                    <Dropdown.Item href="#/action-3">
                      Proof Payment
                    </Dropdown.Item>
                    <Dropdown.Item>Update</Dropdown.Item>
                    <Dropdown.Item href="#/action-3">Delete</Dropdown.Item>
                  </Dropdown.Menu>
                </Dropdown>
              </td>
            </tr>
            <tr>
              <td>2</td>
              <td>
                <strong>Nur Syamim binti Abdul Rahman</strong>
                <br />
                <small>940423016064</small>
                <br />
                <strong>Unemployed</strong>
              </td>
              <td>-</td>
              <td>
                <span>
                  Email : <strong>nursyamim.rahman@gmail.com</strong>{" "}
                </span>
                <br />
                <span>
                  Contact No : <strong> 0137405168</strong>{" "}
                </span>
              </td>
              <td>
                <span>
                  Promo Code : <strong></strong>{" "}
                </span>
                <br />
                <span>
                  Special Diet : <strong></strong>{" "}
                </span>
                <br />
                <span>
                  Referrer : <strong></strong>{" "}
                </span>
                <br />
              </td>
              <td>X </td>
              <td>/</td>
              <td>X</td>
              <td>RESEND</td>
              <td>
                <Dropdown>
                  <Dropdown.Toggle
                    variant="success"
                    id="dropdown-basic"
                  ></Dropdown.Toggle>

                  <Dropdown.Menu>
                    <Dropdown.Item>
                      <Button
                        style={{ width: "100%" }}
                        onClick={() =>
                          this.setState({ show: true, stateList: true })
                        }
                        className="primary"
                      >
                        <FontAwesomeIcon
                          icon={["fas", "calendar-alt"]}
                          color="white"
                          size="sm"
                        />
                        &nbsp; Attendance
                      </Button>
                    </Dropdown.Item>
                    <Dropdown.Item>
                      <Button
                        style={{ width: "100%" }}
                        onClick={() =>
                          this.setState({ show: true, statusChange: true })
                        }
                        className="primary"
                      >
                        <FontAwesomeIcon
                          icon={["fas", "redo-alt"]}
                          color="white"
                          size="sm"
                        />
                        &nbsp; Status Change
                      </Button>
                    </Dropdown.Item>
                    {this.state.statusChange && (
                      <Modal
                        show={this.state.show}
                        onHide={() =>
                          this.setState({
                            show: false,
                            statusChange: false,
                            stateList: false,
                          })
                        }
                      >
                        <Modal.Header closeButton>
                          <Modal.Title>CHANGE FORM</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                          <Form>
                            <Form.Group className="mb-3">
                              <Form.Label>CHANGE</Form.Label>
                              <Form.Control
                                type="string"
                                placeholder="Leong Mei Ling"
                              />
                            </Form.Group>
                            <Form.Group className="mb-3">
                              <Form.Label>FROM</Form.Label>
                              <Form.Control
                                type="string"
                                placeholder="PCHE Withdraw Participation ( 2021-01-01 - 2021-01-01)"
                              />
                            </Form.Group>
                            <Form.Group className="mb-3">
                              <Form.Label>TO</Form.Label>
                            </Form.Group>
                          </Form>
                        </Modal.Body>

                        <Modal.Footer>
                          <Button
                            variant="primary"
                            onClick={() =>
                              this.setState({
                                show: false,
                                statusChange: false,
                                stateList: false,
                              })
                            }
                          >
                            Close
                          </Button>
                          <Button
                            variant="primary"
                            onClick={() =>
                              this.setState({
                                show: false,
                                statusChange: false,
                                stateList: false,
                              })
                            }
                          >
                            Submit
                          </Button>
                        </Modal.Footer>
                      </Modal>
                    )}
                    <Dropdown.Item>
                      <Button
                        style={{ width: "100%" }}
                        onClick={() =>
                          this.setState({ show: true, stateList: true })
                        }
                        className="primary"
                      >
                        <FontAwesomeIcon
                          icon={["fas", "bars"]}
                          color="white"
                          size="sm"
                        />
                        &nbsp; Profoma Invoice
                      </Button>
                    </Dropdown.Item>
                    <Dropdown.Item>
                      <Button
                        style={{ width: "100%" }}
                        onClick={() =>
                          this.setState({ show: true, stateList: true })
                        }
                        className="primary"
                      >
                        <FontAwesomeIcon
                          icon={["fas", "envelope"]}
                          color="white"
                          size="sm"
                        />
                        &nbsp; Resend Profoma
                      </Button>
                    </Dropdown.Item>
                    <Dropdown.Item>
                      <Button
                        style={{ width: "100%" }}
                        onClick={() =>
                          this.setState({ show: true, generateInvoice: true })
                        }
                        className="primary"
                      >
                        <FontAwesomeIcon
                          icon={["fas", "file-alt"]}
                          color="white"
                          size="sm"
                        />
                        &nbsp; Generate Invoice
                      </Button>
                      {this.state.generateInvoice && (
                        <Modal
                          show={this.state.show}
                          onHide={() =>
                            this.setState({
                              show: false,
                              generateInvoice: false,
                              stateList: false,
                            })
                          }
                        >
                          <Modal.Header closeButton>
                            <Modal.Title>GENERATE INVOICE</Modal.Title>
                          </Modal.Header>
                          <Modal.Body>
                            <Form>
                              <Form.Group className="mb-3">
                                <Form.Label>INVOICE NUMBER</Form.Label>
                                <Form.Control
                                  type="string"
                                  placeholder="HL/INV201907/00001"
                                />
                              </Form.Group>

                              <Form.Group className="mb-3">
                                <Form.Label>INVOICE DATE</Form.Label>
                              </Form.Group>
                            </Form>
                          </Modal.Body>

                          <Modal.Footer>
                            <Button
                              variant="primary"
                              onClick={() =>
                                this.setState({
                                  show: false,
                                  generateInvoice: false,
                                  stateList: false,
                                })
                              }
                            >
                              Close
                            </Button>
                            <Button
                              variant="primary"
                              onClick={() =>
                                this.setState({
                                  show: false,
                                  generateInvoice: false,
                                  stateList: false,
                                })
                              }
                            >
                              Submit
                            </Button>
                          </Modal.Footer>
                        </Modal>
                      )}
                    </Dropdown.Item>
                    <Dropdown.Item>
                      <Button
                        style={{ width: "100%" }}
                        onClick={() =>
                          this.setState({ show: true, sendHRDC: true })
                        }
                        className="primary btn-block"
                      >
                        <FontAwesomeIcon
                          icon={["fas", "envelope"]}
                          color="white"
                          size="sm"
                        />
                        &nbsp; Send HRDC
                      </Button>
                    </Dropdown.Item>
                    {this.state.sendHRDC && (
                      <Modal
                        show={this.state.show}
                        onHide={() =>
                          this.setState({
                            show: false,
                            sendHRDC: false,
                            stateList: false,
                          })
                        }
                      >
                        <Modal.Header closeButton>
                          <Modal.Title>SEND HRDC</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                          <Form>
                            <Form.Group className="mb-3">
                              <Form.Label>HRDC</Form.Label>
                              <Form.Control
                                type="string"
                                placeholder="Enter email you want to send"
                              />
                            </Form.Group>
                          </Form>
                        </Modal.Body>

                        <Modal.Footer>
                          <Button
                            variant="primary"
                            onClick={() =>
                              this.setState({
                                show: false,
                                sendHRDC: false,
                                stateList: false,
                              })
                            }
                          >
                            Cancel
                          </Button>
                          <Button
                            variant="primary"
                            onClick={() =>
                              this.setState({
                                show: false,
                                sendHRDC: false,
                                stateList: false,
                              })
                            }
                          >
                            OK
                          </Button>
                        </Modal.Footer>
                      </Modal>
                    )}
                    <Dropdown.Item>
                      <Button
                        style={{ width: "100%" }}
                        onClick={() =>
                          this.setState({ show: true, proofPayment: true })
                        }
                        className="primary btn-block"
                      >
                        <FontAwesomeIcon
                          icon={["fas", "plus"]}
                          color="white"
                          size="sm"
                        />
                        &nbsp; Proof Payment
                      </Button>
                    </Dropdown.Item>
                    {this.state.proofPayment && (
                      <Modal
                        show={this.state.show}
                        onHide={() =>
                          this.setState({
                            show: false,
                            proofPayment: false,
                            stateList: false,
                          })
                        }
                      >
                        <Modal.Header closeButton>
                          <Modal.Title>PAYMENT PROOF</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                          <Form>
                            <Form.Group className="mb-3">
                              <Form.Label>SELECT YOUR PAYMENT FILE:</Form.Label>
                              <br />
                              <input type="file" />
                            </Form.Group>
                          </Form>
                        </Modal.Body>

                        <Modal.Footer>
                          <Button
                            variant="primary"
                            onClick={() =>
                              this.setState({
                                show: false,
                                proofPayment: false,
                                stateList: false,
                              })
                            }
                          >
                            Cancel
                          </Button>
                          <Button
                            variant="primary"
                            onClick={() =>
                              this.setState({
                                show: false,
                                sendHRDC: false,
                                proofPayment: false,
                              })
                            }
                          >
                            Upload
                          </Button>
                        </Modal.Footer>
                      </Modal>
                    )}

                    <Dropdown.Item>
                      <Button
                        style={{ width: "100%" }}
                        onClick={() =>
                          this.setState({ show: true, stateList: true })
                        }
                        className="primary"
                      >
                        <FontAwesomeIcon
                          icon={["fas", "file-alt"]}
                          color="white"
                          size="sm"
                        />
                        &nbsp; Update
                      </Button>
                    </Dropdown.Item>
                    <Dropdown.Item>
                      {" "}
                      <Button
                        style={{ width: "100%" }}
                        onClick={() =>
                          this.setState({ show: true, stateList: true })
                        }
                        className="primary"
                      >
                        <FontAwesomeIcon
                          icon={["fas", "trash-alt"]}
                          color="white"
                          size="sm"
                        />
                        &nbsp; Delete
                      </Button>
                    </Dropdown.Item>
                  </Dropdown.Menu>
                </Dropdown>
              </td>
            </tr>
          </tbody>
        </Table>
      </Fragment>
    );
  }
}

export default navigateHook(CohortDetailsContext);

function navigateHook(CohortDetailsContext) {
  return function WrappedComponent(props) {
    const navigate = useNavigate();
    return <CohortDetailsContext {...props} navigate={navigate} />;
  };
}
